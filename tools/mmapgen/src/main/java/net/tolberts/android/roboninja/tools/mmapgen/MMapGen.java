package net.tolberts.android.roboninja.tools.mmapgen;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*
0. create master image (if it doesn't exist)
        1. read each map file, gather the X/Y coordinates
        2. Export image
        3. Add dots for items?
        4. See if master image needs to be resized
        5. update master image
*/


public class MMapGen {

    File inputMap;
    File outputImage;

    List<IntPoint> items = new LinkedList<IntPoint>();
    IntPoint teleporter = null;
    IntPoint mapPosition = null;

    int scale = 4;
    String exporter = "./tmxrasterizer -t %s -a";
    String tmpDir = "/tmp";
    String zone = "";

    Map<String, String> zoneLookup = new HashMap<String, String>() {{
        put("temple", "#b76c97");
        put("cave", "#89b583");
        put("city", "#a68159");
        put("industry", "#685356");
        put("gazebo", "#f2735f");
        put("boss", "#b0a5aa");
    }};

    public static void log(String str) {
        System.out.println(str);
    }


    public static void main(String[] args) {
        if (args.length < 2) {
            exitWithError("usage: java -jar MMapGen.jar (inputMap.tmx|mapDir) outputImage.png [scale] [exporterCommand] [tmpdir]");
        }
        File sourceMap = new File(args[0]);
        if (!sourceMap.exists()) {
            exitWithError("error: input map " + args[0] + " does not exist");
        }

        if (args[1].endsWith("tmx")) {
            log("running mMapGen with file " + args[0]);
            MMapGen mmapGen = new MMapGen(sourceMap, new File(args[1]));
            if (args.length > 2) {
                mmapGen.scale = Integer.parseInt(args[2]);
            }
            if (args.length > 3) {
                mmapGen.exporter = args[3];
            }
            if (args.length > 4) {
                mmapGen.tmpDir = args[4];
            }
            mmapGen.process();
            System.exit(0);
        } else {
            log("running mMapGen against dir" + args[0]);
            File output = new File(args[1]);
            log("output file: " + output.getAbsolutePath());
            log("temp dir: " + args[4]);
            if (output.exists()) {

                output.delete();
            }
            File folder = new File(args[0]);
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles == null) {
                exitWithError("Cannot read files in map directory " + args[0]);
            }

            for (File fileInMapDir : listOfFiles) {
                if (fileInMapDir.isFile() && fileInMapDir.getName().endsWith("tmx")) {
                    MMapGen mmapGen = new MMapGen(fileInMapDir, new File(args[1]));
                    if (args.length > 2) {
                        mmapGen.scale = Integer.parseInt(args[2]);
                    }
                    if (args.length > 3) {
                        mmapGen.exporter = args[3];
                    }
                    if (args.length > 4) {
                        mmapGen.tmpDir = args[4];
                    }
                    mmapGen.process();
                }
            }
        }


    }

    public static void exitWithError(String errorMsg) {

        System.err.print(errorMsg + System.getProperty("line.separator"));
        System.exit(1);
    }

    public MMapGen(File inputMap, File outputImage) {
        this.inputMap = inputMap;
        this.outputImage = outputImage;
    }


    public void process() {
        readMapXml();
        if (mapPosition == null) {
            return;
        }

        File tempImage = exportMapToImage();
        try {
            addBackdropColor(tempImage, zoneLookup.get(zone));
            addItemDots(tempImage);
            writeTempToOutputImage(tempImage);
        } catch (Exception e) {
            e.printStackTrace();
            exitWithError("Could not write map to final output image");
        }

    }


    public void readMapXml() {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document dom = db.parse(inputMap);


            //find the map position metadata and zone
            NodeList propertyTags = dom.getDocumentElement().getElementsByTagName("property");
            if (propertyTags != null && propertyTags.getLength() > 0) {
                for (int i = 0; i < propertyTags.getLength(); i++) {
                    Element propertyTag = (Element) propertyTags.item(i);
                    if (propertyTag.getAttribute("name").equals("minimap-position")) {
                        String minimapPosition = propertyTag.getAttribute("value");
                        mapPosition = IntPoint.createFromString(minimapPosition);
                    }
                    if (propertyTag.getAttribute("name").equals("zone")) {
                        zone = propertyTag.getAttribute("value");
                    }
                }
            }
            if (mapPosition == null) {
                System.out.println("No minimap-position attribute specified in map, ignoring " + inputMap.toString());
                mapPosition = null;
                return;
            }

            //find all the items
            NodeList objectGroups = dom.getDocumentElement().getElementsByTagName("objectgroup");
            if (objectGroups != null && objectGroups.getLength() > 0) {
                for (int i = 0; i < objectGroups.getLength(); i++) {
                    Element objectGroup = (Element) objectGroups.item(i);
                    String name = objectGroup.getAttribute("name");
                    if (name.equals("items")) {
                        NodeList objects = objectGroup.getElementsByTagName("object");
                        if (objects != null && objects.getLength() > 0) {
                            for (int j = 0; j < objects.getLength(); j++) {
                                Element itemElement = (Element) objects.item(j);
                                items.add(IntPoint.createFromStrings(itemElement.getAttribute("x"), itemElement.getAttribute("y")));
                                System.out.println("item at " + itemElement.getAttribute("x") + "," + itemElement.getAttribute("y"));
                            }
                        }
                    } else if (name.equals("enemies")) {
                        NodeList objects = objectGroup.getElementsByTagName("object");
                        if (objects != null && objects.getLength() > 0) {
                            for (int j = 0; j < objects.getLength(); j++) {
                                Element enemyElement = (Element) objects.item(j);
                                if ("teleporter".equals(enemyElement.getAttribute("name"))) {
                                    teleporter = IntPoint.createFromStrings(enemyElement.getAttribute("x"), enemyElement.getAttribute("y"));
                                    System.out.println("teleporter at " + enemyElement.getAttribute("x") + "," + enemyElement.getAttribute("y"));
                                }
                            }
                        }

                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            exitWithError("Could not parse map xml file");
        }
    }


    private File exportMapToImage() {
        File tempImage = null;
        try {
            String name = tmpDir + "/tmpMap" + inputMap.getName() + ".png";
            tempImage = new File(name);

            String exporterCommand = exporter.replace("%s", "" + scale);
            String command = exporterCommand + " " + inputMap.toString() + " " + tempImage.getAbsolutePath();
            Process p = Runtime.getRuntime().exec(command);
            System.out.println("running command: " + command);
            System.out.println("exporting " + inputMap.toString() + " to " + tempImage.getAbsolutePath());
            if (p.waitFor() != 0) {
                exitWithError("exporter exited with error");
            }

            if (new File(name).exists()) {
                return new File(name);
            }
            exitWithError(name + " does not exist for some dumb reason");
        } catch (Exception e) {
            e.printStackTrace();
            exitWithError("Could not run " + exporter + " to export map");
        }

        return null;
    }

    private void addBackdropColor(File imageFile, String color) throws IOException {
        if (color == null) {
            return;
        }
        BufferedImage original = ImageIO.read(imageFile);
        BufferedImage newImg = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());
        Graphics2D graphics = newImg.createGraphics();
        graphics.setColor(Color.decode(color));
        graphics.fillRect(0, 0, newImg.getWidth(), newImg.getHeight());
        graphics.drawImage(original, 0, 0, null, null);
        graphics.setColor(Color.BLACK);
        graphics.drawRect(0, 0, newImg.getWidth(), newImg.getHeight());
        ImageIO.write(newImg, "PNG", imageFile);
    }

    private void writeTempToOutputImage(File tempImage) throws IOException {
        IntPoint p = getMapCoordinates();
        BufferedImage map = ImageIO.read(tempImage);
        checkAndCreateOutputImage();
        BufferedImage output = ImageIO.read(outputImage);
        resizeOutputImageIfNecessary(map, output);
    }

    private void resizeOutputImageIfNecessary(BufferedImage map, BufferedImage output) throws IOException {
        IntPoint offset = calculateOffset(getMapCoordinates());
        if (output.getWidth() < (offset.x + map.getWidth()) ||
                output.getHeight() < (offset.y + map.getHeight())) {
            output = resizeImage(output, Math.max(output.getWidth(), offset.x + map.getWidth()),
                    Math.max(output.getHeight(), offset.y + map.getHeight()));
        }
        output.getGraphics().drawImage(map, offset.x, offset.y, null, null);
        ImageIO.write(output, "PNG", outputImage);
    }

    private BufferedImage resizeImage(BufferedImage img, int width, int height) {
        System.out.println("resizing destination image from " + img.getWidth() + "," + img.getHeight() + " to " + width + "," + height);
        BufferedImage resized = new BufferedImage(width, height, img.getType());
        Graphics2D g = resized.createGraphics();
        g.drawImage(img, 0, 0, null, null);
        g.dispose();
        return resized;
    }

    private IntPoint calculateOffset(IntPoint mapCoordinates) {
        return new IntPoint(mapCoordinates.x * scale, mapCoordinates.y * scale);
    }


    private void addItemDots(File tempImage) throws IOException {
        BufferedImage yellowDot = ImageIO.read(new File("yellowDotSmall.png"));
        BufferedImage blueDot = ImageIO.read(new File("blueDotSmall.png"));
        BufferedImage resized = ImageIO.read(tempImage);
        Graphics2D g = resized.createGraphics();
        for (IntPoint item : items) {
            g.drawImage(yellowDot, item.x / 16 * scale - 4, item.y / 16 * scale - 4, null);
        }
        if (teleporter != null) {
            g.drawImage(blueDot, teleporter.x / 16 * scale - 4, teleporter.y / 16 * scale - 4, null);
        }
        ImageIO.write(resized, "PNG", tempImage);
    }


    private void checkAndCreateOutputImage() {
        if (outputImage.exists()) {
            return;
        }
        BufferedImage blank = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        try {
            ImageIO.write(blank, "PNG", outputImage);
        } catch (IOException e) {
            e.printStackTrace();
            exitWithError("Could not create output image");

        }

    }

    public IntPoint getMapCoordinates() {
        return mapPosition;
    }
}
