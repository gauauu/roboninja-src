package net.tolberts.android.roboninja.tools.mmapgen;

public class IntPoint {
    public int x;
    public int y;

    public IntPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static IntPoint createFromString(String string) {
        String[] split = string.split(",");
        if (split.length != 2) {
            return null;
        }
        return createFromStrings(split[0], split[1]);
    }

    public static IntPoint createFromStrings(String x, String y) {
        IntPoint ip = new IntPoint(0,0);
        ip.x = Integer.parseInt(x);
        ip.y = Integer.parseInt(y);
        return ip;
    }
}
