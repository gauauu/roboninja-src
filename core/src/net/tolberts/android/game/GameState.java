package net.tolberts.android.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.characters.NPC;
import net.tolberts.android.game.levels.Level;
import net.tolberts.android.game.levels.LevelChangedListener;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.screens.GameScreen;

import java.util.*;

public abstract class GameState {

    public final static int MODE_NONE = 0;
    public final static int MODE_ACTION = 1;
    public final static int MODE_READY = 2;
    public final static int MODE_COUNTDOWN = 3;
    public final static int MODE_HUD_MENU = 4;
    public final static int MODE_PAUSE = 5;
    public final static int MODE_DYING = 6;
    public final static int MODE_HUD = 7;
    public final static int MODE_TRIGGER_MAIN_MENU = 8;
    public static final int MODE_TELEPORTING = 9;

    public MainCharacter mc;
    public List<GameCharacter> characters = new LinkedList<GameCharacter>();
    protected List<GameCharacter> charactersToAdd = new LinkedList<GameCharacter>();
    protected List<GameCharacter> toRespawn = new LinkedList<GameCharacter>();
    public GameSettings gameSettings;
    public GameProgress gameProgress;
    public Game game;
    public Level level;

    public final static int SPEED_MODE_NORMAL = 0;
    public final static int SPEED_MODE_FAST = 1;
    public final static int SPEED_MODE_EASY = 2;

    public float slowMotion;


    /*
     * These are used to tell the HUD what it should be showing during MODE_HUD.
     * Really, I should yank these out and use a generic HUD interface that the gamestate
     * can know about. I'm not sure why I didn't do that in the first place.
     */
    public String hudType;
    public String hudDetail;
    public HudCallback hudOnClose;


    public int currentMode;
    public float levelTimer;
    public int speedMode = SPEED_MODE_NORMAL;

    public abstract void newGame(int speedMode);

    protected Set<LevelChangedListener> levelChangedListeners = new HashSet<LevelChangedListener>();

    public void preloadAsync() {
        // do any pre-loading here (probably maps?)
    }

    public void checkForCollisions() {
        // check main character and all other characters
        for (GameCharacter character : characters) {
            if (character instanceof NPC) {
                if (character.collidesWith(mc)) {
                    ((NPC) character).collideWithMc(this);
                }
            }
            //we don't need to check ever enemy against every other enemy
            if (character instanceof NPC && !(character instanceof Enemy)) {
                for (GameCharacter compareAgainst : characters) {
                    if (compareAgainst == character) {
                        continue;   //don't compare with yourself
                    }
                    if (character.collidesWith(compareAgainst)) {
                        character.handleCollision(compareAgainst, this);
                    }
                }

            }
        }

        // check bullets against other characters?

    }

    public void update(float delta) {


        if (currentMode == MODE_ACTION) {
            levelTimer += delta;
            ((RoboNinjaProgress) gameProgress).addPlayTime(delta);
        }



        if (slowMotion > 0) {
            delta /= 2;
            slowMotion -= Math.min(delta, slowMotion);
        }

        delta *= (1 + level.zone.speedupFactor * levelTimer);

        if (speedMode == GameState.SPEED_MODE_FAST) {
            delta *= 1.27;
        } else if (speedMode == GameState.SPEED_MODE_EASY) {
            delta *= 0.9;
        }


        Collections.sort(characters, new Comparator<GameCharacter>() {
            public int compare(GameCharacter c1, GameCharacter c2) {
                if (c1.z == c2.z)
                    return 0;
                return c1.z < c2.z ? -1 : 1;
            }
        });

        mc.update(delta, currentMode);


        Iterator<GameCharacter> i = characters.iterator();
        while (i.hasNext()) {
            GameCharacter character = i.next();
            if (character instanceof NPC) {
                ((NPC) character).setGameState(this);
            }
            character.update(delta, currentMode);

            // remove dead characters
            if (character.dead) {
                i.remove();
                if (character.doesRespawn()) {
                    toRespawn.add(character);
                }
            }
        }
        characters.addAll(charactersToAdd);
        charactersToAdd.clear();
        checkForCollisions();
    }

    public void setLevel(Level level, Vector2 position, int facing) {
        this.level = level;
        if (position == null) {
            position = level.startingPoint;
        }
        mc.setPosition(position);
        mc.setFacing(facing);
        mc.checkAndUnstick();
        mc.cancelAbilitiesOnLevelChange();
        characters.clear();
        Set<GameCharacter> levelCharactersToSpawn = filterCharactersToSpawn(level.getCharacters());
        //instead of removing them, convert to collectible on pickup....
//        if (speedMode == SPEED_MODE_EASY) {
//            //omit consumable items in easy mode.
//            for (GameCharacter gameCharacter : levelCharactersToSpawn) {
//                if (gameCharacter instanceof ItemCharacter) {
//                    String id = ((ItemCharacter) gameCharacter).getId();
//                    if (Slowmo.ID.equals(id) || PortableRespawn.ID.equals(id)) {
//                        gameCharacter = CollectibleItem.createFromConsumable(gameCharacter);
//                    }
//                }
//                characters.add(gameCharacter);
//            }
//
//        } else {
            characters.addAll(levelCharactersToSpawn);
//        }
        for (LevelChangedListener listener : levelChangedListeners) {
            listener.levelChanged(level, this);
        }
    }

    public void addCharacter(GameCharacter npc) {
        charactersToAdd.add(npc);
    }

    private Set<GameCharacter> filterCharactersToSpawn(Collection<GameCharacter> rawCharacterList) {
        Set<GameCharacter> toSpawn = new HashSet<GameCharacter>();
        for (GameCharacter character : rawCharacterList) {

            //don't spawn NPC's if their flag has been set
            if (character instanceof NPC) {
                String flag = ((NPC) character).getFlag();
                if (flag != null && gameProgress.isFlagSet(flag)) {
                    continue;
                }
            }

            toSpawn.add(character);

        }


        return toSpawn;
    }

    public void resumeGame() {
        gameProgress.loadProgress();
        speedMode = gameProgress.getSpeedMode();
    }

    public void setGameScreenListener(LevelChangedListener listener) {
        levelChangedListeners.clear();
        levelChangedListeners.add(listener);
    }

    public GameScreen getGameScreen() {
        Screen screen = game.getScreen();
        if (screen instanceof GameScreen) {
            return (GameScreen) screen;
        } else {
            return null;
        }
    }

}
