package net.tolberts.android.game.levels;

public class LevelFactory {

	static Level currentLevel;

	public static Level getLevel(String levelId) {
		if (currentLevel == null || !(currentLevel.id.equals(levelId))) {
			currentLevel = new Level(levelId);
		}
		return currentLevel;
	}

    public static void clearCache() {
        currentLevel = null;
    }

}
