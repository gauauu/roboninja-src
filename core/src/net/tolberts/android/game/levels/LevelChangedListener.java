package net.tolberts.android.game.levels;

import net.tolberts.android.game.GameState;

public interface LevelChangedListener {
	
	public void levelChanged(Level level, GameState gamestate);

}
