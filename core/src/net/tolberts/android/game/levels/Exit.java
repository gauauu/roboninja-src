package net.tolberts.android.game.levels;

import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Maps;

import com.badlogic.gdx.math.Vector2;

public class Exit extends LevelTrigger {

    public final static float DEST_MATCH = Float.NEGATIVE_INFINITY; //crazy random number that we won't use otherwise....

	String destinationLevel;

    //the destination point is tracked based on the "normal" Y (how tiled maps it, quadrant IV instead of I)
    //instead of quadrant I like the game uses. This gets transformed when the actual level is already loaded
    //since we don't know the vertical size of the destination level until after we've loaded it.
	Vector2 upsideDownDestinationPoint;

	@Override
	public void onScrollVisible(GameState state) {

	}

	@Override
	public void onIntersect(GameState state) {
		Level level = LevelFactory.getLevel(destinationLevel);
        float destX = upsideDownDestinationPoint.x;
        if (destX == DEST_MATCH) {
            destX = state.mc.bounds.x;
        }
        float destY = level.getHeight() - upsideDownDestinationPoint.y;
        if (upsideDownDestinationPoint.y == DEST_MATCH) {
            destY = state.mc.bounds.y;
        }

        Vector2 destinationPoint = new Vector2(destX, destY);
		state.setLevel(level, destinationPoint, state.mc.getFacing());
	}

	public void preload() {
		if (destinationLevel != null && !destinationLevel.isEmpty()) {
			Maps.loadInBackground(destinationLevel);
		}
	}

}
