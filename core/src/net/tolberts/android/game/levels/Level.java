package net.tolberts.android.game.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntIntMap;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.loaders.Maps;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.characters.EnemyFactory;
import net.tolberts.android.roboninja.characters.ItemCharacter;
import net.tolberts.android.roboninja.characters.Teleporter;

import java.util.*;

public class Level {

    private static final String SPIKE_TILES = "spike-tiles";
    private static final String LAYER_START = "start";
    private static final String LAYER_COLLISION = "collision";
    private static final String LAYER_EXITS = "exits";
    private static final String LAYER_ITEMS = "items";
    private static final String LAYER_ENEMIES = "enemies";
    private static final String EXIT_LEVEL = "level";
    private static final String TARGET_X = "targetX";
    private static final String TARGET_Y = "targetY";
    private static final String TILE_RAMP_LEFT = "ramp-left-tiles";
    private static final String TILE_RAMP_RIGHT = "ramp-right-tiles";
    private static final String NO_COLLISION_TILES = "no-collide-tiles";
    private static final String ZONE = "zone";
    private static final String MINIMAP_POSITION = "minimap-position";
    private static final String ACTIVATION_MESSAGE = "activation-message";
    private static final String ARRIVAL_ACHIEVEMENT = "achievement";
    private static final String ARRIVAL_CUTSCENE = "cutscene";


    public final static int COLLISION_NONE = 0;
    public final static int COLLISION_WALL = 1;
    public final static int COLLISION_SPIKE_DEATH = 2;
    public final static int COLLISION_RAMP_LEFT = 3;
    public final static int COLLISION_RAMP_RIGHT = 4;


    public String id;

    TiledMap map;
    TiledMap background;

    IntIntMap specialTiles;


    public Vector2 minimapPosition = new Vector2(0, 0);

    public Vector2 startingPoint;

    public Vector3 backgroundColor = new Vector3(0.784f, 0.431f, 0.235f);

    private TiledMapTileLayer collisionLayer;

    List<LevelTrigger> triggers = new LinkedList<LevelTrigger>();
    Collection<GameCharacter> levelCharacters = new HashSet<GameCharacter>();
    public String mapName;
    public Zone zone;
    private int levelHeight;
    private int levelWidth;

    public String activationMessage = null;

    public String arrivalAchievement;
    public String arrivalCutscene = null;


    Level(String mapName) {
        this.id = mapName;
        this.mapName = mapName;
        setMap(mapName);
    }

    private void setMap(String mapName) {
        RoboNinjaGame.timedDebug("Level.setMap: " + mapName);
        map = Maps.loadMap(mapName);
        RoboNinjaGame.timedDebug("--after Maps.loadMap: " + mapName);

        specialTiles = new IntIntMap(10);

        initFromMap();
        RoboNinjaGame.timedDebug("after initFromMap: " + mapName);

        String backgroundMap = zone.background;
        if (backgroundMap != null) {
            RoboNinjaGame.timedDebug("loading background map: " + backgroundMap + " for " + mapName);
            background = Maps.loadMap(backgroundMap);
            RoboNinjaGame.timedDebug("(finished Level.setMap)" + mapName);
        }
    }

    public Collection<GameCharacter> getCharacters() {
        return levelCharacters;
    }

    public void initializeRenderer(BatchTiledMapRenderer renderer, BatchTiledMapRenderer bgRenderer) {
        renderer.setMap(map);
        bgRenderer.setMap(background);
    }

    protected void checkTriggers(GameState state) {
        for (LevelTrigger trigger : triggers) {
            trigger.checkIntersect(state);
            // trigger.checkOnScrollVisible(state);
        }
    }

    /**
     * Children could possibly use this for map updates or something?
     */
    @SuppressWarnings("UnusedParameters")
    public void update(GameState state, float delta) {
        checkTriggers(state);
    }

    public void render() {

    }

    public int isCollision(float x, float y) {
        return isCollision((int) x, (int) y);
    }

    public int isCollision(int x, int y) {
        if (y < 0) {
            return COLLISION_SPIKE_DEATH;
        }
        Cell cell = collisionLayer.getCell(x, y);
        if (cell == null) {
            return COLLISION_NONE;
        }

        int tileId = cell.getTile().getId();

        if (specialTiles.containsKey(tileId)) {
            return specialTiles.get(tileId, COLLISION_NONE);
        } else {
            return COLLISION_WALL;
        }
    }

    public int getWidth() {
        return levelWidth;
    }

    public int getHeight() {
        return levelHeight;
    }

    protected void initSpecialTiles(String tileType, int collisionType) {
        // initialize spike tiles from property "spike-tiles"
        Object spikeTilesProperty = map.getProperties().get(tileType);
        if ((spikeTilesProperty != null) && (spikeTilesProperty instanceof String)) {
            String[] split = ((String) spikeTilesProperty).split(",");
            //spikeTiles = new int[split.length];

            for (String tile : split) {
                if ("".equals(tile.trim())) {
                    continue;
                }
                try {
                    int parsedTile = Integer.parseInt(tile);
                    specialTiles.put(parsedTile, collisionType);
                } catch (NumberFormatException e) {
                    Gdx.app.debug("RoboNinja", "Could not parse special tile: " + tile + " (" + tileType + ")" + id);
                }
            }
        }
    }

    public void initFromMap() {

        float scale = GameSettings.getInstance().scale;

        initSpecialTiles(SPIKE_TILES, COLLISION_SPIKE_DEATH);
        initSpecialTiles(TILE_RAMP_LEFT, COLLISION_RAMP_LEFT);
        initSpecialTiles(TILE_RAMP_RIGHT, COLLISION_RAMP_RIGHT);
        initSpecialTiles(NO_COLLISION_TILES, COLLISION_NONE);


        // a starting point?
        MapLayer mapLayer = map.getLayers().get(LAYER_START);
        if (mapLayer != null) {
            MapObject mapObject = mapLayer.getObjects().get(LAYER_START);
            if (mapObject instanceof RectangleMapObject) {
                Rectangle rectangle = ((RectangleMapObject) mapObject).getRectangle();
                startingPoint = new Vector2(rectangle.x * scale, rectangle.y * scale);
            }
        }

        // collision map access
        MapLayer probablyCollisionLayer = map.getLayers().get(LAYER_COLLISION);
        if ((probablyCollisionLayer == null) || (!(probablyCollisionLayer instanceof TiledMapTileLayer))) {
            Gdx.app.error("Level", "ERROR - could not find collision layer or is not tile layer");
        } else {
            collisionLayer = ((TiledMapTileLayer) probablyCollisionLayer);
//			for (int y = 0; y < 100; y++) {
//				for (int x = 0; x < 100; x++) {
//					isCollision(x, y);
//				}
//			}
        }

        levelHeight = collisionLayer.getHeight();
        Gdx.app.debug("Level", "level height is " + levelHeight);

        levelWidth = collisionLayer.getWidth();

        // object layer "exits"
        MapLayer exitLayer = map.getLayers().get(LAYER_EXITS);
        if (exitLayer != null) {
            MapObjects objects = exitLayer.getObjects();

            for (MapObject mapObject : objects) {
                if (mapObject instanceof RectangleMapObject) {
                    Rectangle mapRectangle = ((RectangleMapObject) mapObject).getRectangle();

                    Rectangle rectangle = new Rectangle(mapRectangle);

                    rectangle.x *= scale;
                    rectangle.y *= scale;
                    rectangle.height *= scale;
                    rectangle.width *= scale;

                    Exit exit = new Exit();
                    exit.region = rectangle;
                    MapProperties props = mapObject.getProperties();
                    exit.destinationLevel = props.get(EXIT_LEVEL).toString();
                    exit.upsideDownDestinationPoint = new Vector2();
                    //parse out the X and Y destination points, with a blank meaning to preserve the character's position
                    //in that dimension
                    Object targetXObj = props.get(TARGET_X);
                    if (targetXObj == null || "".equals(targetXObj.toString())) {
                        exit.upsideDownDestinationPoint.x = Exit.DEST_MATCH;
                    } else {
                        exit.upsideDownDestinationPoint.x = Float.parseFloat(targetXObj.toString());
                    }
                    Object targetYObj = props.get(TARGET_Y);
                    if (targetYObj == null || "".equals(targetYObj.toString())) {
                        exit.upsideDownDestinationPoint.y = Exit.DEST_MATCH;
                    } else {
                        exit.upsideDownDestinationPoint.y = Float.parseFloat(targetYObj.toString());
                    }

                    exit.preload();
                    triggers.add(exit);
                }
            }
        }

        // enemies to spawn
        MapLayer enemyLayer = map.getLayers().get(LAYER_ENEMIES);
        if (enemyLayer != null) {
            MapObjects objects = enemyLayer.getObjects();
            for (MapObject mapObject : objects) {
                if (mapObject instanceof RectangleMapObject) {
                    RectangleMapObject rectObj = (RectangleMapObject) mapObject;
                    String enemyId = mapObject.getName();
                    Enemy enemy = EnemyFactory.newEnemy(enemyId);
                    if (enemy != null) {
                        MapProperties properties = mapObject.getProperties();
                        Iterator<String> keys = properties.getKeys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            enemy.setProperty(key, properties.get(key).toString());
                        }
                        Rectangle sourceRect = rectObj.getRectangle();
                        enemy.setDefinitionBounds(sourceRect);


                        levelCharacters.add(enemy);

                    }
                }
            }
        }

        // items
        MapLayer itemLayer = map.getLayers().get(LAYER_ITEMS);
        if (itemLayer != null) {
            MapObjects objects = itemLayer.getObjects();
            for (MapObject mapObject : objects) {
                if (mapObject instanceof RectangleMapObject) {
                    RectangleMapObject rectObj = (RectangleMapObject) mapObject;
                    String itemId = mapObject.getName();



                    ItemCharacter itemCharacter = new ItemCharacter(itemId, id);

                    Rectangle sourceRect = rectObj.getRectangle();
                    itemCharacter.bounds = new Rectangle(sourceRect.x * scale, sourceRect.y * scale, 1, 1);

                    levelCharacters.add(itemCharacter);
                }
            }
        }

        // custom object types in children?

        // zone
        String zoneId = map.getProperties().get(ZONE).toString();
        zone = Zone.loadZone(zoneId);

        // minimap
        try {
            String minimapPositionText = map.getProperties().get(MINIMAP_POSITION).toString();
            String[] coords = minimapPositionText.split(",");
            minimapPosition = new Vector2(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]));
        } catch (Exception e) {
            //no parse-able minimap, default to 0
        }

        Object actvMesgProp = map.getProperties().get(ACTIVATION_MESSAGE);
        if (actvMesgProp != null) {
            activationMessage = actvMesgProp.toString();
        }

        Object arrivalCutsceneProp = map.getProperties().get(ARRIVAL_CUTSCENE);
        if (arrivalCutsceneProp != null) {
            arrivalCutscene = arrivalCutsceneProp.toString();
        }


        Object arrivalAchievementProp = map.getProperties().get(ARRIVAL_ACHIEVEMENT);
        if (arrivalAchievementProp != null) {
            arrivalAchievement = arrivalAchievementProp.toString();
        }

    }

    public Teleporter getTeleporterIfExists() {
        for (GameCharacter levelCharacter : levelCharacters) {
            if (levelCharacter instanceof Teleporter) {
                return (Teleporter) levelCharacter;
            }
        }
        return null;
    }
}
