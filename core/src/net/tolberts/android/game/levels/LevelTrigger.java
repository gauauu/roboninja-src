package net.tolberts.android.game.levels;

import net.tolberts.android.game.GameState;

import com.badlogic.gdx.math.Rectangle;

public abstract class LevelTrigger {

	Rectangle region;


	public void checkIntersect(GameState state) {
		if (state.mc.bounds.overlaps(region)) {
			onIntersect(state);
		}
	}

	public abstract void onScrollVisible(GameState state);

	public abstract void onIntersect(GameState state);

}