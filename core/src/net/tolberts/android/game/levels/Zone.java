package net.tolberts.android.game.levels;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import net.tolberts.android.game.loaders.TextData;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class Zone {

    public final static String MUSIC = "music";
    public final static String BACKDROP = "backdrop";
    public final static String BACKGROUND = "background";
    public final static String SPEEDUP_FACTOR = "speedup-factor";
    public final static String WAVEY_BACKDROP = "wavey-backdrop";

    public String music;
    public String background;
    public String backdrop;

    public float speedupFactor = 0.0f;

    public boolean waveyBackdrop = false;

    static Zone lastLoadedZone;

    static Zone loadZone(String zoneId) {
        Map<String, String> zoneData = TextData.loadTextData("zone_" + zoneId);
        Zone zone = new Zone();
        zone.music = zoneData.get(MUSIC);
        zone.backdrop = zoneData.get(BACKDROP);
        zone.background = zoneData.get(BACKGROUND);
        String speedupStr = zoneData.get(SPEEDUP_FACTOR);


        try {
            if (speedupStr != null) {

                zone.speedupFactor = Float.parseFloat(speedupStr);
            }
        } catch (NumberFormatException e) {
            Gdx.app.error(RoboNinjaGame.TAG, "Could not parse speedup-factor of " + speedupStr);
        }

        if (zoneData.containsKey(WAVEY_BACKDROP)) {
            zone.waveyBackdrop = true;
        }
        return zone;
    }

}
