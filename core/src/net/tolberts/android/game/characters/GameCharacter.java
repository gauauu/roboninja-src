package net.tolberts.android.game.characters;

import net.tolberts.android.game.levels.Level;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;

import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class GameCharacter {

	// world-based coordinates
	public Rectangle bounds = new Rectangle();
	public Vector2 velocity = new Vector2();
	public final Vector2 acceleration = new Vector2();

	// this must be updated when gotten, but the object is reused to prevent
	// garbage collection
	private Vector2 position = new Vector2();

	// simple z-index
	public float z = 0;
	public boolean visible = true;

	protected float animationTick;
    public boolean animationPaused = false;

	public boolean dead = false;

	protected GameSettings gameSettings;

	public abstract void updateCharacter(float delta);

	public abstract void render(BatchTiledMapRenderer renderer);

	public void update(float delta, int gameMode) {
        if (!animationPaused) {
		    animationTick += delta;
        }
		if (updatesDuringMode(gameMode)) {
			updateCharacter(delta);
		}
	}

	protected boolean updatesDuringMode(int gameMode) {
		if (gameMode == GameState.MODE_ACTION) {
			return true;
		}
		return false;
	}

	public Vector2 getPosition() {
		position.x = bounds.x;
		position.y = bounds.y;
		return position;
	}

	public void applyPhysics(float delta) {
		applyPhysics(delta, false);
	}

	public void applyPhysics(float delta, boolean ignoreTerminalVelocity) {

		velocity.x += acceleration.x * delta;
		velocity.y += acceleration.y * delta;

		if (!ignoreTerminalVelocity) {
			if (velocity.y < gameSettings.terminalVelocity) {
				velocity.y = gameSettings.terminalVelocity;
			}
		}

		bounds.x += velocity.x * delta;
		bounds.y += velocity.y * delta;
	}

	protected int collidesWithTile(Level level) {

		int collision = Level.COLLISION_NONE;

		float xMax = bounds.x + bounds.width;
		float yMax = bounds.y + bounds.height;
		float x = bounds.x;

        //check every 1 unit that makes up the character to see if any points collide
        // (if the character is large, we could hit a wall only in some middle part, and
        // not just at a corner)
		while (x < xMax + 1) {
			float y = bounds.y;
			x = Math.min(x, xMax);
			while (y < yMax + 1) {
				y = Math.min(y, yMax);
				collision = Math.max(collision, level.isCollision(x, y));
				y = y + 1;
			}
			x = x + 1;
		}

		return collision;
	}

	public boolean collidesWith(GameCharacter c) {
		return (bounds.overlaps(c.bounds));
	}

	public void setPosition(Vector2 position) {
		bounds.x = position.x;
		bounds.y = position.y;
	}

    public void handleCollision(GameCharacter compareAgainst, GameState gameState) {
        //in the general case, we do nothing.
    }

    public boolean doesRespawn() {
        return false;
    }

    public void respawn() {
        dead = false;
    }

    public void onMcRespawn() {

    }

}
