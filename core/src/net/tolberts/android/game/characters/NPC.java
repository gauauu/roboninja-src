package net.tolberts.android.game.characters;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.roboninja.screens.GameScreen;

public abstract class NPC extends AnimatedCharacter{
	
	protected GameState gameState;
	
	public abstract void collideWithMc(GameState state);
    public void collideWithMcBullet(GameState state){
        //default behavior is we don't care
    }
	
	public String getFlag() {
		return null;
	}
	
	public void setGameState(GameState gs) {
		this.gameState = gs;
	}

    public boolean isOnGameScreen() {
        GameScreen gameScreen = gameState.getGameScreen();
        if (gameScreen == null) {
            return false;
        }
        Rectangle cameraRectangle = gameScreen.getCameraRectangle();
        return (cameraRectangle.overlaps(bounds));
    }

    Vector2 distanceHolder = new Vector2();
    public float distanceFromGameScreen() {
        GameScreen gameScreen = gameState.getGameScreen();
        if (gameScreen == null) {
            return 0.0f;
        }
        Rectangle cameraRectangle = gameScreen.getCameraRectangle();
        if (cameraRectangle.overlaps(bounds)) {
            return 0.0f;
        }
        cameraRectangle.getCenter(distanceHolder);
        return Math.abs(distanceHolder.dst(getPosition()));
    }


}
