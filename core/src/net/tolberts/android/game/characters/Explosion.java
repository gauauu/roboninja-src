package net.tolberts.android.game.characters;

import com.badlogic.gdx.graphics.g2d.Batch;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Art;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;

public class Explosion extends GameCharacter {

	private float size;
	private float speed;
	private float currentSpread;

	static Texture shrapnelTexture;

	public static Explosion McExplosion(float x, float y) {
		return new Explosion(x, y, 4, 4);
	}

	public static Explosion EnemyExplosion(float x, float y) {
		return new Explosion(x, y, 1.5f, 0.75f);
	}

	public Explosion(float x, float y, float size, float speed) {
		setPosition(new Vector2(x, y));
		this.size = size;
		this.speed = speed;
		this.currentSpread = 0;
		if (shrapnelTexture == null) {
			shrapnelTexture = Art.getTexture("shrapnelSquare");
		}
		visible = true;
	}

	protected boolean updatesDuringMode(int gameMode) {
		if (gameMode == GameState.MODE_DYING) {
			return true;
		}
		return super.updatesDuringMode(gameMode);
	}

	public void resetAt(float x, float y) {
		Vector2 pos = getPosition();
		pos.x = x;
		pos.y = y;
		currentSpread = 0;
		visible = true;
	}

	@Override
	public void updateCharacter(float delta) {
		currentSpread += delta * speed;
		if (currentSpread > size) {
			visible = false;
			dead = true;
		}
	}

	public boolean isFinished() {
		return dead;
	}

	@Override
	public void render(BatchTiledMapRenderer renderer) {
		Batch batch = renderer.getSpriteBatch();

		Vector2 pos = getPosition();
		
		float size = 0.5f;
		float diagonal = 0.714f * currentSpread;
		
		batch.draw(shrapnelTexture, pos.x + currentSpread, pos.y, size, size);
		batch.draw(shrapnelTexture, pos.x + diagonal, pos.y + diagonal, size, size);
		batch.draw(shrapnelTexture, pos.x, pos.y + currentSpread, size, size);
		batch.draw(shrapnelTexture, pos.x - diagonal, pos.y + diagonal, size, size);
		batch.draw(shrapnelTexture, pos.x - currentSpread, pos.y, size, size);
		batch.draw(shrapnelTexture, pos.x - diagonal, pos.y - diagonal, size, size);
		batch.draw(shrapnelTexture, pos.x, pos.y - currentSpread, size, size);
		batch.draw(shrapnelTexture, pos.x + diagonal, pos.y - diagonal, size, size);

	}

}
