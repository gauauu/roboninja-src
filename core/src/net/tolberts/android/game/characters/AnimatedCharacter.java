package net.tolberts.android.game.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.util.GfxUtils;

import java.util.HashMap;
import java.util.Map;

public abstract class AnimatedCharacter extends GameCharacter {

    //for rendering
    protected Texture texture;
    protected TextureRegion[][] textureRegions;
    public boolean hFlip;

    protected Rectangle inGameSpriteBounds;
    protected Rectangle inTextureSpriteBounds;

    protected Vector2 textureOffset = new Vector2(0, 0);
    protected Vector2 textureScale = new Vector2(1, 1);

    public float alpha = 1;


    protected boolean isTintCycling;

    Map<String, Animation> animations;
    private String currentAnimation;

    public AnimatedCharacter() {
        gameSettings = GameSettings.getInstance();
        if (shouldInitGraphicsOnCreate()) {
            initGraphics();
        }
    }

    protected boolean shouldInitGraphicsOnCreate() {
        return true;
    }

    protected void initGraphics() {
        Vector2 oneFrame = getFrameSize();
        initTextures(getTextureName(), Math.round(oneFrame.x), Math.round(oneFrame.y));
        animations = new HashMap<String, Animation>();
        initAnimations(animations);
    }

    protected abstract String getTextureName();

    protected abstract Vector2 getFrameSize();

    /**
     * Children should add all animations they want to use to this map
     */
    protected abstract void initAnimations(Map<String, Animation> animations);

    /**
     * Initializes the textureRegions for the character. Just need to know the dimensions of each cell
     */
    protected void initTextures(String name, int framePxWidth, int framePxHeight) {
        texture = Art.getTexture(name);
        textureRegions = TextureRegion.split(texture, framePxWidth, framePxHeight);
        inTextureSpriteBounds = new Rectangle(0, 0, framePxWidth * gameSettings.scale, framePxHeight * gameSettings.scale);
        inGameSpriteBounds = new Rectangle(0, 0, framePxWidth * gameSettings.scale * textureScale.x, framePxHeight * gameSettings.scale * textureScale.y);
    }

    int test = 0;
    public void setCurrentAnimation(String animName) {
        if (animations.containsKey(animName)) {
            if (!animName.equals(currentAnimation)) {
                animationTick = 0;
                if ("jump".equals(animName)) {
                    if (test++ == 1) {
                        System.out.println("here");
                    }

                }
            }
            currentAnimation = animName;
        }
    }

    public String getCurrentAnimation() {
        return currentAnimation;
    }

    protected boolean isAnimationFinished() {
        return animations.get(currentAnimation).isAnimationFinished(animationTick);
    }



    public void setTintCycling(boolean isTintCycling) {
        this.isTintCycling = isTintCycling;
    }


    public void render(BatchTiledMapRenderer renderer) {
        if (!visible) {
            return;
        }
        TextureRegion frame = getCurrentFrame();
        if (frame == null) {
            return;
        }

        Batch batch = renderer.getSpriteBatch();

        if (isTintCycling) {
            int tick = (int) ((animationTick - Math.floor(animationTick)) * 10);
            if (tick % 2 == 0) {
                batch.setColor(Color.RED);
            } else {
                batch.setColor(Color.YELLOW);
            }
        } else {
            batch.setColor(Color.WHITE);
        }

        if (alpha != 1) {
            GfxUtils.setBatchAlpha((SpriteBatch) batch, alpha);
        }


        if (!hFlip) {
            batch.draw(frame, bounds.x + inGameSpriteBounds.x + textureOffset.x, bounds.y + inGameSpriteBounds.y + textureOffset.y, 0, 0, inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);

        } else {
            batch.draw(frame, bounds.x - inGameSpriteBounds.x + textureOffset.x + inGameSpriteBounds.width, bounds.y + inGameSpriteBounds.y + textureOffset.y, 0, 0, -inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);
        }
        if (GameSettings.getInstance().debugSprites) {
            batch.draw(Art.getTexture("debugframe"), bounds.x, bounds.y, 0, 0, bounds.width, bounds.height, 1, 1, 0, 0, 0, 16, 16, false, false);

        }
        if (alpha != 1) {
            GfxUtils.setBatchAlpha((SpriteBatch) batch, 1.0f);
        }
        batch.setColor(Color.WHITE);
    }

    protected TextureRegion getCurrentFrame() {
        if (animations == null) {
            Gdx.app.debug("AnimatedCharacter", "animations set to null");
        }
        Animation animation = animations.get(currentAnimation);
        return animation.getKeyFrame(animationTick);
    }

}
