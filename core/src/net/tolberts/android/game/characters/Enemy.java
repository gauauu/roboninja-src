 package net.tolberts.android.game.characters;

import com.badlogic.gdx.math.Rectangle;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.levels.Level;

public abstract class Enemy extends NPC {

    protected Rectangle definitionBounds;
    protected float dyingTimer = -100;

    public abstract String getId();
	
	public abstract void setProperty(String prop, String value);

    /**
     * convenience function for checking properties
     */
    public boolean propertyMatch(String propName, String propValue, String prop, String value) {
        return (propName.equalsIgnoreCase(prop) && propValue.equalsIgnoreCase(value));
    }

    public final static String FACING_LEFT = "left";
    public final static String FACING_RIGHT = "right";
    public final static String PROP_FACING = "facing";

    public final static int FACING_RIGHT_INT = 1;
    public final static int FACING_LEFT_INT = -1;

    @Override
    public void collideWithMcBullet(GameState state) {
        //kill the enemy?
    }

    @Override
    public void collideWithMc(GameState state) {
        if (dyingTimer < -50) {
            state.mc.die();
        }
    }

    public boolean collideWithMcLaser(GameState state) {
        dyingTimer = 0.25f;
        setTintCycling(true);
        return false;
    }

    public boolean usesDefinitionBounds() {
        return false;
    }

    public void applyGravityWithCollision(float delta) {
        acceleration.y = GameSettings.getInstance().gravity;
        float currentY = bounds.y;
        applyPhysics(delta);
        int tileCollision = collidesWithTile(gameState.level);
        if (tileCollision == Level.COLLISION_WALL) {

            bounds.y = currentY;
            velocity.y = 0;
        }
    }

    public void setDefinitionBounds(Rectangle definitionBounds) {
        float scale = GameSettings.getInstance().scale;

        this.definitionBounds = new Rectangle();
        this.definitionBounds.x = definitionBounds.x * scale;
        this.definitionBounds.y = definitionBounds.y * scale;
        this.definitionBounds.width = definitionBounds.width * scale;
        this.definitionBounds.height = definitionBounds.height * scale;

        if (!usesDefinitionBounds()) {
            bounds = new Rectangle(definitionBounds.x * scale, definitionBounds.y * scale, 1, 1);
        }

    }

    @Override
    public void update(float delta, int gameMode) {

        if (!animationPaused) {
            animationTick += delta;
        }
        if (updatesDuringMode(gameMode)) {
            if (dyingTimer > -50) {
                dyingTimer -= delta;
                if (dyingTimer < 0) {
                    dead = true;
                }
            } else {
                updateCharacter(delta);
            }
        }



    }

    @Override
    public void respawn() {
        dyingTimer = -100;
        setTintCycling(false);
        dead = false;
    }
}
