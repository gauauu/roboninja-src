package net.tolberts.android.game;

public interface GameProgress {

	public void saveProgress();
	public void loadProgress();
	
	public boolean isFlagSet(String flag);

	void setSpeedMode(int speedMode);
	int getSpeedMode();
}
