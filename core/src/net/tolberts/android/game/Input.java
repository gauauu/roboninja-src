package net.tolberts.android.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.IntSet;

import java.util.HashMap;
import java.util.Map;

public class Input {

	static boolean spaceReleased = true;
	static boolean ignore = false;
    static boolean ignoreUntilRelease = false;

    static IntSet keysDown = new IntSet();

    /**
     * Instructs the normal input processing to ignore taps until disabled.
     * @param ignore
     */
	public static void ignoreTap(boolean ignore) {
		Input.ignore = ignore;
	}

    /**
     * Instructs the normal input processing the ignore the current tap until it released, then
     * restart normal input processing
     */
    public static void ignoreUntilRelease() {
        Input.ignoreUntilRelease = true;
    }
	
	public static boolean wasSpacebarOrScreenTapped() {
		boolean isCurrent = (Gdx.input.isKeyPressed(Keys.SPACE) || Gdx.input.isTouched(0));
		if (isCurrent) {
			if (!spaceReleased){
				isCurrent = false;
			}
			spaceReleased = false;
			return isCurrent;
		} else {
			spaceReleased = true;
			return false;
		}
	}
	
	public static boolean wasSpacebarOrScreenTouch() {
		if (ignore) {
			return false;
		}
		boolean isCurrent = (Gdx.input.isKeyPressed(Keys.SPACE) || Gdx.input.isTouched(0));
		if (isCurrent) {
			spaceReleased = false;
		} else {
          ignoreUntilRelease = false;
        }
        if (ignoreUntilRelease) {
            return false;
        }
		return isCurrent;
	}

    public static boolean wasKeyPressed(int gdxKey) {
        boolean currentlyPressed = Gdx.input.isKeyPressed(gdxKey);
        if (currentlyPressed) {
            if (keysDown.contains(gdxKey)) {
                return false;
            }
            keysDown.add(gdxKey);
            return true;
        } else {
            keysDown.remove(gdxKey);
            return false;
        }
    }
	
	public static boolean wasPPressed() {
		return (Gdx.input.isKeyPressed(Keys.P));
	}

	public static boolean wasSpacebarPressed() {
		return (Gdx.input.isKeyPressed(Keys.SPACE));
	}

	public static boolean wasUpPressed() {
		return (Gdx.input.isKeyPressed(Keys.UP));
	}

	public static boolean wasDownPressed() {
		return (Gdx.input.isKeyPressed(Keys.DOWN));
	}

	public static boolean wasLeftPressed() {
		return (Gdx.input.isKeyPressed(Keys.LEFT));
	}

	public static boolean wasRightPressed() {
		return (Gdx.input.isKeyPressed(Keys.RIGHT));
	}

	public static boolean isTouched(float startX, float endX) {
		// check if any finger is touch the area between startX and endX
		// startX/endX are given between 0 (left edge of the screen) and 1
		// (right edge of the screen)
		// (shamelessly lifted from SuperKoalio demo)
		for (int i = 0; i < 2; i++) {
			float x = Gdx.input.getX() / (float) Gdx.graphics.getWidth();
			if (Gdx.input.isTouched(i) && (x >= startX && x <= endX)) {
				return true;
			}
		}
		return false;
	}

}
