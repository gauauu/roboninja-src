package net.tolberts.android.game;

import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class GameSettings {

    public final static float GRAVITY = -29.5f;

	public float gravity = GRAVITY;
	public float scale = 1/16f;
	public float terminalVelocity = -25;
	public Vector2 viewport = new Vector2(RoboNinjaGame.WIDTH / 16, RoboNinjaGame.HEIGHT / 16);

    public boolean reloadOnResume = true;
	

	private static GameSettings instance;
    public boolean debugSprites = false;

    public static GameSettings getInstance() {
		if (instance == null) {
			instance = new GameSettings();
		}
		return instance;

	}

}
