package net.tolberts.android.game;

import net.tolberts.android.roboninja.RoboNinjaGame;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class BaseScreen implements Screen {

	public GameState gameState;

	public BaseScreen(GameState state) {
		this.gameState = state;
	}

	protected SpriteBatch getSimpleScreenBatch() {
		SpriteBatch batch = new SpriteBatch();
		batch.getProjectionMatrix().setToOrtho2D(0, 0, RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT);
		return batch;
	}

	protected void setScreen(Screen screen) {
		gameState.game.setScreen(screen);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
