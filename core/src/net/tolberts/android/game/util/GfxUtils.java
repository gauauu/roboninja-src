package net.tolberts.android.game.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GfxUtils {
	
	public static void setBatchAlpha(SpriteBatch batch, float alpha) {
		Color c = batch.getColor();
		batch.setColor(c.r, c.g, c.b, alpha);
	}

}
