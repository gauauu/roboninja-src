package net.tolberts.android.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.security.MessageDigest;

public class MiscUtils {

    public static void assertFalse(boolean val) {
        if (val) {
            RuntimeException exception = new RuntimeException("Assertion failed! val is true");
            exception.printStackTrace();
            throw (exception);
        }
    }

    public static float clamp(float point, float min, float max) {
        return Math.min(Math.max(point, min), max);
    }

    public static Object instantiateFromId(String id, String packageStr) {
        if (id == null) {
            Gdx.app.error(RoboNinjaGame.TAG, "Could not initialize item with null id");
            return null;
        }
        String capitalId = id.substring(0, 1).toUpperCase() + id.substring(1);
        String fullClassName = packageStr + "." + capitalId;
        try {
            return Class.forName(fullClassName).newInstance();
        } catch (Exception e) {
            Gdx.app.log(RoboNinjaGame.TAG, "Could not initialize object " + capitalId);
            e.printStackTrace();
            return null;
        }

    }

    public static String hash(String original) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(original.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        } catch (Exception e) {
            return "";
        }

    }

    public static Vector2 parseFloatPair(String asString) {
        Vector2 result = new Vector2();
        try {
            String[] split = asString.split(",");
            if (split.length != 2) {
                throw new Exception(split.length + " values supplied, 2 needed");
            }
            float x = Float.parseFloat(split[0]);
            float y = Float.parseFloat(split[1]);
            result.set(x, y);
        } catch (Exception e) {
            Gdx.app.error(RoboNinjaGame.TAG, "Could not parse float pair: " + asString);
            Gdx.app.error(RoboNinjaGame.TAG, e.getMessage());
        }
        return result;
    }

    public static String getTimeString(float seconds) {
        return getTimeString((int)seconds);
    }

    public static String getTimeString(int seconds) {
        int hours = seconds / 3600;
        seconds = seconds - (hours * 3600);
        int minutes = seconds / 60;
        seconds = seconds - (minutes * 60);
        String secondsStr = Integer.toString(seconds);
        if (secondsStr.length() == 1) {
            secondsStr = "0" + secondsStr;
        }
        String timeStr = minutes + ":" + secondsStr;
        if (hours > 0) {
            timeStr = hours + ":" + timeStr;
        }

        return timeStr;
    }
}
