package net.tolberts.android.game.util;

import net.tolberts.android.game.GamePrefs;
import net.tolberts.android.game.loaders.Audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AudioPlayer {

    static Music currentMusic;
    static String currentMusicName;
    private static float gapUntilSound = 0;
    private static float secondsUntilStartMusic = -999;

    public final static String PREF_AUDIO_ENABLED = "audio_enabled";
    private static boolean isEnabled = true;

    //ridiculous hack to keep playing sounds to keep the sound engine happy
    private static float keepEngineGoingCounter = 1.0f;

    public static void playMusic(String musicName) {

        if (!isEnabled) {
            return;
        }

        if (musicName.equals(currentMusicName)) {
            return;
        }

        currentMusicName = musicName;
        Music music = Audio.getMusic(musicName);
        MiscUtils.assertFalse(music == null);
        if (music != null) {
            playMusic(music);
        }
    }

    private static void playMusic(Music music) {
        if (!isEnabled) {
            return;
        }


        music.setVolume(0.8f);
        music.setLooping(true);
        if (currentMusic != null) {
            currentMusic.stop();
            currentMusic = music;
            secondsUntilStartMusic = 0.25f;
        } else {
            currentMusic = music;
            startCurrentMusic();
        }
    }

    private static void startCurrentMusic() {
        if (!isEnabled) {
            return;
        }


        currentMusic.play();
        currentMusic.setLooping(true);
        //another attempt to get my music to loop on android
        currentMusic.setOnCompletionListener(new Music.OnCompletionListener() {
            @Override
            public void onCompletion(Music music) {
                music.stop();
                music.play();
            }
        });
    }

    public static void playSound(String soundName) {

        if (!isEnabled) {
            return;
        }

        playSound(soundName, false);
    }

    public static void playSound(String soundName, float volume) {
        if (!isEnabled) {
            return;
        }

        playSound(soundName, false, volume);

    }

    private static void playSound(String soundName, boolean forcePlaying, float volume) {

        if (!isEnabled) {
            return;
        }


        if (gapUntilSound > 0 && (!forcePlaying)) {
            return;
        }
        Sound sound = Audio.getSound(soundName);
        if (sound == null) {
            Gdx.app.error("audio-player", soundName + " returns null sound");
            return;
        }
        gapUntilSound = 0.25f;
        sound.play(volume);
    }

    public static void playSound(String soundName, boolean forcePlaying) {
        playSound(soundName, forcePlaying, 1.0f);
    }

    private static void stopMusic() {
        if (currentMusic != null) {
            currentMusic.stop();
        }
        currentMusic = null;
        currentMusicName = null;
    }

    public static void update(float delta) {
        if (keepEngineGoingCounter < 0 || keepEngineGoingCounter > 1.0f) {
            keepEngineGoingCounter = 1.0f;
            Sound sound = Audio.getSound(Audio.PLAYER_LASER);
            if (sound != null) {
                sound.play(0.001f);
            }
        }
        keepEngineGoingCounter -= delta;

        if (gapUntilSound > 0) {
            gapUntilSound -= delta;
        }
        if (secondsUntilStartMusic > 0) {
            secondsUntilStartMusic -= delta;
            if (secondsUntilStartMusic <= 0) {
                startCurrentMusic();
            }
        }

    }

    public static void initialize() {
        isEnabled = GamePrefs.getBool(AudioPlayer.PREF_AUDIO_ENABLED, true);
        GamePrefs.addListener(new GamePrefs.GamePrefBooleanChangedListener() {
            @Override
            public void prefChanged(boolean newValue) {
                isEnabled = newValue;
                if (!isEnabled) {
                    stopMusic();
                }
            }

            @Override
            public String getPrefToWatch() {
                return AudioPlayer.PREF_AUDIO_ENABLED;
            }
        });
    }


    public static void resetCache() {
        currentMusic = null;
        currentMusicName = null;
        secondsUntilStartMusic = -999;
        gapUntilSound = 0;
    }
}
