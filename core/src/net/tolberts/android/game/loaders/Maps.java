package net.tolberts.android.game.loaders;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class Maps {

	public static void loadInBackground(String mapName) {

        RoboNinjaGame.timedDebug( "started background loading tmx: " + mapName);
        AssetManager am = AsyncLoader.getAssetManager();
		String file = "data/maps/" + mapName + ".tmx";
		am.setLoader(TiledMap.class, new TmxMapLoader(
				new InternalFileHandleResolver()));
		am.load(file, TiledMap.class);
		am.update();
	}

	public static TiledMap loadMap(String mapName) {

		AssetManager am = AsyncLoader.getAssetManager();
		String file = "data/maps/" + mapName + ".tmx";

		// this isn't QUITE right, as it's possible that other things are
		// loading and
		// not just the map. well, we'll see how it goes.
		if (!am.isLoaded(file)) {
            RoboNinjaGame.timedDebug("foreground loading tmx " + mapName);
			loadInBackground(mapName);
		}
		// am.setLoader(TiledMap.class, new ImprovedTmxMapLoader(new
		// InternalFileHandleResolver()));
		// am.load(file, TiledMap.class);
		while (true) {
			am.update();
			if (am.isLoaded(file)) {
                RoboNinjaGame.timedDebug("finished sync loading " + mapName);
				return am.get(file);
			}
            RoboNinjaGame.timedDebug( "waiting for " + mapName + " (or something?)");
			try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// return new ImprovedTmxMapLoader().load("data/maps/" + mapName +
		// ".tmx");
	}

}
