package net.tolberts.android.game.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

import java.util.HashMap;
import java.util.Map;

public class Fonts {

//    public static final String HEADER_FONT = "airborne2";
//    public static final String SMALL_FONT = "orbitron";
	public static final String HEADER_FONT = "commando";
	public static final String SMALL_FONT = "small";
	public static final String SMALL_FONT_BLACK = "small-black";

	public static final float FONT_SCALE_HEADER = 1.0f;
	public static final float FONT_SCALE_MED = 0.75f;
	public static final float FONT_SCALE_SMALL = 0.6f;
	public static final float FONT_SCALE_SMALLER = 0.5f;
	public static final float FONT_SCALE_TINY = 0.4f;
	public static final float FONT_SCALE_TEENY_TINY = 0.3f;
	public static final float FONT_SCALE_HEADER_BUTTONS = 0.5f;


	private static Map<String, BitmapFont> fontCache = new HashMap<String, BitmapFont>();

	public static BitmapFont getFont(String fontName) {
		if (!fontCache.containsKey(fontName)) {
			BitmapFont font = new BitmapFont(Gdx.files.internal("data/fonts/" + fontName + ".fnt"),
					Gdx.files.internal("data/fonts/" + fontName + ".png"), false);
			font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
			fontCache.put(fontName, font);
		}
		return fontCache.get(fontName);
	}

    public static void resetCache() {
        for (BitmapFont bitmapFont : fontCache.values()) {
            bitmapFont.dispose();
        }
        fontCache.clear();
    }

}
