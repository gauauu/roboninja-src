package net.tolberts.android.game.loaders;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class AbilityIcons {

	static Map<String, TextureRegionDrawable> icons = new HashMap<String, TextureRegionDrawable>();
    static Map<String, Texture> textures = new HashMap<String, Texture>();

	public static Drawable getAsDrawable(String id) {

		if (!icons.containsKey(id)) {
			Texture texture = Art.getTexture("ability-btn-" + id);
			TextureRegion textureRegion = new TextureRegion(texture);
			TextureRegionDrawable drawable = new TextureRegionDrawable(textureRegion);
			icons.put(id, drawable);
		}
		return icons.get(id);

	}

    public static void resetCache() {
        icons.clear();
        textures.clear();
    }

	public static Texture getAsTexture(String id) {
        if (id.endsWith("Blank")) {
            return null;
        }
        if (!textures.containsKey(id)) {
            Texture texture = Art.getTexture("ability-btn-" + id);
            textures.put(id, texture);
        }
        return textures.get(id);
	}
}
