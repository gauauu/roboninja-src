package net.tolberts.android.game.loaders;

import com.badlogic.gdx.assets.AssetManager;

public class AsyncLoader {
	
	static AssetManager am = new AssetManager();
	
	public static AssetManager getAssetManager() {
		return am;
	}
	
	public static void update() {
		am.update();
	}

    public static void clearCache() {
//        am.dispose();
        am = new AssetManager();
    }

}
