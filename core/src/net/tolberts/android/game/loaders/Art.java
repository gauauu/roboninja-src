package net.tolberts.android.game.loaders;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Art {

	private static Map<String, Texture> textureCache = new HashMap<String, Texture>();

	public static void initArt() {
		// pack textures -- requires adding gdx.tools to classpath
		// Settings settings = new Settings();
		// settings.maxWidth = 512;
		// settings.maxHeight = 512;
		// TexturePacker2.process(settings, "../images",
		// "../game-android/assets", "game");
	}

	public static Texture getTexture(String imageName) {
		if (!textureCache.containsKey(imageName)) {
			Texture texture = new Texture(Gdx.files.internal("data/textures/"
					+ imageName + ".png"));
            texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
			textureCache.put(imageName, texture);

		}
		return textureCache.get(imageName);

	}

	public static Sprite[] getSprites(String imageName, int width, int height) {
		// probably should cache this....
		TextureRegion[][] regions = TextureRegion.split(getTexture(imageName),
				width, height);
		Sprite[] sprites = new Sprite[regions.length * regions[0].length];
		int spriteCtr = 0;
		for (int i = 0; i < regions.length; i++) {
			for (int j = 0; j < regions[i].length; j++) {
				sprites[spriteCtr] = new Sprite(regions[i][j]);
				spriteCtr++;
			}
		}
		return sprites;
	}

    public static Pixmap getPixmap(String imageName) {
        return new Pixmap(Gdx.files.internal("data/textures/" + imageName + ".png"));
    }

    public static Drawable drawableFromTexture(Texture texture) {
        TextureRegion textureRegion = new TextureRegion(texture);
        return new TextureRegionDrawable(textureRegion);
    }
	
	public static Drawable getAsDrawable(String imageName) {
		Texture texture = getTexture(imageName);
		return drawableFromTexture(texture);
	}

    public static void resetCache() {
        for (Texture texture : textureCache.values()) {
            texture.dispose();
        }
        textureCache.clear();
    }

}
