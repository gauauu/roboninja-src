package net.tolberts.android.game.loaders;

import java.util.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class TextData {

	public static LinkedHashMap<String, String> loadTextData(String fileId) {
        LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        FileHandle fileHandle = Gdx.files.internal("data/text-data/" + fileId
				+ ".txt");
        if (fileHandle == null) {
            Gdx.app.log(RoboNinjaGame.TAG, "could not load text file " + fileId);
            return result;
        }
		Scanner scanner = new Scanner(fileHandle.read());
		while (scanner.hasNext()) {
			String key = scanner.next();
			String value = scanner.nextLine().trim();
			result.put(key, value);
		}
		scanner.close();

		return result;
	}

    public static List<String> loadTextLines(String fileId) {
        FileHandle fileHandle = Gdx.files.internal("data/text-data/" + fileId
                + ".txt");
        List<String> result = new LinkedList<String>();
        if (fileHandle == null) {
            Gdx.app.log(RoboNinjaGame.TAG, "could not load text file " + fileId);
            return result;
        }
        //surely not the most efficient way to do this. But I'm lazy tonight and don't care.
        Scanner scanner = new Scanner(fileHandle.read());
        while (scanner.hasNext()) {
            result.add(scanner.nextLine());
        }
        scanner.close();
        return result;
    }

}
