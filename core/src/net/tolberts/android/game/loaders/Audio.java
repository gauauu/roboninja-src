package net.tolberts.android.game.loaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.util.HashMap;
import java.util.Map;

public class Audio {

    private static Map<String, Sound> soundCache = new HashMap<String, Sound>();
	private static Map<String, Music> musicCache = new HashMap<String, Music>();

    public static final String UI_CLICK = "ui_click.wav";
    public final static String ACQUIRE_ITEM = "acquire_item.wav";
	public final static String CHILI_BITE = "chiliBite.wav";
    public static final String PLAYER_LASER = "playerLaser.mp3";

	public final static String ASSET_DIR = "data/audio/";

	public static Sound getSound(String soundName) {

        if (!soundName.endsWith("mp3") && !soundName.endsWith("wav")){
            soundName = soundName + ".wav";
        }
		if (!soundCache.containsKey(soundName)) {

			FileHandle soundFile = Gdx.files.internal(ASSET_DIR + soundName);
			if (!soundFile.exists()) {
				Gdx.app.error("loaders", "Sound file " + soundName + " doesn't exit");
				return null;
			}
			soundCache.put(soundName, Gdx.audio.newSound(Gdx.files.internal("data/audio/" + soundName)));
		}
		return soundCache.get(soundName);
		/*
		AssetManager am = AsyncLoader.getAssetManager();
		if (!am.isLoaded(ASSET_DIR + soundName)) {
			Gdx.app.error("RoboNinja", "Sound not preloaded: " + soundName);
			return null;
		}
		return am.get(ASSET_DIR + soundName);
		*/
	}

	public static Music getMusic(String musicName) {
		if (!musicCache.containsKey(musicName)) {
			musicCache.put(musicName, Gdx.audio.newMusic(Gdx.files.internal(ASSET_DIR + musicName + ".mp3")));
		}
		return musicCache.get(musicName);
	}

	public static void preloadSfx() {
		getSound(ACQUIRE_ITEM);
		getSound(CHILI_BITE);
        getSound(UI_CLICK);
        getSound(PLAYER_LASER);
		/*
		AssetManager am = AsyncLoader.getAssetManager();
		am.load(ASSET_DIR + ACQUIRE_ITEM, Sound.class);
		am.load(ASSET_DIR + CHILI_BITE, Sound.class);
		*/
	}

    public static void resetCache() {
        for (Sound sound : soundCache.values()) {
            sound.dispose();
        }
        soundCache.clear();
        for (Music music : musicCache.values()) {
            music.dispose();
        }
        musicCache.clear();
    }

}
