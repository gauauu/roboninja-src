package net.tolberts.android.game.platformadapters;


public interface PlayServicesAdapter {

    public static final String ALLOW_PLAY_SERVICES = "allowPlayServices";

    //see https://developers.google.com/android/reference/com/google/android/gms/games/leaderboard/LeaderboardVariant#TIME_SPAN_WEEKLY
    //hardcoding these instead of using 7 layers of indirection to support non-android clients
    public static final int TIME_SPAN_WEEKLY = 1;
    public static final int TIME_SPAN_ALL_TIME = 2;

    public String getPlayServiceLabel();
    public boolean isAvailable();
    public boolean isEnabled();
    public boolean attemptLogin();
    public void logOut();
    public boolean isLoggedIn();

    public void unlockAchievement(String achievement);
    public void showAchievements();
    public void submitLeaderboardScore(String leaderboard, int score, AdapterStringCallback callback);
    public void showLeaderboard(String leaderboard);
    public void showAllLeaderboards();


    public void queryLeaderboardScore(String leaderboard, int timeSpan, AdapterStringCallback callback);


    public interface AdapterStringCallback {
        void setResult(String result);
    }

}
