package net.tolberts.android.game.platformadapters;


import java.util.Set;

public interface PlatformAdapter {

    public final static String FLAG_ALLOW_FAST_MODE = "fast-mode";

    public PlayServicesAdapter getPlayServicesAdapter();

    public boolean usesKeyboard();

    public boolean usesOldGraphics();

    public String overrideStartingLevel();

    public Set<String> getOtherFlags();

}
