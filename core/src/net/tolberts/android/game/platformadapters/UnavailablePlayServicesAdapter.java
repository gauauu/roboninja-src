package net.tolberts.android.game.platformadapters;


import com.badlogic.gdx.Gdx;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class UnavailablePlayServicesAdapter implements PlayServicesAdapter {
    @Override
    public String getPlayServiceLabel() {
        return null;
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    @Override
    public boolean attemptLogin() {
        return false;
    }

    @Override
    public void logOut() {

    }

    @Override
    public boolean isLoggedIn() {
        return false;
    }

    @Override
    public void unlockAchievement(String key) {
        Gdx.app.log(RoboNinjaGame.TAG, "Achievement unlocked: "  + key);
    }

    @Override
    public void showAchievements() {

    }

    @Override
    public void submitLeaderboardScore(String leaderboard, int score, AdapterStringCallback callback) {

    }

    @Override
    public void showLeaderboard(String leaderboard) {

    }

    @Override
    public void showAllLeaderboards() {

    }

    @Override
    public void queryLeaderboardScore(String leaderboard, int timeSpan, AdapterStringCallback callback) {

    }
}
