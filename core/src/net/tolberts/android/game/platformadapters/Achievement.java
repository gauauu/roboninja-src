package net.tolberts.android.game.platformadapters;

public class Achievement {
    public final static String COMPLETE = "COMPLETE";
    public final static String COMPLETE_500 = "COMPLETE_500";
    public final static String COMPLETE_250 = "COMPLETE_250";
    public final static String COMPLETE_50 = "COMPLETE_50";
    public final static String COMPLETE_0 = "COMPLETE_0";
    public final static String ANGUNA = "ANGUNA";
    public static final String ONE_DOWN = "ONE_DOWN" ;
    public static final String COLLECTIBLES = "COLLECTIBLES";

    public final static int[] COMPLETION_ACHIEVEMENTS = {500,250,50,0};
}
