package net.tolberts.android.game.platformadapters;

public class Leaderboard {
    public final static String DEATHS = "DEATHS";
    public final static String TIME = "TIME";
    public final static String DEATHS_HARD = "DEATHS_HARD";
    public final static String TIME_HARD = "TIME_HARD";
}
