package net.tolberts.android.game.parallax;

import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C1;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C2;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C3;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.C4;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.U1;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.U2;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.U3;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.U4;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.V1;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.V2;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.V3;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.V4;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.X1;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.X2;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.X3;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.X4;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.Y1;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.Y2;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.Y3;
import static com.badlogic.gdx.graphics.g2d.SpriteBatch.Y4;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class RepeatingBackgroundRenderer extends OrthogonalTiledMapRenderer {

	private float[] rVertices = new float[20];

	public RepeatingBackgroundRenderer(TiledMap map, float unitScale) {
		super(map, unitScale);
	}

    public void setShader(ShaderProgram shader) {
        spriteBatch.setShader(shader);
    }

	@Override
	public void renderTileLayer(TiledMapTileLayer layer) {

		final float color = Color.toFloatBits(1, 1, 1, layer.getOpacity());

		final int layerWidth = layer.getWidth();
		final int layerHeight = layer.getHeight();

		final float layerTileWidth = layer.getTileWidth() * unitScale;
		final float layerTileHeight = layer.getTileHeight() * unitScale;

		// ok, what I think I need to do, but too tired to do it:
		// change col1 to be just viewbounds.x / layerTileWidth
		// and col2 = that second line (without the min).
		// in other words, we don't care about the bounds of the actual layer
		final int col1 = (int) (viewBounds.x / layerTileWidth) - 1;
		final int col2 = (int) ((viewBounds.x + viewBounds.width + layerTileWidth) / layerTileWidth);

		// then the same thing here
		final int row1 = (int) (viewBounds.y / layerTileHeight);
		final int row2 = (int) ((viewBounds.y + viewBounds.height + layerTileHeight) / layerTileHeight);

		float y = row1 * layerTileHeight;
		float xStart = col1 * layerTileWidth;
		final float[] vertices = this.rVertices;

		for (int row = row1; row < row2; row++) {
			float x = xStart;
			for (int col = col1; col < col2; col++) {
				// then here, we do col mod width, row mod height to get the
				// actual cell.
				int colLookup = col % layerWidth;
				int rowLookup = row % layerHeight;

				if (rowLookup < 0) {
					rowLookup = layerHeight + rowLookup;
				}
				if (colLookup < 0) {
					colLookup = layerWidth + colLookup;
				}

				final TiledMapTileLayer.Cell cell = layer.getCell(colLookup, rowLookup);
				if (cell == null) {
					x += layerTileWidth;
					continue;
				}
				final TiledMapTile tile = cell.getTile();
				if (tile != null) {

					TextureRegion region = tile.getTextureRegion();

					float x1 = x;
					float y1 = y;
					float x2 = x1 + region.getRegionWidth() * unitScale;
					float y2 = y1 + region.getRegionHeight() * unitScale;

					float u1 = region.getU();
					float v1 = region.getV2();
					float u2 = region.getU2();
					float v2 = region.getV();

					vertices[X1] = x1;
					vertices[Y1] = y1;
					vertices[C1] = color;
					vertices[U1] = u1;
					vertices[V1] = v1;

					vertices[X2] = x1;
					vertices[Y2] = y2;
					vertices[C2] = color;
					vertices[U2] = u1;
					vertices[V2] = v2;

					vertices[X3] = x2;
					vertices[Y3] = y2;
					vertices[C3] = color;
					vertices[U3] = u2;
					vertices[V3] = v2;

					vertices[X4] = x2;
					vertices[Y4] = y1;
					vertices[C4] = color;
					vertices[U4] = u2;
					vertices[V4] = v1;

					spriteBatch.draw(region.getTexture(), vertices, 0, 20);
					x += layerTileWidth;
				}
			}
			y += layerTileHeight;
		}
	}

}
