package net.tolberts.android.game.parallax;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;

/**
 * Unused currently, could be cleaned up
 * @author tolbert
 *
 */
public class ParallaxTiledRenderer extends OrthogonalTiledMapRenderer {

	private OrthographicCamera camera;

	public ParallaxTiledRenderer(TiledMap map, float scale) {
		super(map, scale);
	}

	@Override
	public void renderTileLayer(TiledMapTileLayer layer) {
		if (layer.getProperties().get("parallax") != null) {
			ParallaxCamera pcam = (ParallaxCamera) camera;
			Matrix4 parallax = pcam.calculateParallaxMatrix(0.95f, 0.95f);
			spriteBatch.setProjectionMatrix(parallax);
			super.renderTileLayer(layer);
			spriteBatch.setProjectionMatrix(camera.combined);
		} else {
			super.renderTileLayer(layer);
		}
	}

	@Override
	public void setView(OrthographicCamera camera) {
		this.camera = camera;
		spriteBatch.setProjectionMatrix(camera.combined);
		setViewInternal(camera);
	}

	private void setViewInternal(OrthographicCamera camera) {
		float width = camera.viewportWidth * camera.zoom;
		float height = camera.viewportHeight * camera.zoom;
		viewBounds.set(camera.position.x - width / 2, camera.position.y - height / 2, width, height);
	}

}
