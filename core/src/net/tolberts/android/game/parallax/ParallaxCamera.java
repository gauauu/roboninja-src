package net.tolberts.android.game.parallax;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

public class ParallaxCamera extends OrthographicCamera {
	Matrix4 parallaxView = new Matrix4();
	Matrix4 parallaxCombined = new Matrix4();
	Vector3 tmp = new Vector3();
	Vector3 tmp2 = new Vector3();
	
	private float oldX;
	private float oldY;
	
	public ParallaxCamera() {
		super();
	}

	public ParallaxCamera (float viewportWidth, float viewportHeight) {
		super(viewportWidth, viewportHeight);
	}
	
	public void jumpToParallax(float parallaxX, float parallaxY, float offsetX, float offsetY) {
		oldX = position.x;
		oldY = position.y;
		position.x *= parallaxX;
		position.x += offsetX;
		position.y *= parallaxY;
		position.y += offsetY;
		update();
	}
	
	public void resetParallax() {
		position.x = oldX;
		position.y = oldY;
		update();
	}

	public Matrix4 calculateParallaxMatrix (float parallaxX, float parallaxY) {
		update();
		tmp.set(position);
		tmp.x *= parallaxX;
		tmp.y *= parallaxY;

		parallaxView.setToLookAt(tmp, tmp2.set(tmp).add(direction), up);
		parallaxCombined.set(projection);
		Matrix4.mul(parallaxCombined.val, parallaxView.val);
		return parallaxCombined;
	}
}