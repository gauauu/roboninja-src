package net.tolberts.android.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import java.util.LinkedList;
import java.util.List;

public class GamePrefs {
    private static final String PREFS_ID = "prefs";

    private static List<GamePrefBooleanChangedListener> booleanChangedListenerList = new LinkedList<GamePrefBooleanChangedListener>();

    public static void setPref(String pref, String value) {
        Preferences prefs = Gdx.app.getPreferences(PREFS_ID);
        prefs.putString(pref, value);
        prefs.flush();
    }

    public static String getPref(String pref) {
        Preferences prefs = Gdx.app.getPreferences(PREFS_ID);
        return prefs.getString(pref);
    }

    public static void setBool(String pref, boolean value) {
        Preferences prefs = Gdx.app.getPreferences(PREFS_ID);
        prefs.putBoolean(pref, value);
        prefs.flush();
        for (GamePrefBooleanChangedListener listener : booleanChangedListenerList) {
            if (pref.equals(listener.getPrefToWatch())) {
                listener.prefChanged(value);
            }
        }
    }

    public static boolean getBool(String pref, boolean defaultVal) {
        Preferences prefs = Gdx.app.getPreferences(PREFS_ID);
        return prefs.getBoolean(pref, defaultVal);
    }

    public static void addListener(GamePrefBooleanChangedListener listener) {
        booleanChangedListenerList.add(listener);
    }

    public interface GamePrefBooleanChangedListener {
        public void prefChanged(boolean newValue);
        public String getPrefToWatch();
    }
}
