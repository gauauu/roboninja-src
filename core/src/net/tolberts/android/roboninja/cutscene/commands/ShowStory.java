package net.tolberts.android.roboninja.cutscene.commands;

@SuppressWarnings("UnusedDeclaration")
public class ShowStory extends Command {


    private String storyText;

    @Override
    public void finish(float timer) {
        //and...we still don't do anything.
    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {
        cutscene.showStory = true;
        cutscene.storyText = storyText;
        cutscene.dismissStory = false;
    }

    @Override
    protected void setParameter(String param) {
        storyText = param;
    }

    @Override
    public boolean isFinished(float time) {
        return cutscene.dismissStory;
    }
}
