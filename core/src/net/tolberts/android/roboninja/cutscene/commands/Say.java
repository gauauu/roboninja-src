package net.tolberts.android.roboninja.cutscene.commands;

public class Say extends Command {

    public final static String ID = "say";

    @Override
    protected void setParameter(String part) {

    }

    @Override
    public boolean isFinished(float time) {
        return false;
    }

    @Override
    public void finish(float timer) {

    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {

    }
}
