package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.Gdx;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.cutscene.Cutscene;
import net.tolberts.android.roboninja.cutscene.Puppet;

public abstract class Command {


    protected Puppet puppet;
    protected Cutscene cutscene;
    public String rawCommand;

    public static Command parseCommand(String commandString, Cutscene cutscene) {

        if (commandString.trim().length() == 0) {
            return null;
        }

        String[] parts = commandString.split("\\s+", 3);

        Puppet puppet = cutscene.getPuppet(parts[0]);
        Command command = createCommandFor(parts[1]);
        if (command == null) {
            Gdx.app.error(RoboNinjaGame.TAG, "Command " + parts[1] + " not defined");
            throw new RuntimeException();
        }
        command.setParameter(parts[2]);
        command.setTarget(puppet);
        command.setCutscene(cutscene);
        command.rawCommand = commandString;

        return command;
    }



    protected abstract void setParameter(String part);

    private static Command createCommandFor(String action) {
        return (Command) MiscUtils.instantiateFromId(action, "net.tolberts.android.roboninja.cutscene.commands");

    }


    public abstract boolean isFinished(float time);

    public abstract void finish(float timer);

    public abstract void update(float timer, float delta);

    public abstract void start(float timer);

    public void setTarget(Puppet target) {
        this.puppet = target;
    }

    public void setCutscene(Cutscene cutscene) {
        this.cutscene = cutscene;
    }





}
