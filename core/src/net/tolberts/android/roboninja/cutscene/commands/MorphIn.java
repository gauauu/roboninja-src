package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.Gdx;
import net.tolberts.android.roboninja.RoboNinjaGame;

@SuppressWarnings("UnusedDeclaration")
public class MorphIn extends Command {


    private float duration;
    protected float startTime;

    protected float originalX;
    protected float originalY;

    @Override
    protected void setParameter(String part) {
        try {
            duration = Float.parseFloat(part);
        } catch (NumberFormatException e) {
            Gdx.app.error(RoboNinjaGame.TAG, "MorphIn parse fail:command needs a float as param");
        }
    }


    @Override
    public boolean isFinished(float time) {
        return (time > startTime + duration);
    }

    @Override
    public void finish(float timer) {
        puppet.visible = true;
        puppet.alpha = 1.0f;
    }

    @Override
    public void update(float timer, float delta) {

        float pctFinished = (duration - (timer - startTime)) / duration;
        puppet.alpha = 1.0f - pctFinished;


    }

    @Override
    public void start(float timer) {
        startTime = timer;
        puppet.alpha = 0.0f;
    }
}
