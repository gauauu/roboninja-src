package net.tolberts.android.roboninja.cutscene.commands;

public interface CommandGroup {
    public void addCommand(Command command);
    public boolean isTakingCommands();
}
