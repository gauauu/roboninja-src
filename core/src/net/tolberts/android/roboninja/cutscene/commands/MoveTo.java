package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.cutscene.Puppet;

@SuppressWarnings("UnusedDeclaration")
public class MoveTo extends Command {


    float destX = 0;
    float destY = 0;
    private float duration;

    protected float startTime;
    float speedX = 0;
    float speedY = 0;

    @Override
    protected void setParameter(String part) {
        String[] split = part.split(",");
        if (split.length != 3) {
            Gdx.app.error(RoboNinjaGame.TAG, "Moveto command needs a format of x,y,t (" + part + ")");
        }
        try {
            destX = Float.parseFloat(split[0]);
            destY = Float.parseFloat(split[1]);
            duration = Float.parseFloat(split[2]);
        } catch (NumberFormatException e) {
            Gdx.app.error(RoboNinjaGame.TAG, "Moveto parse fail:command needs a format of x,y,t");
        }
    }

    protected void calculateSpeed() {
        Vector2 start = puppet.getPosition();
        float deltaX = destX - start.x;
        float deltaY = destY - start.y;
        speedX = deltaX / duration;
        speedY = deltaY / duration;

    }

    @Override
    public boolean isFinished(float time) {
        return (time > startTime + duration);
    }

    @Override
    public void finish(float timer) {
        puppet.setCurrentAnimation(Puppet.STATE_IDLE);
    }

    @Override
    public void update(float timer, float delta) {
        Vector2 position = puppet.getPosition();
        position.x += speedX * delta;
        position.y += speedY * delta;
        puppet.setPosition(position);
    }

    @Override
    public void start(float timer) {
        startTime = timer;
        puppet.setCurrentAnimation(Puppet.STATE_WALKING);
        calculateSpeed();
        puppet.setFacing(speedX > 0);
    }
}
