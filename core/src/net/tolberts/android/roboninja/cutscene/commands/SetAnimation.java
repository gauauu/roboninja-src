package net.tolberts.android.roboninja.cutscene.commands;

@SuppressWarnings("UnusedDeclaration")
public class SetAnimation extends Command {

    private String animation;

    @Override
    protected void setParameter(String param) {
        animation = param;
    }

    @Override
    public boolean isFinished(float time) {
        return true;
    }

    @Override
    public void finish(float timer) {

    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {
        puppet.setCurrentAnimation(animation);
    }
}
