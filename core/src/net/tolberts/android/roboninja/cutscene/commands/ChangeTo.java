package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.cutscene.Puppet;

@SuppressWarnings("UnusedDeclaration")
public class ChangeTo extends MoveTo {


    @Override
    public void start(float timer) {
        startTime = timer;
        puppet.setCurrentAnimation(Puppet.STATE_WALKING);
        calculateSpeed();
    }
}
