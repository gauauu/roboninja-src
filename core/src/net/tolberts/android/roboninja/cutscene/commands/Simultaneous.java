package net.tolberts.android.roboninja.cutscene.commands;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("UnusedDeclaration")
public class Simultaneous extends Command implements CommandGroup{

    List<Command> commands;
    private int numCommands;

    @Override
    protected void setParameter(String part) {
        numCommands = Integer.parseInt(part);
        commands = new ArrayList<Command>(numCommands);
    }

    @Override
    public boolean isFinished(float time) {
        for (Command command : commands) {
            if (!command.isFinished(time)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void finish(float timer) {
        for (Command command : commands) {
            command.finish(timer);
        }
    }

    @Override
    public void update(float timer, float delta) {
        for (Command command : commands) {
            command.update(timer, delta);
        }
    }

    @Override
    public void start(float timer) {
        for (Command command : commands) {
            command.start(timer);
        }
    }

    public boolean isTakingCommands() {
        return commands.size() != numCommands;
    }

    @Override
    public void addCommand(Command command) {
        commands.add(command);

    }

}
