package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.Gdx;
import net.tolberts.android.roboninja.RoboNinjaGame;

@SuppressWarnings("UnusedDeclaration")
public class Wait extends Command {

    public final static String ID = "wait";
    private float startTime;
    private float waitTime;


    @Override
    public void finish(float timer) {
        //and...we still don't do anything.
    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {
        startTime = timer;
    }

    @Override
    protected void setParameter(String param) {
        try {
            waitTime = Float.parseFloat(param);
        } catch (NumberFormatException e) {
            Gdx.app.error(RoboNinjaGame.TAG, "Couldn't parse time for wait command " + param, e);
        }
    }

    @Override
    public boolean isFinished(float time) {
        return (time > startTime + waitTime);
    }
}
