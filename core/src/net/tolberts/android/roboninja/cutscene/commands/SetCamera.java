package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.util.MiscUtils;

public class SetCamera extends Command {
    private Vector2 cameraPosition;

    @Override
    protected void setParameter(String param) {
         cameraPosition = MiscUtils.parseFloatPair(param);
    }

    @Override
    public boolean isFinished(float time) {
        return true;
    }

    @Override
    public void finish(float timer) {

    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {
        cutscene.setCameraPosition(cameraPosition);
    }
}
