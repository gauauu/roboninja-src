package net.tolberts.android.roboninja.cutscene.commands;

import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.util.MiscUtils;

@SuppressWarnings("UnusedDeclaration")
public class SetPosition extends Command {

    private Vector2 position;
    private boolean faceRight = false;

    @Override
    protected void setParameter(String param) {
        String[] split = param.split(",", 2);
        if (split[0].startsWith("r")) {
            faceRight = true;
        }

        position = MiscUtils.parseFloatPair(split[1]);
    }

    @Override
    public boolean isFinished(float time) {
        return true;
    }

    @Override
    public void finish(float timer) {

    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {
        puppet.setFacing(faceRight);
        puppet.setPosition(position);
    }
}
