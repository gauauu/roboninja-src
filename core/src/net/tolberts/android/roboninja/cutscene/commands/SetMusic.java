package net.tolberts.android.roboninja.cutscene.commands;

import net.tolberts.android.game.util.AudioPlayer;

@SuppressWarnings("UnusedDeclaration")
public class SetMusic extends Command {
    private String musicId;

    @Override
    protected void setParameter(String param) {
         musicId = param;
    }

    @Override
    public boolean isFinished(float time) {
        return true;
    }

    @Override
    public void finish(float timer) {

    }

    @Override
    public void update(float timer, float delta) {

    }

    @Override
    public void start(float timer) {
        AudioPlayer.playMusic(musicId);
    }
}
