package net.tolberts.android.roboninja.cutscene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.loaders.TextData;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.cutscene.commands.Command;
import net.tolberts.android.roboninja.cutscene.commands.CommandGroup;

import java.util.*;

public class Cutscene {

    private final CutsceneFinishedCallback callback;
    public String levelId;
    public String music;
    private Map<String, Puppet> puppets;

    Vector2 cameraPosition = new Vector2(0, 0);

    protected float timer = 0.0f;
    private List<Command> commands;
    private int currentCommand = 0;
    private boolean isPaused = false;
    private boolean isFinished;
    private boolean firstTimeThrough;

    public boolean showStory = false;
    public String storyText = "";
    public boolean dismissStory = false;


    public Cutscene(String cutsceneId, CutsceneFinishedCallback callback) {
        firstTimeThrough = true;
        this.callback = callback;
        String fileId = "cutscene_" + cutsceneId;
        List<String> strings = TextData.loadTextLines(fileId);
        LinkedHashMap<String, String> textData = TextData.loadTextData(fileId);
        parseCutsceneData(textData, strings);
    }

    private void parseCutsceneData(Map<String, String> params, List<String> script) {
        levelId = params.get("level");
        parsePuppets(params.get("puppets"));
        parseCommands(script);
    }

    private void parseCommands(List<String> script) {
        boolean started = false;
        commands = new ArrayList<Command>(script.size());
        CommandGroup currentGroup = null;
        for (String s : script) {

            //the first stuff is parsed as key/val "textdata", so we don't
            //start parsing commands until we get to the --SCRIPT-- tag.
            if ("--SCRIPT--".equals(s)) {
                started = true;
                continue;
            }
            if (!started) {
                continue;
            }

            Command command = Command.parseCommand(s, this);


            if (command != null) {

                if (command instanceof CommandGroup) {
                    currentGroup = (CommandGroup) command;
                    commands.add(command);
                } else if (currentGroup != null && currentGroup.isTakingCommands()) {
                    currentGroup.addCommand(command);
                } else {
                    commands.add(command);
                }

            }

        }

    }

    public void setPaused(boolean paused) {
        isPaused = paused;
    }

    private void parsePuppets(String puppetsList) {
        String[] puppetIds = puppetsList.split(",");
        puppets = new HashMap<String, Puppet>();
        for (String puppetId : puppetIds) {
            puppets.put(puppetId, new Puppet(puppetId));
        }
    }

    public Vector2 getCameraPosition() {
        return cameraPosition;
    }

    public Collection<Puppet> getPuppets() {
        return puppets.values();
    }

    public void update(float delta) {
        if (!isPaused) {
            timer += delta;
        }

        for (Puppet puppet : puppets.values()) {
            puppet.updateCharacter(delta);
        }

        if (currentCommand == commands.size()) {
            isFinished = true;
            return;
        }
        Command command = commands.get(currentCommand);

        if (firstTimeThrough) {
            command.start(timer);
            firstTimeThrough = false;
        }

        command.update(timer, delta);


        while (command.isFinished(timer)) {
            command.finish(timer);
            currentCommand++;
            if (commands.size() > currentCommand) {
                command = commands.get(currentCommand);

                try {
                    command.start(timer);
                } catch (Exception e) {
                    Gdx.app.error(RoboNinjaGame.TAG, command.rawCommand);
                    e.printStackTrace();
                }
            } else {
                callback.cutsceneFinished();
                break;
            }
        }


    }

    public void skip() {
        isFinished = true;
        callback.cutsceneFinished();
    }

    public boolean isFinished() {
        return isFinished;
    }

    public Puppet getPuppet(String puppetId) {
        return puppets.get(puppetId);
    }

    public void setCameraPosition(Vector2 cameraPosition) {
        this.cameraPosition = cameraPosition;
    }
}
