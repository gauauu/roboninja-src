package net.tolberts.android.roboninja.cutscene;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.NPC;
import net.tolberts.android.game.loaders.TextData;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.LinkedHashMap;
import java.util.Map;

public class Puppet extends NPC {

    private static final String PROP_TEXTURE_NAME = "textureName";
    private static final String PROP_FRAME_SIZE = "frameSize";
    private static final String PROP_ANIM_TYPE = "animType";
    private final String puppetId;
    private final String puppetDef;


    private String textureName;
    boolean loadedProps = false;

    public final static String STATE_IDLE = "idle";
    public final static String STATE_WALKING = "walking";
    private Vector2 frameSize;

    public boolean isMirrored = false;
    private int[] framesX;
    private int[] framesY;
    private float animSpeed = 0.3f;
    private TextureRegion[] frameDefs;

    private Animation.PlayMode playmode = Animation.PlayMode.LOOP_PINGPONG;


    public Puppet(String puppetId) {
        this.puppetId = puppetId;
        if (puppetId.contains(".")) {
            String[] split = puppetId.split("\\.");
            this.puppetDef = split[0];
        } else {
            this.puppetDef = puppetId;
        }
        gameSettings = GameSettings.getInstance();

        loadPuppetData();
        initGraphics();
        setCurrentAnimation(STATE_IDLE);
    }

    @Override
    protected boolean shouldInitGraphicsOnCreate() {
        return false;
    }

    private void loadPuppetData() {
        if (!loadedProps) {
            LinkedHashMap<String, String> data = TextData.loadTextData("puppet_" + puppetDef);
            frameSize = MiscUtils.parseFloatPair(data.get(PROP_FRAME_SIZE));
            if (data.containsKey("textureScale")) {
                textureScale = MiscUtils.parseFloatPair(data.get("textureScale"));
            }
            textureName = data.get(PROP_TEXTURE_NAME);
            if (textureName == null) {
                Gdx.app.error(RoboNinjaGame.TAG, "no texture name specified in puppet_" + puppetDef);
            }
            if (data.containsKey("hFlip")) {
                isMirrored = true;
            }

            if (data.containsKey(PROP_ANIM_TYPE)) {
                if ("once".equals(data.get(PROP_ANIM_TYPE))){
                    playmode = Animation.PlayMode.NORMAL;
                }
            }

            framesX = new int[]{0, 1, 2};
            framesY = new int[]{0, 0, 0};
            if (data.containsKey("frames")) {
                try {
                    String[] frameDefStrs = data.get("frames").split(" ");
                    int i = 0;
                    framesX = new int[frameDefStrs.length];
                    framesY = new int[frameDefStrs.length];

                    for (String frameDef : frameDefStrs) {
                        //the TextureRegions haven't been initialized yet, so just store ints and
                        //build the array of textureregions later
                        String[] split = frameDef.split(",", 2);
                        framesX[i] = Integer.parseInt(split[0]);
                        framesY[i] = Integer.parseInt(split[1]);

                        i++;
                    }
                } catch (Exception e) {
                    //could be number format, array out of bounds, etc. Either way, barf.
                    Gdx.app.error(RoboNinjaGame.TAG, "Could not parse puppet frames for " + puppetDef, e);
                }
            }
            if (data.containsKey("animSpeed")) {
                try {
                animSpeed = Float.parseFloat(data.get("animSpeed"));
                } catch (Exception e) {
                    Gdx.app.error(RoboNinjaGame.TAG, "Could not parse animSpeed for " + puppetDef);
                }
            }
        }
    }


    @Override
    public void updateCharacter(float delta) {
        animationTick += delta;

    }

    public void setFacing(boolean right) {
        hFlip = right;
        if (isMirrored) {
            hFlip = !hFlip;
        }
    }

    @Override
    public void collideWithMc(GameState state) {
        //we don't care, we live in a different world
    }

    @Override
    protected String getTextureName() {
        loadPuppetData();
        return textureName;
    }

    @Override
    protected Vector2 getFrameSize() {
        loadPuppetData();
        return frameSize;
    }


    @Override
    protected void initAnimations(Map<String, Animation> animations) {

        //build frameDefs array
        if (frameDefs == null) {
            frameDefs = new TextureRegion[framesX.length];
            int i = 0;
            while (i < framesX.length) {
                int x = framesX[i];
                int y = framesY[i];
                try {
                    frameDefs[i] = textureRegions[y][x];
                } catch (IndexOutOfBoundsException e) {
                    Gdx.app.error(RoboNinjaGame.TAG, "Error init'ing animations for " + puppetId + ", textureRegion " + y + "," + x + " out of bounds", e);
                }
                i++;
            }
        }

        //every puppet will have a 1-frame "standing" and a 2-frame "moving" animation?
        try {
            Animation idle = new Animation(0.10f, frameDefs[0]);
            idle.setPlayMode(Animation.PlayMode.NORMAL);
            animations.put(STATE_IDLE, idle);

            try {
                Animation walking = new Animation(animSpeed, frameDefs);

//                        textureRegions[0][1], textureRegions[0][2]);
                walking.setPlayMode(playmode);
                animations.put(STATE_WALKING, walking);
            } catch (ArrayIndexOutOfBoundsException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Texture Regions out of bounds for " + puppetId + " -- defaulting to idle");
                animations.put(STATE_WALKING, idle);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            Gdx.app.error(RoboNinjaGame.TAG, "Texture Regions out of bounds for " + puppetId);
            e.printStackTrace();

        }
    }

    protected void initFromScript(String textDataId) {
        LinkedHashMap<String, String> textData = TextData.loadTextData(textDataId);
        textureName = textData.get(PROP_TEXTURE_NAME);
        String frameSizeString = textData.get(PROP_FRAME_SIZE);
        if (frameSizeString == null) {
            Gdx.app.log(RoboNinjaGame.TAG, "No frame size in puppet script " + textDataId);
            return;
        }
        String[] split = frameSizeString.split(",");
        try {
            float width = Float.parseFloat(split[0]);
            float height = Float.parseFloat(split[1]);
            frameSize = new Vector2(width, height);
        } catch (NumberFormatException e) {
            Gdx.app.log(RoboNinjaGame.TAG, "Can't parse frame frame size in puppet script " + textDataId);
        }
    }

}
