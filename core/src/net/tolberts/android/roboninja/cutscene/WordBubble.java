package net.tolberts.android.roboninja.cutscene;

import com.badlogic.gdx.scenes.scene2d.Group;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.ParagraphActor;

public class WordBubble extends Group {

    private final ParagraphActor storyLabel;
    private final ActorFrame frame;
    private String text;

    public final static int PER_LINE = 17;

    public WordBubble() {

        storyLabel = new ParagraphActor(" ", Fonts.SMALL_FONT_BLACK, PER_LINE - 8);
        storyLabel.setPosition(10, getHeight() - 10);
        storyLabel.setFontScale(Fonts.FONT_SCALE_TINY);

        frame = new ActorFrame();
//        frame.setStyle(ActorFrame.STYLE_GRAY_FRAME);
        frame.setStyle(ActorFrame.STYLE_GREEN_FRAME);
        frame.setBoundsByMargin(20);
        addActor(frame);
        frame.addActor(storyLabel);

    }

    public void setText(String text) {
        this.text = text;
        String newText = text.replace("\\n", "\n");
        String[] split = newText.split("\n");
        int numLines = split.length;
        int frameHeight = PER_LINE * numLines + 30;
        frameHeight = Math.max(64, frameHeight) + 64;
        frame.setHeight(frameHeight);
        frame.setPosition(20, RoboNinjaGame.HEIGHT - 30 - frameHeight);
        storyLabel.setText(newText);
        storyLabel.centerOn(frame);

    }
}
