package net.tolberts.android.roboninja.cutscene;

public interface CutsceneFinishedCallback {
    public void cutsceneFinished();
}
