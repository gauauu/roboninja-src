package net.tolberts.android.roboninja;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import net.tolberts.android.game.GameProgress;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.characters.ItemCharacter;
import net.tolberts.android.roboninja.mc.AbilityFactory;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.mc.abilities.air.AirAbility;
import net.tolberts.android.roboninja.mc.abilities.air.BlankAirAbility;
import net.tolberts.android.roboninja.mc.abilities.consumable.ConsumableAbility;
import net.tolberts.android.roboninja.mc.abilities.wall.BlankWallAbility;
import net.tolberts.android.roboninja.mc.abilities.wall.WallAbility;
import net.tolberts.android.roboninja.minimap.MinimapTracker;

import java.util.*;

public class RoboNinjaProgress implements GameProgress {

    public static final String PROGRESS_PREFS_ID = "roboninja-progress";
    public static final String ABILITIES = "abilities";
    public static final String RESPAWN = "respawn";
    public static final String FLAGS = "flags";
    public static final String CONSUMABLES = "consumables";
    private static final String MINIMAP = "minimap";
    private static final String TELEPORTERS = "teleporters";
    private static final String DEATHS = "deaths";
    private static final String CHECKSUM = "extra";
    private static final String TIME = "time";
    private static final String SPEED_MODE = "smode";

    private static final int CHEAT_FLAG = 98765;


    public static final String COLLECTIBLES = "collectibles";
    public static final int MAX_COLLECTIBLES = 15;

    IntMap<LinkedList<McAbility>> abilities = new IntMap<LinkedList<McAbility>>();
    Set<String> flags = new LinkedHashSet<String>();
    ObjectIntMap<String> consumables = new ObjectIntMap<String>();
    ObjectIntMap<String> consumablesMax = new ObjectIntMap<String>();
    Map<String, Float> consumableRespawn = new HashMap<String, Float>();
    LinkedHashSet<TeleportLocation> teleporters = new LinkedHashSet<TeleportLocation>();
    int deaths = 0;
    float time = 0;

    int numCollectibles = 0;


    // respawn place
    public String levelId;
    public float respawnX;
    public float respawnY;
    public int respawnFacing;

    public boolean isPortableRespawning;
    public float portableRespawnX;
    public float portableRespawnY;
    public int portableRespawnFacing;

    public MinimapTracker minimapTracker = new MinimapTracker();
    private int speedMode = GameState.SPEED_MODE_NORMAL;

    public void addPlayTime(float seconds) {
        time += seconds;
    }

    public LinkedList<McAbility> getAbilitiesOfType(int type) {

        LinkedList<McAbility> abilityList = abilities.get(type);
        if (abilityList == null) {
            abilityList = new LinkedList<McAbility>();
            abilities.put(type, abilityList);
        }
        return abilityList;
    }

    public void flagDeath() {
        deaths++;
    }

    public int acquireCollectible(ItemCharacter sourceCharacter) {
        if (sourceCharacter != null) {
            flags.add(sourceCharacter.getFlag());
        }
        numCollectibles++;
        return numCollectibles;
    }

    public void acquireAbility(McAbility ability, boolean incrementConsumables) {
        acquireAbility(ability, incrementConsumables, null);
    }

    public void acquireAbility(McAbility ability, ItemCharacter sourceCharacter) {
        acquireAbility(ability, true, sourceCharacter);
    }

    public void acquireAbility(McAbility ability, boolean incrementConsumables, ItemCharacter sourceCharacter) {


        int type = ability.getType();
        LinkedList<McAbility> ofType = abilities.get(type);


        if (ofType == null) {
            ofType = new LinkedList<McAbility>();
            abilities.put(type, ofType);
        }

        //if this isn't consumable and we already have it, don't add it. Otherwise add it.
        if (type == McAbility.TYPE_CONSUMABLE || !(abilities.get(type).contains(ability))) {
            ofType.add(ability);
        }

        if (incrementConsumables && ability instanceof ConsumableAbility) {
            consumables.put(ability.getId(), consumables.get(ability.getId(), 0) + ((ConsumableAbility) ability).getNumberGained());
            consumablesMax.put(ability.getId(), consumablesMax.get(ability.getId(), 0) + ((ConsumableAbility) ability).getNumberGained());

            if (sourceCharacter != null) {
                flags.add(sourceCharacter.getFlag());
            }
        } else {
            flags.add(ability.getId());
        }

        //wall abilities need to also have the "blank" wall ability
        if (ability instanceof WallAbility &&
                !BlankWallAbility.ID.equals(ability.getId())) {
            LinkedList<McAbility> wallAbilities = abilities.get(McAbility.TYPE_WALL);
            if (wallAbilities.size() == 1) {
                wallAbilities.addFirst(AbilityFactory.getAbility(BlankWallAbility.ID));
            }
        } else if (ability instanceof AirAbility &&
                !BlankAirAbility.ID.equals(ability.getId())) {
            //and air abilities need to also have the "blank" wall ability
            LinkedList<McAbility> airAbilities = abilities.get(McAbility.TYPE_JUMP);
            if (airAbilities.size() == 1) {
                airAbilities.addFirst(AbilityFactory.getAbility(BlankAirAbility.ID));
            }
        }

    }

    void setRespawnPoint(String level, float x, float y, int facing) {
        RoboNinjaGame.timedDebug("set respawn point (which forces a save)");
        respawnX = x;
        respawnY = y;
        respawnFacing = (int) Math.signum(facing);
        //noinspection StatementWithEmptyBody
        if (level != null && level.equals(levelId)) {
            //keep portable respawn if it's there
        } else {
            isPortableRespawning = false;
        }
        levelId = level;

        saveProgress();
    }

    public void setPortableRespawn(float x, float y, int facing) {
        isPortableRespawning = true;
        portableRespawnX = x;
        portableRespawnY = y;
        portableRespawnFacing = facing;
    }

    public void saveProgress() {
        RoboNinjaGame.timedDebug("start saveProgress");
        Preferences prefs = Gdx.app.getPreferences(PROGRESS_PREFS_ID);
        prefs.putString(ABILITIES, getAbilityList());
        prefs.putString(RESPAWN, getRespawnString());
        prefs.putString(FLAGS, getFlagList());
        prefs.putString(CONSUMABLES, getConsumablesList());
        prefs.putString(MINIMAP, minimapTracker.serializeToString());
        prefs.putString(TELEPORTERS, getTeleporterListString());
        prefs.putInteger(DEATHS, deaths);
        prefs.putFloat(TIME, time);
        prefs.putInteger(SPEED_MODE, speedMode);
        prefs.putString(CHECKSUM, calcChecksum());
        prefs.putInteger(COLLECTIBLES, numCollectibles);
        prefs.flush();
        RoboNinjaGame.timedDebug("done saveProgress");
    }

    private String calcChecksum() {
        String base = getAbilityList() + "roboninja" + getRespawnString() + getFlagList() + getConsumablesList() + getTeleporterListString() + deaths + time + "abadabacrabasaba" + speedMode;
        return MiscUtils.hash(base);
    }


    public void loadProgress() {
        RoboNinjaGame.timedDebug("start loadProgress");
        Preferences prefs = Gdx.app.getPreferences(PROGRESS_PREFS_ID);
        rebuildAbilities(prefs.getString(ABILITIES));
        rebuildRespawn(prefs.getString(RESPAWN));
        rebuildFlags(prefs.getString(FLAGS));
        rebuildConsumables(prefs.getString(CONSUMABLES));
        rebuildTeleporters(prefs.getString(TELEPORTERS));
        deaths = prefs.getInteger(DEATHS, CHEAT_FLAG);
        time = prefs.getFloat(TIME, CHEAT_FLAG);
        minimapTracker.initializeFromString(prefs.getString(MINIMAP));
        speedMode = prefs.getInteger(SPEED_MODE, GameState.SPEED_MODE_NORMAL);
        numCollectibles = prefs.getInteger(COLLECTIBLES, 0);
        String checksum = prefs.getString(CHECKSUM);
        if (!checksum.equals(calcChecksum())) {
            AudioPlayer.playSound(Audio.CHILI_BITE);
            Gdx.app.error(RoboNinjaGame.TAG, "BAD CHECKSUM");
            deaths = CHEAT_FLAG;
            time = CHEAT_FLAG;
        }

        RoboNinjaGame.timedDebug("done loadProgress");
    }

    public static boolean hasProgress() {
        Preferences prefs = Gdx.app.getPreferences(PROGRESS_PREFS_ID);
        String string = prefs.getString(RESPAWN);
        if ((string == null) || ("".equals(string))) {
            return false;
        }

        if (string.startsWith("null")) {
            return false;
        }
        String[] split = string.split(",");
        return split.length >= 4;
    }


    private String getRespawnString() {
        String result = levelId + "," + respawnX + "," + respawnY + "," + respawnFacing;
        if (isPortableRespawning) {
            result = result + "/" + portableRespawnX + "," + portableRespawnY + "," + portableRespawnFacing;
        } else {
            result = result + "/X";
        }
        return result;
    }

    private void rebuildRespawn(String string) {
        String[] majorParts = string.split("/");

        String[] primaryRespawn = majorParts[0].split(",");
        levelId = primaryRespawn[0];
        try {
            respawnX = Float.parseFloat(primaryRespawn[1]);
            respawnY = Float.parseFloat(primaryRespawn[2]);
            respawnFacing = Integer.parseInt(primaryRespawn[3]);

            if (majorParts.length < 2 || "X".equals(majorParts[1])) {
                isPortableRespawning = false;
                return;
            }

            String[] portableParts = majorParts[1].split(",");
            portableRespawnX = Float.parseFloat(portableParts[0]);
            portableRespawnY = Float.parseFloat(portableParts[1]);
            if (portableParts.length > 2) {
                portableRespawnFacing = Integer.parseInt(portableParts[2]);
            }
            isPortableRespawning = true;

        } catch (NumberFormatException e) {
            Gdx.app.error(RoboNinjaGame.TAG, "Could not parse respawn: " + string);
        }

    }

    private void rebuildAbilities(String string) {
        String[] split = string.split(",");
        abilities.clear();
        for (String abilityId : split) {
            //don't try to build a blank ability
            if (abilityId == null || "".equals(abilityId)) {
                continue;
            }
            McAbility ability = AbilityFactory.getAbility(abilityId);
            if (ability != null) {
                acquireAbility(ability, false);
            }
        }
    }

    private String getAbilityList() {
        String abilitiesString = "";
        for (LinkedList<McAbility> abilityList : abilities.values()) {
            for (McAbility ability : abilityList) {
                abilitiesString = abilitiesString + ability.getId() + ",";
            }
        }
        return abilitiesString;
    }

    private void rebuildConsumables(String string) {
        consumables.clear();
        String[] split = string.split(",");
        for (String line : split) {
            if (line == null || "".equals(line.trim())) {
                continue;
            }
            String[] parts = line.split(":");
            if (parts.length != 4) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse line for consumables: " + line);
                continue;
            }
            try {
                int quantity = Integer.parseInt(parts[1]);
                consumables.put(parts[0], quantity);

                consumablesMax.put(parts[0], Integer.parseInt(parts[2]));
                consumableRespawn.put(parts[0], Float.valueOf(parts[3]));

            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse line for consumables: " + line);
            }
        }

    }

    private String getConsumablesList() {
        String consumablesString = "";
        for (String key : consumables.keys()) {
            int number = consumables.get(key, 0);
            int max = consumablesMax.get(key, 0);
            float respawn = 0;
            try {
                respawn = consumableRespawn.get(key);
            } catch (NullPointerException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "null consumableRespawn, this should never happen");
            }
            consumablesString = consumablesString + key + ":" + number + ":" + max + ":" + respawn + ",";
        }
        return consumablesString;
    }

    private void rebuildFlags(String string) {
        flags.clear();
        String[] split = string.split(",");
        for (String flag : split) {
            if (flag == null) {
                continue;
            }
            if ("".equals(flag.trim())) {
                continue;
            }

            try {
                flags.add(flag);
            } catch (NumberFormatException e) {
                // just ignore it if we can't parse it
            }

        }

    }

    private String getFlagList() {
        String flagString = "";
        for (String flag : flags) {
            flagString = flagString + flag + ",";
        }
        return flagString;
    }

    @Override
    public boolean isFlagSet(String flag) {
        return flags.contains(flag);
    }

    @Override
    public void setSpeedMode(int speedMode) {
        this.speedMode = speedMode;
    }

    @Override
    public int getSpeedMode() {
        return this.speedMode;
    }

    public void setFlag(String flag) {
        flags.add(flag);
    }

    public boolean hasConsumable(String id) {
        return getNumberOfConsumable(id) > 0;
    }

    public void gainConsumable(String id, int numberGained) {
        consumables.put(id, Math.max(0, getNumberOfConsumable(id) + numberGained));
    }


    public int getNumberOfConsumable(String id) {
        return consumables.get(id, 0);
    }

    public int getMaxNumberOfConsumables(String itemType) {
        if (speedMode == GameState.SPEED_MODE_EASY) {
            return 1;
        }
        return consumablesMax.get(itemType, 0);
    }

    public Float getConsumableRespawn(String itemType) {
        return consumableRespawn.get(itemType);
    }


    public LinkedHashSet<TeleportLocation> getTeleportOptions() {
        return teleporters;
    }


    private String getTeleporterListString() {
        String result = "";
        for (TeleportLocation teleporter : teleporters) {
            result = result + teleporter.toString() + ",";
        }
        return result;
    }

    public void discoverTeleporter(TeleportLocation teleporterLocation) {
        teleporters.add(teleporterLocation);
    }


    private void rebuildTeleporters(String fromString) {
        teleporters.clear();
        String[] split = fromString.split(",");
        for (String teleporterDef : split) {
            if (teleporterDef != null && !teleporterDef.trim().isEmpty()) {
                TeleportLocation teleporter = new TeleportLocation(teleporterDef);
                teleporters.add(teleporter);
            }
        }

    }

    public int getNumDeaths() {
        return deaths;
    }

    public float getPlayTime() {
        return time;
    }
}
