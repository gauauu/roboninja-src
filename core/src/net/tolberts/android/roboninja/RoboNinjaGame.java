package net.tolberts.android.roboninja;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import net.tolberts.android.game.GamePrefs;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.levels.LevelFactory;
import net.tolberts.android.game.loaders.*;
import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.screens.SplashScreen;

import java.util.Set;

public class RoboNinjaGame extends Game {

    public final static int WIDTH = 576;
    public final static int HEIGHT = 320;
    //36x20

    public final static float SCALE = 1 / 16f;

    public final static int WIDTH_IN_TILES = (int) (WIDTH * SCALE);
    public final static int HEIGHT_IN_TILES = (int) (HEIGHT * SCALE);

    public final static int HALF_WIDTH_IN_TILES = WIDTH_IN_TILES / 2;
    public final static int HALF_HEIGHT_IN_TILES = HEIGHT_IN_TILES / 2;
    public static final String TAG = "RoboNinja";

    public static float ACTUAL_RATIO_X;
    public static float ACTUAL_RATIO_Y;

    public final PlatformAdapter platformAdapter;


    public RoboNinjaGame(PlatformAdapter platformAdapter) {
        this.platformAdapter = platformAdapter;
    }

    public static void timedDebug(String message) {
        Gdx.app.error(RoboNinjaGame.TAG, message + " / " + System.currentTimeMillis());
    }


    @Override
    public void create() {
        resetAssetCaches();

        GameState gameState = new RoboNinjaGameState(this);
        GameSettings.getInstance().scale = 1 / 16f;
        gameState.gameSettings = GameSettings.getInstance();
        gameState.mc = new MainCharacter(gameState, platformAdapter.usesOldGraphics());
//		gameState.mc.setAbility(MainCharacter.STATE_RUNNING, new JumpAbility());

        AudioPlayer.initialize();

        Audio.preloadSfx();

        SplashScreen titleScreen = new SplashScreen(gameState);
        setScreen(titleScreen);

        if (GamePrefs.getBool(PlayServicesAdapter.ALLOW_PLAY_SERVICES, false)) {
            platformAdapter.getPlayServicesAdapter().attemptLogin();
        }




        ACTUAL_RATIO_X = (float) WIDTH / (float) Gdx.graphics.getWidth();
        ACTUAL_RATIO_Y = (float) HEIGHT / (float) Gdx.graphics.getHeight();


    }

    private void resetAssetCaches() {
        AsyncLoader.clearCache();
        LevelFactory.clearCache();
        Art.resetCache();
        AbilityIcons.resetCache();
        Audio.resetCache();
        Fonts.resetCache();
        AudioPlayer.resetCache();
    }

    public boolean adapterHasFlag(String flag) {
        Set<String> otherFlags = platformAdapter.getOtherFlags();
        if (otherFlags == null) {
            return false;
        }
        return otherFlags.contains(flag);
    }





}
