package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.AnimatedCharacter;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class RollingBoulder extends Enemy {

    final static Vector2 frameSize = new Vector2(32, 32);
    public static final String ID = "rollingBoulder";
    private static final String PROP_MIN_X = "minX";
    private static final String PROP_SPEED = "speed";
    private static final String PROP_MAX_X = "maxX";
    private float minX;
    private float maxX = 9999999;

    protected float speed = 10;
    protected int facing = FACING_RIGHT_INT;

    public RollingBoulder() {
        initGraphics();
        minX = -1;
        setCurrentAnimation("main");
    }

    @Override
    public void collideWithMc(GameState state) {
        state.mc.die();
    }

    @Override
    protected String getTextureName() {
        return "boulder";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation rollRight = new Animation(0.1f, textureRegions[0][0],
                textureRegions[0][1],
                textureRegions[0][2],
                textureRegions[0][3],
                textureRegions[0][4],
                textureRegions[0][5],
                textureRegions[0][6],
                textureRegions[0][7]
        );
        Animation rollLeft = new Animation(0.1f,
                textureRegions[0][7],
                textureRegions[0][6],
                textureRegions[0][5],
                textureRegions[0][4],
                textureRegions[0][3],
                textureRegions[0][2],
                textureRegions[0][1],
                textureRegions[0][0]
        );
        rollRight.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("main", rollRight
        );

        rollLeft.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("left", rollLeft
        );

    }

    @Override
    public void updateCharacter(float delta) {


        bounds.width = 2.0f;
        bounds.height = 2.0f;
        bounds.x += speed * facing * delta;
        if (facing == FACING_LEFT_INT && bounds.x < -1) {
            bounds.x = gameState.level.getWidth();
        } else if (facing == FACING_RIGHT_INT && bounds.x > Math.min(maxX, gameState.level.getWidth())) {
            bounds.x = minX;
        }
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {
        if (propertyMatch(PROP_FACING, FACING_LEFT, prop, value)) {
            facing = FACING_LEFT_INT;
            setCurrentAnimation("left");
        }
        if (PROP_MIN_X.equals(prop)) {
            try {
                minX = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse minX for rollingBoulder: " + value);
            }
        }
        if (PROP_MAX_X.equals(prop)) {
            try {
                maxX = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse maxX for rollingBoulder: " + value);
            }
        }
        if (PROP_SPEED.equals(prop)) {
            try {
                speed = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse speed for rollingBoulder: " + value);
            }
        }
    }

}
