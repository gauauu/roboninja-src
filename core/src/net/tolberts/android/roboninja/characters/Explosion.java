package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;

import java.util.Map;

public class Explosion extends Enemy {

	final static Vector2 frameSize = new Vector2(64, 64);
	public static final String ID = "explosion";


	public Explosion() {
		initGraphics();
		setCurrentAnimation("main");
	}

	@Override
	public void collideWithMc(GameState state) {
		//don't care
	}

	@Override
	protected String getTextureName() {
		return "explosion";
	}

	@Override
	protected Vector2 getFrameSize() {
		return frameSize;
	}

	@Override
	protected void initAnimations(Map<String, Animation> animations) {
		Animation anim = new Animation(0.05f,
                textureRegions[0][0],
                textureRegions[1][0],
                textureRegions[2][0],
                textureRegions[3][0],
                textureRegions[4][0],
                textureRegions[5][0],
                textureRegions[6][0],
                textureRegions[7][0],
                textureRegions[8][0],
                textureRegions[9][0],
                textureRegions[10][0],
                textureRegions[11][0],
                textureRegions[12][0],
                textureRegions[13][0],
                textureRegions[14][0],
                textureRegions[15][0],
                textureRegions[16][0],
                textureRegions[17][0],
                textureRegions[18][0],
                textureRegions[19][0],
                textureRegions[20][0],
                textureRegions[21][0],
                textureRegions[22][0],
                textureRegions[23][0]);

        anim.setPlayMode(Animation.PlayMode.NORMAL);
		animations.put("main", anim);
	}

	@Override
	public void updateCharacter(float delta) {
        if (dyingTimer < -99) {
            dyingTimer = 1.5f;
        }
	}

	@Override
	public String getId() {
		return ID;
	}

	@Override
	public void setProperty(String prop, String value) {


	}

    @Override
    public boolean doesRespawn() {
        return true;
    }
}
