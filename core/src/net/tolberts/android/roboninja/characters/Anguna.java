package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class Anguna extends Enemy {

    final static Vector2 frameSize = new Vector2(32, 32);
    public static final String ID = "anguna";



    public Anguna() {
        initGraphics();
        setCurrentAnimation("standing");
    }


    @Override
    protected String getTextureName() {
        return "anguna";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(1.0f, textureRegions[0][0]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("standing", anim);

    }

    @Override
    public void updateCharacter(float delta) {


    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {

    }


    @Override
    public void collideWithMcBullet(GameState state) {

    }


}
