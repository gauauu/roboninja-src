package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.mc.abilities.air.DoubleJumpAbility;

public class EnemyFactory {

	public static Enemy newEnemy(String enemyId) {
        return (Enemy) MiscUtils.instantiateFromId(enemyId, "net.tolberts.android.roboninja.characters");


	}
	
	

}
