package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class Elevator extends Platform {

    public final static String ID = "elevator";
    public final static String PROP_MIN_Y = "minY";
    public final static String PROP_MAX_Y = "maxY";
    private float minY = -99999f;
    private float maxY = 99999f;

    public Elevator() {
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {
        super.setProperty(prop, value);
        if (PROP_MIN_Y.equals(prop)) {
            try {
                minY = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse minY of " + value);
            }
        }
        if (PROP_MAX_Y.equals(prop)) {
            try {
                maxY = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse maxY of " + value);
            }
        }
    }

    @Override
    public void updateCharacter(float delta) {

        float levelHeight = gameState.level.getHeight();

        actualBounds.y += speed * delta;
        if (gameState.mc.glueToPlatform == this) {
            gameState.mc.bounds.y += speed * delta;
        }
        if (actualBounds.y > Math.min(levelHeight + 1, levelHeight - minY)) {
            actualBounds.y = -2;
        }
        if (actualBounds.y < Math.max(-2, levelHeight - maxY)) {
            actualBounds.y = levelHeight;
        }


        updateEdgeRectangles();

    }
}
