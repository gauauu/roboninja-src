package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.NPC;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.mc.AbilityFactory;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.mc.abilities.consumable.ConsumableAbility;

import java.util.Map;

public class ItemCharacter extends NPC {

    private String itemId;
    private String sourceLevelId;
    private McAbility ability;
    final static Vector2 frameSize = new Vector2(24, 25);

    public ItemCharacter(String itemId, String sourceLevelId) {
        super();
        this.itemId = itemId;
        initGraphics();
        this.sourceLevelId = sourceLevelId;
        this.ability = AbilityFactory.getAbility(itemId);
        setCurrentAnimation("item");
    }

    @Override
    protected String getTextureName() {
        return "item";
    }

    public String getSourceLevelId() {
        return sourceLevelId;
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(0.25f, textureRegions[0][0], textureRegions[0][1], textureRegions[0][2],
                textureRegions[0][3]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("item", anim);
    }

    @Override
    public void updateCharacter(float delta) {
        bounds.width = 1.5f;
        bounds.height = 1.5f;
        setCurrentAnimation("item");
    }

    @Override
    public void collideWithMc(GameState state) {
        McAbility ability = AbilityFactory.getAbility(itemId);
        MiscUtils.assertFalse(ability == null);
        ((RoboNinjaGameState) state).acquireAbility(ability, this);
        dead = true;
    }

    @Override
    public String getFlag() {
        if (ability instanceof ConsumableAbility) {
            return itemId + "_" + getSourceLevelId();
        }
        return itemId;
    }

    public String getId() {
        return itemId;
    }


}
