package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.TeleportLocation;

import java.util.Map;

public class Teleporter extends Enemy {

    public final static String ID = "teleporter";
    private Vector2 frameSize;

    public final static String PROP_NAME = "name";

    public final static String STATE_IDLE = "idle";
    public final static String STATE_TELEPORTING = "teleporting";
    public static final String STATE_ARRIVING = "arriving";


    public final static float TELEPORT_ANIM_TIME = 1.0f;

    protected String state;

    protected float stateTimer = 0.0f;
    protected float ignoreTimer = 0;

    public String name;

    protected TeleportLocation location;

    public Teleporter() {

        textureScale.x = 0.5f;
        textureScale.y = 0.5f;
        state = STATE_IDLE;
        initGraphics();
        setCurrentAnimation(STATE_IDLE);
        ignoreTimer = 3.0f;
    }


    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {
        if (PROP_NAME.equals(prop)) {
            name = value;
        }
    }

    @Override
    protected String getTextureName() {
        return "teleporter";
    }

    @Override
    public boolean collideWithMcLaser(GameState state) {
        return false;
    }

    @Override
    public void collideWithMc(GameState gs) {
        if (STATE_IDLE.equals(state) && ignoreTimer <= 0) {
            if (bounds.contains(gs.mc.bounds)) {
                if (gs.mc.secondsSinceRespawn > 2) {

                    int numDiscovered = ((RoboNinjaProgress) gameState.gameProgress).getTeleportOptions().size();


                    if (numDiscovered < 2 && gameState.level.id.equals(RoboNinjaGameState.STARTING_LEVEL)) {
                        ((RoboNinjaGameState) gameState).enterTeleporter(this, getTeleportLocation());
                    } else {
                        gs.mc.freezeHorizontal = true;
                        stateTimer = TELEPORT_ANIM_TIME;
                        state = STATE_TELEPORTING;
                        setCurrentAnimation(STATE_TELEPORTING);
                    }
                }
            }
        }
    }

    @Override
    protected Vector2 getFrameSize() {
        if (frameSize == null) {
            frameSize = new Vector2(64, 64);
        }
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation idle = new Animation(0.10f, textureRegions[0][0]);
        idle.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(STATE_IDLE, idle);


        Animation anim = new Animation(0.166f, textureRegions[0][0], textureRegions[0][1],
                textureRegions[0][2]);
        anim.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
        animations.put(STATE_TELEPORTING, anim);

    }

    @Override
    public void updateCharacter(float delta) {
        bounds.width = 1.1f;
        bounds.height = 4;

        if (ignoreTimer > 0) {
            //state = STATE_IDLE;
        }

        if (STATE_TELEPORTING.equals(state)) {
            stateTimer -= delta;
            if (stateTimer <= (TELEPORT_ANIM_TIME / 2)) {
                gameState.mc.visible = false;
            }
            if (stateTimer <= 0) {
                ((RoboNinjaGameState) gameState).enterTeleporter(this, getTeleportLocation());
                state = STATE_IDLE;
                setCurrentAnimation(STATE_IDLE);
                ignoreTimer = 2.0f;
            }
        } else if (STATE_ARRIVING.equals(state)) {
            stateTimer -= delta;
            if (stateTimer >= (TELEPORT_ANIM_TIME / 2)) {
                gameState.mc.visible = false;
                gameState.mc.freezeHorizontal = true;
            } else {
                gameState.mc.visible = true;
            }
            if (stateTimer <= 0) {
                setCurrentAnimation(STATE_IDLE);
                state = STATE_IDLE;
                gameState.mc.freezeHorizontal = false;
                ignoreTimer = 2.0f;
            }
        } else {
            if (ignoreTimer > 0) {
                ignoreTimer -= delta;
            }
        }
    }

    public void cancelTeleport() {
        gameState.mc.visible = true;
        gameState.mc.freezeHorizontal = false;

        ignoreTimer = 2.0f;
    }

    public void teleportTo() {
        Vector2 newPosition = new Vector2(getPosition());
        newPosition.x += 1.0f; //bump things up and right a bit to make it line up nicely
        newPosition.y += 1.0f;
        gameState.mc.setPosition(newPosition);
        state = STATE_ARRIVING;
        gameState.mc.freezeHorizontal = true;
        stateTimer = TELEPORT_ANIM_TIME;
        setCurrentAnimation(STATE_TELEPORTING);

    }

    public TeleportLocation getTeleportLocation() {
        if (location == null) {
            location = new TeleportLocation();
            if (name == null) {
                Gdx.app.error(RoboNinjaGame.TAG, "Teleporter is missing a name, needs prop " + PROP_NAME);
            }
            location.name = name;
            location.levelId = gameState.level.id;

            Vector2 minimapPosition = new Vector2(gameState.level.minimapPosition);
            minimapPosition.x += bounds.x;
            minimapPosition.y += (gameState.level.getHeight() - bounds.y);

            location.minimapX = minimapPosition.x;
            location.minimapY = minimapPosition.y;
            location.actualY = bounds.y;

        }
        return location;
    }
}