package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class SpikeWall extends Enemy {

    public final static String ID = "spikeWall";
    public float dropSpeed = 1.0f;
    private Vector2 frameSize = new Vector2(16, 16);
    private Texture spikeWallTexture;
    private Rectangle initialBounds;
    private float maxHeight = -1;

    @Override
    public String getId() {
        return ID;
    }

    @Override
    protected void initGraphics() {
        spikeWallTexture = Art.getTexture("spikeWall");
    }

    @Override
    public void setProperty(String prop, String value) {
        if ("speed".equals(prop)) {
            try {
                dropSpeed = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "could not parse spikewall speed: " + e.toString());
            }
        }
        if ("maxHeight".equals(prop)) {
            try {
                maxHeight = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "could not parse spikewall maxHeigt: " + e.toString());
            }
        }
    }

    @Override
    protected String getTextureName() {
        return "spikeWall";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {

    }

    @Override
    public void updateCharacter(float delta) {
        float diff = dropSpeed * delta;

        if (bounds.height < 1000) {
            bounds.height += diff;
            bounds.y -= diff;


            if (maxHeight > 0) {
                if (bounds.height > maxHeight) {
                    dropSpeed = -dropSpeed;
                    bounds.height = maxHeight - 0.001f;
                }
                if (bounds.height < 0.5f) {
                    dropSpeed = -dropSpeed;
                }
            }
        }
    }

    @Override
    public void setDefinitionBounds(Rectangle definitionBounds) {
        super.setDefinitionBounds(definitionBounds);

        bounds.x = this.definitionBounds.x;
        bounds.y = this.definitionBounds.y;

        bounds.height = this.definitionBounds.height;
        bounds.width = this.definitionBounds.width;

        initialBounds = new Rectangle(bounds);

    }

    @Override
    public boolean collideWithMcLaser(GameState state) {
        return false;
    }

    @Override
    public void render(BatchTiledMapRenderer renderer) {

        Batch batch = renderer.getSpriteBatch();


        //render the middle
        //draw the main platform part
        batch.draw(spikeWallTexture, bounds.x + 1, bounds.y + 1, bounds.width - 1, bounds.height - 1, 16, 16, 16, 16, false, false);


        //render spikes on top
        //render spikes on bottom
        for (int i = 1; i < bounds.width - 1; i++) {
            batch.draw(spikeWallTexture, bounds.x + i, bounds.y, 1, 1, 16, 32, 16, 16, false, false);
            batch.draw(spikeWallTexture, bounds.x + i, bounds.y + bounds.height, 1, 1, 16, 0, 16, 16, false, false);
        }
        //render spikes on left
        //render spikes on right
        for (int i = 1; i < bounds.height - 1; i++) {
            batch.draw(spikeWallTexture, bounds.x, bounds.y + i, 1, 1, 0, 16, 16, 16, false, false);
            batch.draw(spikeWallTexture, bounds.x + bounds.width, bounds.y + i, 1, 1, 32, 16, 16, 16, false, false);
        }
    }

    @Override
    public void update(float delta, int gameMode) {
        if (gameMode == GameState.MODE_READY) {
            bounds.set(initialBounds);
        }
        super.update(delta, gameMode);
    }
}
