package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;

import java.util.Map;

public class FlameSpout extends Enemy{


    final static Vector2 frameSize = new Vector2(16, 64);

    public final static String ID = "flameSpout";

    public final static String STATE_IDLE = "idle";
    public final static String STATE_SHOOTING = "shooting";

    private float timeIdle = 1.0f;
    private float timeShooting = 1.0f;

    private String state;
    private float stateTimer;

    private Rectangle idleBounds;
    private Rectangle shootingBounds;


    public FlameSpout() {
        super();
        state = STATE_IDLE;
        stateTimer = timeIdle;
        bounds.height = 64;
        bounds.width = 16;
        setCurrentAnimation(STATE_SHOOTING);
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {

    }


    @Override
    protected String getTextureName() {
        return "fire_vert";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(0.10f, textureRegions[0][0], textureRegions[1][0], textureRegions[2][0],
                textureRegions[3][0], textureRegions[4][0], textureRegions[5][0],
                textureRegions[6][0], textureRegions[7][0]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(STATE_SHOOTING, anim);

    }

    @Override
    public void collideWithMc(GameState state) {
        if (visible) {
            super.collideWithMc(state);
        }
    }

    @Override
    public void updateCharacter(float delta) {

        stateTimer -= delta;
        if (stateTimer < 0) {
            if (STATE_IDLE.equals(state)) {
                state = STATE_SHOOTING;
                stateTimer = timeShooting;
                visible = true;
            } else {
                state = STATE_IDLE;
                stateTimer = timeIdle;
                visible = false;
            }
        }

    }
}
