package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class BlueGuard extends Enemy {

    final static Vector2 frameSize = new Vector2(64, 64);
    public static final String ID = "blueGuard";


    private static final String MODE_SHOOTING = "mode_shooting";
    private static final String MODE_WAITING = "mode_waiting";

    public static final String PROPS_SHOT_MODE = "shotMode";
    public static final String SHOT_MODE_NORMAL = "normal";
    public static final String SHOT_MODE_ALTERNATING = "alternating";

    public static final String PROP_FLIPS_FACING = "flipsFacing";

    public static final String PROP_RESPAWN = "respawn";

    private String mode;
    private boolean facingLeft = false;

    private float shootTiming = 1.5f;
    private float flipsFacing = 0;

    private float timeUntilFlips = 0;
    private float timeUntilShoots = 0;

    private boolean lastShotWasBig = true;
    private boolean isShooting = false;

    private boolean doesRespawn = false;


    public BlueGuard() {
        textureScale.x = 0.5f;
        textureScale.y = 0.5f;

        textureOffset.x = -0.75f;


        initGraphics();
        setMode(MODE_WAITING);
    }


    @Override
    protected String getTextureName() {
        return "guardBlue64";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(1.0f, textureRegions[0][1]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(MODE_WAITING, anim);

        Animation shooting = new Animation(0.1f, textureRegions[1][0], textureRegions[1][1], textureRegions[1][2]);
        shooting.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(MODE_SHOOTING, shooting);

    }

    @Override
    public void updateCharacter(float delta) {

        bounds.height = 1.75f;

        applyGravityWithCollision(delta);

        hFlip = !facingLeft;

        if (flipsFacing > 0) {
            //handle flipping the facing
        }

        if (isShooting && isAnimationFinished()) {
            setCurrentAnimation(MODE_WAITING);
            isShooting = false;
        }

        if (shootTiming > 0) {
            if (timeUntilShoots < 0) {
                shoot();
                timeUntilShoots = getTimeUntilNextShot();
            }

            timeUntilShoots -= delta;
        }
    }

    private void shoot() {
        isShooting = true;
        setCurrentAnimation(MODE_SHOOTING);
        EnemyLaser enemyLaser = new EnemyLaser();
        enemyLaser.setGameState(gameState);
        //numbers decided via fiddling about until they looked right....
        enemyLaser.bounds.x = bounds.x + (facingLeft ? -1.25f : 1.0f);
        enemyLaser.bounds.y = bounds.y + 1.0f;
        enemyLaser.setSpeed(facingLeft ? -10.0f : 10.0f);
        //draw the laser behind the guard so it looks like it's coming out of his gun
        enemyLaser.z = -10;
        gameState.addCharacter(enemyLaser);
        if (isOnGameScreen()) {
            AudioPlayer.playSound(Audio.PLAYER_LASER);
        } else {
            int guards = 0;
            for (GameCharacter character : gameState.characters) {
                if (character instanceof BlueGuard) {
                    guards++;
                }
            }
            if (guards > 1) {
                AudioPlayer.playSound(Audio.PLAYER_LASER);
            } else {
                float width = gameState.getGameScreen().currentLevel.getWidth();
                float height = gameState.getGameScreen().currentLevel.getHeight();
                float maxSize = Math.max(width, height);
                float distance = distanceFromGameScreen();
                float volume = Math.min(1.0f, (Math.max(0, maxSize - distance) / maxSize));
                AudioPlayer.playSound(Audio.PLAYER_LASER, volume);
            }
        }


    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {
        if (PROP_FACING.equals(prop)) {
            facingLeft = FACING_LEFT.equals(value);
        }
        if (PROP_FLIPS_FACING.equals(prop)) {
            if ("false".equals(value)) {
                flipsFacing = 0.0f;
            } else {
                try {
                    flipsFacing = Float.parseFloat(value);
                } catch (NumberFormatException e) {
                    Gdx.app.error(RoboNinjaGame.TAG, "Could not parse flip facing");
                    Gdx.app.error(RoboNinjaGame.TAG, e.toString());
                }
            }
        }
        if (PROP_RESPAWN.equals(prop)) {
            doesRespawn = true;
        }
    }

    @Override
    public boolean doesRespawn() {
        return doesRespawn;
    }

    public void setMode(String mode) {
        this.mode = mode;
        setCurrentAnimation(mode);
    }

    public String getMode() {
        return mode;
    }

    public float getTimeUntilNextShot() {
        return shootTiming;
    }
}
