package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;

import java.util.Map;
import java.util.Random;

public class Boss extends Enemy {

    public static final String ID = "boss";
    public final static String MODE_NORMAL = "normal";
    final static Vector2 frameSize = new Vector2(85, 94);
    private static final int BOSS_START_HP = 3;
    private final Random random;

    private final static int STATE_FLAME_ATTACKS = 0;
    private final static int STATE_PLATFORMS = 1;
    private final static int STATE_EXPLODING = 2;

    private static final float EXPLOSION_DELAY = 0.5f;

    private float stateTimer;
    private int attackCounter = 3;
    private int state;

    private float speed = 4.0f;
    private int bossHp;
    private float explosionTimer = 0;


    public Boss() {
        textureScale.x = 1.0f;
        textureScale.y = 1.0f;

        initGraphics();
        setCurrentAnimation(MODE_NORMAL);

        stateTimer = 5.0f;

        random = new Random();
        hFlip = true;


        bossHp = BOSS_START_HP;

        state = STATE_FLAME_ATTACKS;
//        state = STATE_EXPLODING;
    }

    @Override
    protected String getTextureName() {
        return "mage-1";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(0.25f, textureRegions[0][0], textureRegions[0][1], textureRegions[0][2], textureRegions[0][3],
                textureRegions[1][0], textureRegions[1][1], textureRegions[1][2], textureRegions[1][3]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(MODE_NORMAL, anim);

    }

    @Override
    public void updateCharacter(float delta) {
        bounds.height = 5.8f;
        bounds.width = 5.3f;
        if (state == STATE_FLAME_ATTACKS) {
            updateStateFlameAttacks(delta);
        } else if (state == STATE_PLATFORMS) {
            updateStatePlatforms(delta);
        } else if (state == STATE_EXPLODING) {
            updateStateExploding(delta);
        }
    }

    private void updateStateFlameAttacks(float delta) {

        bounds.x += speed * delta;

        if (bounds.x > RoboNinjaGame.WIDTH_IN_TILES - 4) {
            bounds.x = RoboNinjaGame.WIDTH_IN_TILES - 4;
            speed = -speed;
            hFlip = false;
        }
        if (bounds.x < 1) {
            bounds.x = 1;
            speed = -speed;
            hFlip = true;
        }


        stateTimer -= delta;
        if (stateTimer < 0) {
            if (attackCounter == 0) {
                switchToStatePlatforms();
                return;
            }
            stateTimer = 5.0f;
            if (random.nextBoolean()) {
                spawnFlames();
            } else {
                spawnBottomFlames();
            }
            attackCounter--;
        }

    }

    private void updateStatePlatforms(float delta) {
        if (!isInMiddle()) {
            bounds.x += speed * delta;

            if (bounds.x > RoboNinjaGame.WIDTH_IN_TILES - 4) {
                bounds.x = RoboNinjaGame.WIDTH_IN_TILES - 4;
                speed = -speed;
                hFlip = false;
            }
            if (bounds.x < 1) {
                bounds.x = 1;
                speed = -speed;
                hFlip = true;
            }

        }

        if (stateTimer < 0) {
            setTintCycling(false);
            switchToStateFlameAttacks();
        }
        stateTimer -= delta;
        if (stateTimer < 10 && stateTimer + delta > 10) {
            activatePlatforms();
        }

    }

    private void updateStateExploding(float delta) {
        explosionTimer -= delta;
        if (explosionTimer <= 0) {
            addExplosion();
            explosionTimer = EXPLOSION_DELAY;
        }
        stateTimer -= delta;
        if (stateTimer < 0) {
            dyingTimer = 0.1f;
            ((RoboNinjaGameState)gameState).showEndingIn(5);
        }
    }

    private void addExplosion() {
        Explosion explosion = new Explosion();
        explosion.setGameState(gameState);
        explosion.bounds.x = bounds.x + (random.nextInt(2));
        explosion.bounds.y = bounds.y + (random.nextInt(4));

        //draw the laser behind the guard so it looks like it's coming out of his gun
        gameState.addCharacter(explosion);
        AudioPlayer.playSound("explosion");
    }

    @Override
    public void onMcRespawn() {
        super.onMcRespawn();
        bossHp = BOSS_START_HP;
        isTintCycling = false;
    }


    private void switchToStateFlameAttacks() {
        Gdx.app.log(RoboNinjaGame.TAG, "hp: " + bossHp);
        attackCounter = 3;
        stateTimer = 3.0f;
        state = STATE_FLAME_ATTACKS;
    }

    private void switchToStatePlatforms() {
        stateTimer = 18.0f;
        state = STATE_PLATFORMS;
    }

    private boolean isInMiddle() {
        float middle = ((float)RoboNinjaGame.WIDTH_IN_TILES / 2);
        middle -= bounds.width / 2;
        float diff = bounds.x - middle;
        return (diff > 0 && diff < 1);
    }


    private void spawnBottomFlames() {
        int heightOfTopFlame = random.nextInt(10) + 3;
        for (GameCharacter character : gameState.characters) {
            if (character instanceof BossFlamesBottomSides) {
                final BossFlamesBottomSides flame = (BossFlamesBottomSides) character;

                if (flame.activateFlames()) {
                    if (flame.bounds.y > 2) {
                        flame.setPos(heightOfTopFlame);
                    }
                }
            }
        }
    }

    private void activatePlatforms() {
        for (GameCharacter character : gameState.characters) {
            if (character instanceof BossPlatform) {
                BossPlatform bp = (BossPlatform) character;
                bp.activate();
            }
        }
    }

    public boolean collideWithMcLaser(GameState gameState) {
        bossHp--;
        if (bossHp == 0) {
            state = STATE_EXPLODING;
            stateTimer = 5.0f;
        }
        setTintCycling(true);
        return true;
    }

    private void spawnFlames() {

        final int middleRange = RoboNinjaGame.WIDTH_IN_TILES / 3;
        int middleMiddle = random.nextInt(middleRange) + RoboNinjaGame.WIDTH_IN_TILES / 4;
        int middleLeft = random.nextInt(middleMiddle - 4) + 6;
        int middleRight = random.nextInt(middleMiddle - 6) + 4 + middleMiddle;

        for (GameCharacter character : gameState.characters) {
            if (character instanceof BossFlamesTop) {
                final BossFlamesTop flamesTop = (BossFlamesTop) character;
                if (flamesTop.whichOne.equals("left")) {
                    flamesTop.setPos(0, middleLeft - 3);
                } else if (flamesTop.whichOne.equals("middle")) {
                    flamesTop.setPos(middleLeft, middleRight - middleLeft);
                } else if (flamesTop.whichOne.equals("right")) {
                    flamesTop.setPos(middleRight + 3, RoboNinjaGame.WIDTH_IN_TILES - (middleRight + 3));
                }

                flamesTop.activateFlames();
            }
        }
    }


    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {

    }


}
