package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;

import java.util.Map;

public class Trampoline extends Enemy {

    public final static String ID = "trampoline";
    private Vector2 frameSize;

    public final static String STATE_IDLE = "idle";
    public final static String STATE_BOUNCING = "bouncing";

    public final static float bounceTimer = 0.30f;

    protected String state;

    protected float stateTimer = 0.0f;


    public Trampoline() {
        super();
        state = STATE_IDLE;
        setCurrentAnimation(STATE_IDLE);
    }


    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {

    }

    @Override
    protected String getTextureName() {
        return "trampoline";
    }

    @Override
    public void collideWithMc(GameState gs) {
        if (STATE_IDLE.equals(state)) {
            gs.mc.velocity.y = 60;
            stateTimer = bounceTimer;
            setCurrentAnimation(STATE_BOUNCING);
            state = STATE_BOUNCING;
            gs.mc.freezeHorizontal = false;
        }
    }

    @Override
    protected Vector2 getFrameSize() {
        if (frameSize == null) {
            frameSize = new Vector2(16, 16);
        }
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation idle = new Animation(0.10f, textureRegions[0][0]);
        idle.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(STATE_IDLE, idle);


        Animation anim = new Animation(0.05f, textureRegions[0][0], textureRegions[0][1],
                textureRegions[1][0], textureRegions[1][1]);
        anim.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
        animations.put(STATE_BOUNCING, anim);

    }

    @Override
    public void updateCharacter(float delta) {
        if (STATE_BOUNCING.equals(state)) {
            stateTimer -= delta;
            if (stateTimer <= 0) {
                setCurrentAnimation(STATE_IDLE);
                state = STATE_IDLE;
            }
        }
    }

    @Override
    public boolean collideWithMcLaser(GameState state) {
        return false;
    }
}
