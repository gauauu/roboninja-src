package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntFloatMap;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class BossFlamesBottomSides extends Enemy {

    final static Vector2 frameSize = new Vector2(64, 16);

    public final static String ID = "bossFlamesBottomSides";

    public final static int STATE_IDLE = 0;
    public final static int STATE_PREPPING = 1;
    public final static int STATE_GROWING = 2;
    public final static int STATE_FULL = 3;
    public final static int STATE_SHRINKING = 4;


    private int state;
    private float stateTimer;

    private IntFloatMap stateTimers = new IntFloatMap() {{
        put(STATE_PREPPING, 2.0f);
        put(STATE_GROWING, 0.75f);
        put(STATE_FULL, 1.5f);
        put(STATE_SHRINKING, 0.75f);
    }};
    public String whichOne = "";

    public BossFlamesBottomSides() {
        super();
        state = STATE_IDLE;
        stateTimer = 0;
        bounds.x = -10;
        bounds.width = 0;
        bounds.y = 1;
        bounds.height = 1;
        setCurrentAnimation(ID);
    }

    @Override
    public String getId() {
        return ID;
    }


    public void onMcRespawn() {
        state = STATE_IDLE;
        updateCharacter(1.0f);
    }

    @Override
    public void setProperty(String prop, String value) {
        if ("which".equals(prop)) {
            whichOne = value;
            if ("left".equals(value)) {
                hFlip = true;
            }
        }
        if ("height".equals(prop)) {
            try {
                float h = Float.parseFloat(value);
                bounds.y = h;
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parse height for FlamesBottomSides: " + prop, e);
            }
        }
    }


    @Override
    protected String getTextureName() {
        return "fire_horiz";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(0.10f, textureRegions[0][0], textureRegions[0][1], textureRegions[0][2],
                textureRegions[0][3], textureRegions[0][4], textureRegions[0][5],
                textureRegions[0][6], textureRegions[0][7]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ID, anim);

    }

    public boolean activateFlames() {
        if (state != STATE_IDLE) {
            return false;
        }
        state = STATE_PREPPING;
        stateTimer = stateTimers.get(state, 0);
        return true;
    }


    @Override
    public void render(BatchTiledMapRenderer renderer) {
        Batch batch = renderer.getSpriteBatch();

        TextureRegion frame = getCurrentFrame();
        if (frame == null) {
            return;
        }

        if (state == STATE_IDLE) {
            return;
        }

        float drawX = bounds.x;
        float maxX = bounds.x + bounds.width;

        if ("left".equals(whichOne)) {
            maxX -= 2;
            while (drawX < maxX) {
                if (!hFlip) {
                    batch.draw(frame, maxX - 2 + inGameSpriteBounds.x + textureOffset.x, bounds.y + inGameSpriteBounds.y + textureOffset.y, 0, 0, inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);
                } else {
                    batch.draw(frame, maxX - 2 - inGameSpriteBounds.x + textureOffset.x + inGameSpriteBounds.width, bounds.y + inGameSpriteBounds.y + textureOffset.y, 0, 0, -inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);
                }
                maxX -= 2;
            }

        } else {

            while (drawX < maxX + 2) {
                if (!hFlip) {
                    batch.draw(frame, drawX + inGameSpriteBounds.x + textureOffset.x, bounds.y + inGameSpriteBounds.y + textureOffset.y, 0, 0, inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);
                } else {
                    batch.draw(frame, drawX - inGameSpriteBounds.x + textureOffset.x + inGameSpriteBounds.width, bounds.y + inGameSpriteBounds.y + textureOffset.y, 0, 0, -inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);
                }
                drawX += 2;
            }
        }

    }

    public void setPos(float y) {
        bounds.y = y;
        bounds.height = 1;
    }

    @Override
    public void updateCharacter(float delta) {


        if (state == STATE_IDLE) {
            bounds.x = -10;
            bounds.width = 0;
            return;
        }

        stateTimer -= delta;


        if (stateTimer < 0) {
            int nextState = state + 1;
            if (nextState > STATE_SHRINKING) {
                state = STATE_IDLE;
            } else {
                state = nextState;
                stateTimer = stateTimers.get(state, 0);
            }
            return;
        }

        float percentDone = 1 - stateTimer / stateTimers.get(state, 0);
        float max = RoboNinjaGame.WIDTH_IN_TILES / 2;
        float rawWidth = max * percentDone;
        float rawInverse = max - rawWidth;


        if ("left".equals(whichOne)) {
            updateLeftSide(max, rawWidth, rawInverse);
        } else {
            updateRightSide(max, rawWidth, rawInverse);
        }
    }

    private void updateLeftSide(float max, float rawWidth, float rawInverse) {

        bounds.x = -2;

        if (state == STATE_GROWING) {
            bounds.width = 3 + rawWidth;
        } else if (state == STATE_FULL) {
            bounds.width = 3 + max;
        } else if (state == STATE_SHRINKING) {
            bounds.width = 3 + rawInverse;
        } else if (state == STATE_PREPPING) {
            bounds.width = 3;
        }

    }

    private void updateRightSide(float max, float rawWidth, float rawInverse) {

        float right = RoboNinjaGame.WIDTH_IN_TILES - 1;

        if (state == STATE_GROWING) {
            bounds.x = right - rawWidth;
            bounds.width = rawWidth;
        } else if (state == STATE_FULL) {
            bounds.x = right - max;
            bounds.width = max;
        } else if (state == STATE_SHRINKING) {
            bounds.x = right - rawInverse;
            bounds.width = rawInverse;
        } else if (state == STATE_PREPPING) {
            bounds.x = right;
            bounds.width = 1;
        }
    }

    @Override
    public boolean collideWithMcLaser(GameState state) {
        return false;
    }
}
