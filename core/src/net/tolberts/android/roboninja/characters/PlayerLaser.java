package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.characters.NPC;
import net.tolberts.android.game.levels.Level;

import java.util.Map;

public class PlayerLaser extends NPC {

    final static Vector2 frameSize = new Vector2(32, 32);
    public static final String ID = "playerLaser";

    protected float speed = -25;
    private float deadTimer = -1;


    public PlayerLaser() {
        deadTimer = -1;
        textureOffset.y = -0.9f;
        bounds.width = 2.0f;
        bounds.height = 0.2f;
        initGraphics();
        setCurrentAnimation("main");
        setTintCycling(true);
    }

    public void setSpeed(float newSpeed) {
        speed = newSpeed;
    }

    @Override
    public void collideWithMc(GameState state) {
    }

    @Override
    public void handleCollision(GameCharacter compareAgainst, GameState gameState) {
        if (compareAgainst instanceof Enemy) {
            if (((Enemy)compareAgainst).collideWithMcLaser(gameState)) {
                dead = true;
            }
        }
    }

    @Override
    protected String getTextureName() {
        return "good-laser";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(1.0f, textureRegions[0][0]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("main", anim);
    }

    @Override
    public void updateCharacter(float delta) {


        if (deadTimer > 0) {
            deadTimer -= delta;
            if (deadTimer <= 0) {
                dead = true;
            }
        }

        bounds.x += speed * delta;
        if (bounds.x < -1) {
            dead = true;
        }

        if (bounds.x > gameState.level.getWidth()) {
            dead = true;
        }



    }



}
