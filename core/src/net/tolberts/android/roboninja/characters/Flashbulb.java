package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.roboninja.screens.GameScreen;

import java.util.Map;

public class Flashbulb extends Enemy {

    public final static String ID = "flashbulb";
    private Vector2 frameSize;

    public final static String STATE_IDLE = "idle";

    public final static String STATE_STARTING = "starting";
    public final static String STATE_LIT = "lit";

    public final static float STARTING_TIME = 0.5f;

    protected String state;

    protected float stateTimer = 0.0f;
    protected float litAmount = 0.0f;


    public Flashbulb() {
        super();
        state = STATE_IDLE;
        textureScale.x = 0.5f;
        textureScale.y = 0.5f;
        setCurrentAnimation(STATE_IDLE);
    }


    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {

    }

    @Override
    protected String getTextureName() {
        return "flashbulb";
    }

    @Override
    public void collideWithMc(GameState gs) {
        //nothing happens here
    }

    @Override
    protected Vector2 getFrameSize() {
        if (frameSize == null) {
            frameSize = new Vector2(48,48);
        }
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation idle = new Animation(0.10f, textureRegions[0][0]);
        idle.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(STATE_IDLE, idle);


        Animation anim = new Animation(STARTING_TIME / 6, textureRegions[0][0], textureRegions[1][0],
                textureRegions[0][0], textureRegions[1][0],
                textureRegions[0][0], textureRegions[1][0]
        );
        anim.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(STATE_STARTING, anim);

        Animation lit = new Animation(1.0f, textureRegions[1][0]);
        animations.put(STATE_LIT, anim);

    }

    @Override
    public void updateCharacter(float delta) {
        if (isOnGameScreen()) {
            if (STATE_LIT.equals(state)) {
                if (litAmount < 1) {
                    litAmount += delta * 2;
                }
                litAmount = Math.min(litAmount, 1);
                setBlinding();
            } else if (STATE_IDLE.equals(state)) {
                litAmount = 0;
                stateTimer = STARTING_TIME;
                state = STATE_STARTING;
                setCurrentAnimation(STATE_STARTING);
            } else {
                stateTimer -= delta;
                if (stateTimer < 0) {
                    state = STATE_LIT;
                    setCurrentAnimation(STATE_LIT);
                }

            }
        } else {
            litAmount = 0;
            if (STATE_LIT.equals(state)) {
                setBlinding();
            }
            state = STATE_IDLE;
            setCurrentAnimation(STATE_IDLE);
        }

    }

    private void setBlinding() {
        GameScreen gameScreen = gameState.getGameScreen();
        if (gameScreen != null) {
            gameScreen.setBlindingAmount(litAmount);
        }
    }

    public boolean isLit() {
        return (STATE_LIT.equals(state));
    }



    public float getLitAmount() {
        return litAmount;
    }

    @Override
    public void respawn() {
        super.respawn();
        reset();
    }

    @Override
    public void onMcRespawn() {
        reset();
    }

    private void reset() {
        state = STATE_IDLE;
    }
}
