package net.tolberts.android.roboninja.characters;

import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class BossPlatform extends Platform {

    public final static String ID = "bossPlatform";

    public final static String PROP_SIDE = "which";
    public final static int STATE_INACTIVE = 0;
    public final static int STATE_COMING = 1;
    public final static int STATE_WAITING = 2;
    public final static int STATE_GOING = 3;

    private String which = "left";

    private float stateTimer = 0;
    private int state = STATE_INACTIVE;
    private float moveDir = 0.0f;

    private final static int platformWidth = 7;
    private final static float waitTime = 3.5f;
    private final static float moveSpeed = 0.05f;

    public BossPlatform() {
        edgeLeft = false;
        edgeRight = false;
        vertical = false;
        speed = 1.0f;
    }

    @Override
    protected void initGraphics() {
        wallComponent = Art.getTexture("bossfloor");


    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {


        if (propertyMatch(PROP_SIDE, "right", prop, value)) {
            which = "right";
        }
    }

    public void activate() {
        if (state != STATE_INACTIVE) {
            return;
        }
        moveDir = moveSpeed;
        if ("right".equals(which)) {
            moveDir = -moveDir;
        }
        state = STATE_COMING;
    }

    @Override
    public void updateCharacter(float delta) {
        actualBounds.width = platformWidth;
        definitionBounds.width = platformWidth;

        if (state == STATE_INACTIVE) {
            if ("left".equals(which)) {
                actualBounds.x = -platformWidth;
            } else {
                actualBounds.x = RoboNinjaGame.WIDTH_IN_TILES;
            }
            moveDir = 0.0f;
        } else if (state == STATE_COMING) {
            if ("left".equals(which)) {
                if (actualBounds.x > 0) {
                    state = STATE_WAITING;
                    stateTimer = waitTime;
                    moveDir = 0;
                }
            } else {
                if (actualBounds.x < RoboNinjaGame.WIDTH_IN_TILES - platformWidth) {
                    state = STATE_WAITING;
                    stateTimer = waitTime;
                    moveDir = 0;
                }
            }
        } else if (state == STATE_WAITING) {
            stateTimer -= delta;
            if (stateTimer < 0) {
                state = STATE_GOING;
                moveDir = moveSpeed;
                if ("left".equals(which)) {
                    moveDir = -moveDir;
                }
            }
        } else if (state == STATE_GOING) {
            if (actualBounds.x < -platformWidth || actualBounds.x > RoboNinjaGame.WIDTH_IN_TILES) {
                moveDir = 0;
                state = STATE_INACTIVE;
            }
        }

        actualBounds.x += moveDir;


        updateEdgeRectangles();

    }

}
