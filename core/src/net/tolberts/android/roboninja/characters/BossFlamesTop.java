package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntFloatMap;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class BossFlamesTop extends Enemy {

    final static Vector2 frameSize = new Vector2(16, 64);

    public final static String ID = "bossFlamesTop";

    public final static int STATE_IDLE = 0;
    public final static int STATE_PREPPING = 1;
    public final static int STATE_GROWING = 2;
    public final static int STATE_FULL = 3;
    public final static int STATE_SHRINKING = 4;


    private int state;
    private float stateTimer;

    private IntFloatMap stateTimers = new IntFloatMap() {{
        put(STATE_PREPPING, 3.2f);
        put(STATE_GROWING, 0.5f);
        put(STATE_FULL, 1.0f);
        put(STATE_SHRINKING, 0.5f);
    }};
    public String whichOne = "";

    public BossFlamesTop() {
        super();
        state = STATE_IDLE;
        stateTimer = 0;
        bounds.height = 0;
        bounds.width = RoboNinjaGame.WIDTH;
        setCurrentAnimation(ID);
    }

    @Override
    public String getId() {
        return ID;
    }



    public void onMcRespawn() {
        state = STATE_IDLE;
        updateCharacter(1.0f);
    }

    @Override
    public void setProperty(String prop, String value) {
        if ("which".equals(prop)) {
            whichOne = value;
        }
    }

    public void setPos(float left, float width) {
        bounds.setX(left);
        bounds.setWidth(width);
    }


    @Override
    protected String getTextureName() {
        return "fire_vert";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(0.10f, textureRegions[0][0], textureRegions[1][0], textureRegions[2][0],
                textureRegions[3][0], textureRegions[4][0], textureRegions[5][0],
                textureRegions[6][0], textureRegions[7][0]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ID, anim);

    }

    public void activateFlames() {
        if (state != STATE_IDLE) {
            return;
        }
        state = STATE_PREPPING;
        stateTimer = stateTimers.get(state, 0);
    }


    @Override
    public void render(BatchTiledMapRenderer renderer) {
        Batch batch = renderer.getSpriteBatch();

        float drawX = bounds.x;
        float drawY = bounds.y;
        float maxX = bounds.x + bounds.width;
        float maxY = bounds.y + bounds.height;
        TextureRegion frame = getCurrentFrame();
        if (frame == null) {
            return;
        }

        if (state == STATE_IDLE) {
            return;
        }

        while (drawY < maxY) {
            while (drawX < maxX) {
                batch.draw(frame, drawX + inGameSpriteBounds.x + textureOffset.x, drawY + inGameSpriteBounds.y + textureOffset.y, 0, 0, inTextureSpriteBounds.width, inTextureSpriteBounds.height, textureScale.x, textureScale.y, 0);
                drawX += 1;
            }
            drawX = bounds.x;
            drawY += 1;
        }

    }

    @Override
    public void updateCharacter(float delta) {


        if (state == STATE_IDLE) {
            bounds.height = 0;
            bounds.y = -10;
            return;
        }

        stateTimer -= delta;


        if (stateTimer < 0) {
            int nextState = state + 1;
            if (nextState > STATE_SHRINKING) {
                state = STATE_IDLE;
            } else {
                state = nextState;
                stateTimer = stateTimers.get(state, 0);
            }
            return;
        }

        float percentDone = stateTimer / stateTimers.get(state, 0);
        float rawHeight = RoboNinjaGame.HEIGHT_IN_TILES * percentDone;
        float rawInverse = RoboNinjaGame.HEIGHT_IN_TILES - rawHeight;


        if (state == STATE_GROWING) {
            bounds.height = rawInverse;
            bounds.y = rawHeight;
        } else if (state == STATE_FULL) {
            bounds.height = RoboNinjaGame.HEIGHT_IN_TILES;
            bounds.y = 0;
        } else if (state == STATE_SHRINKING) {
            bounds.height = rawHeight;
            bounds.y = rawInverse;
        } else if (state == STATE_PREPPING) {
            bounds.height = 2;
            bounds.y = RoboNinjaGame.HEIGHT_IN_TILES - 2;
        }

    }

    @Override
    public boolean collideWithMcLaser(GameState state) {
        return false;
    }
}
