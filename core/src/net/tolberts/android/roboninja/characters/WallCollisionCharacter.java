package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.math.Rectangle;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.characters.GameCharacter;

/**
 * All "enemies" that cause a wall collision instead of death
 */
public abstract class WallCollisionCharacter extends Enemy {

    /**
     * Returns a limited rectangle that shows bounds of the part of the wallCollisionCharacter
     * that the character collided with.
     * @return null if there is no collision, or a rectangle if the bounds are set.
     */
    public Rectangle getCollisionBlockBounds(GameCharacter collider) {
        if (collidesWith(collider)) {
            return bounds;
        } else {
            return null;
        }
    }

    @Override
    public void collideWithMc(GameState state) {
        //(I think we do nothing here, and instead handle it in the MC's movement routines)
    }



}
