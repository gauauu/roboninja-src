package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;
import net.tolberts.android.game.levels.Level;

import java.util.Map;

public class EnemyLaser extends Enemy {

    final static Vector2 frameSize = new Vector2(32, 32);
    public static final String ID = "enemyLaser";
    public static final String SPEED = "speed";

    protected float speed = -25;
    private float deadTimer = -1;


    public EnemyLaser() {
        deadTimer = -1;
        textureOffset.y = -0.9f;
        bounds.width = 2.0f;
        bounds.height = 0.2f;
        initGraphics();
        setCurrentAnimation("main");
    }

    public void setSpeed(float newSpeed) {
        speed = newSpeed;
    }

    @Override
    public void collideWithMc(GameState state) {
        state.mc.die();
    }

    @Override
    protected String getTextureName() {
        return "laser";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(1.0f, textureRegions[0][0]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("main", anim);
    }

    @Override
    public void updateCharacter(float delta) {


        if (deadTimer > 0) {
            deadTimer -= delta;
            if (deadTimer <= 0) {
                dead = true;
            }
        }

        bounds.x += speed * delta;
        if (bounds.x < -1) {
            dead = true;
        }

        if (bounds.x > gameState.level.getWidth()) {
            dead = true;
        }

        if (collidesWithTile(gameState.level) == Level.COLLISION_WALL) {
            float offset = bounds.x - (long) bounds.x;

            double shrink = 0;
            if (speed < 0) {
                shrink = 1.1 - offset;
                bounds.x += shrink;
            } else {
                shrink = offset + 0.1f;
            }
            bounds.width -= shrink;
            if (shrink < bounds.width) {
                textureScale.x *= (bounds.width - shrink) / bounds.width;
            } else {
                dead = true;
            }

        }

    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {
        if (SPEED.equals(prop)) {
            try {
                speed = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error("robo-ninja", e.getMessage());
            }
        }

    }

}
