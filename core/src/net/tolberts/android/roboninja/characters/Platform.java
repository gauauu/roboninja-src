package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.renderers.BatchTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Map;

public class Platform extends WallCollisionCharacter {

    public final static String ID = "platform";

    public final static String PROP_DIRECTION = "direction";
    public final static String PROP_HAS_EDGES = "hasEdges";
    public final static String PROP_WIDTH = "width";
    public final static String PROP_EDGE_HEIGHT = "edgeHeight";
    public final static String PROP_TEXTURE = "texture";

//    public final static String DIRECTION_VERTICAL = "vertical";
    public final static String DIRECTION_HORIZONTAL = "horizontal";

//    public final static String HAS_EDGES_BOTH = "both";
    public final static String HAS_EDGES_LEFT = "left";
    public final static String HAS_EDGES_RIGHT = "right";
    public final static String HAS_EDGES_NONE = "none";




    protected Texture wallComponent;


    boolean vertical = true;
    boolean edgeLeft = true;
    boolean edgeRight = true;

//    float actualX = 0;
//    float actualWidth = 0;
//    float actualY = 0;

    Rectangle actualBounds = new Rectangle();
    Rectangle leftEdge = new Rectangle();
    Rectangle rightEdge = new Rectangle();

    float speed = 5;
    private float edgeHeight = 1;

    public Platform() {
    }

    @Override
    protected void initGraphics() {
        wallComponent = Art.getTexture("red-square");
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void setProperty(String prop, String value) {
        if (propertyMatch(PROP_DIRECTION, DIRECTION_HORIZONTAL, prop, value)) {
            vertical = false;
        }

        if (propertyMatch(PROP_HAS_EDGES, HAS_EDGES_LEFT, prop, value)) {
            edgeRight = false;
        }
        if (propertyMatch(PROP_HAS_EDGES, HAS_EDGES_RIGHT, prop, value)) {
            edgeLeft = false;
        }
        if (propertyMatch(PROP_HAS_EDGES, HAS_EDGES_NONE, prop, value)) {
            edgeLeft = false;
            edgeRight = false;
        }

        if (PROP_TEXTURE.equals(prop)) {
            wallComponent = Art.getTexture(value);
        }

        //the width property only really makes sense if we're in horizontal mode
        if (PROP_WIDTH.equals(prop)) {
            try {
                actualBounds.width = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parses platform width of " + value);
            }
        }

        if (PROP_EDGE_HEIGHT.equals(prop)) {
            try {
                edgeHeight = Float.parseFloat(value);
            } catch (NumberFormatException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not parses edge height of " + value);
            }
        }


    }

    @Override
    public void setDefinitionBounds(Rectangle unscaledDefBounds) {
        //this always happens AFTER all properties are set, so we can make assumptions based on whether "vertical"
        //is set
        //(I really need to force that by some sort of contract probably)
        super.setDefinitionBounds(unscaledDefBounds);


        actualBounds.x = this.definitionBounds.x;
        actualBounds.y = this.definitionBounds.y;

        if (vertical) {
            actualBounds.width = this.definitionBounds.width;
        }
        actualBounds.height = 1;

    }

    @Override
    public void render(BatchTiledMapRenderer renderer) {

        Batch batch = renderer.getSpriteBatch();
        //draw the main platform part
        batch.draw(wallComponent, actualBounds.x, actualBounds.y, actualBounds.width, 1);

        //and the edges
        if (edgeLeft) {
            batch.draw(wallComponent, actualBounds.x, actualBounds.y + 1, 1, edgeHeight);
        }
        if (edgeRight) {
            batch.draw(wallComponent, actualBounds.x + actualBounds.width - 1, actualBounds.y + 1, 1, edgeHeight);
        }

    }

    @Override
    protected String getTextureName() {
        return null;
    }

    @Override
    protected Vector2 getFrameSize() {
        return null;
    }

    public Rectangle getCollisionBlockBounds(GameCharacter collider) {
        if (collider.bounds.overlaps(actualBounds)) {
            return actualBounds;
        }
        if (edgeLeft && collider.bounds.overlaps(leftEdge)) {
            return leftEdge;
        }
        if (edgeRight && collider.bounds.overlaps(rightEdge)) {
            return rightEdge;
        }

        return null;
    }


    @Override
    protected void initAnimations(Map<String, Animation> animations) {

    }

    @Override
    public void updateCharacter(float delta) {
        if (vertical) {
            actualBounds.y += speed * delta;
            if (actualBounds.y > definitionBounds.y + definitionBounds.height - 1) {
                speed = -speed;
                actualBounds.y = definitionBounds.y + definitionBounds.height - 1;
            }
            if (actualBounds.y < definitionBounds.y) {
                speed = -speed;
                actualBounds.y = definitionBounds.y;
            }
            if (gameState.mc.glueToPlatform == this) {
                gameState.mc.bounds.y += speed * delta;
            }

        } else {
            actualBounds.x += speed * delta;
            if (actualBounds.x < definitionBounds.x) {
                speed = -speed;
                actualBounds.x = definitionBounds.x;
            }
            if (actualBounds.x + actualBounds.width > (definitionBounds.x + definitionBounds.width)) {
                speed = -speed;
                actualBounds.x = (definitionBounds.x + definitionBounds.width) - actualBounds.width;
            }
            if (gameState.mc.glueToPlatform == this && (!gameState.mc.glueToPlatformVerticalOnly)) {
                gameState.mc.bounds.x += speed * delta;
            }
        }


        updateEdgeRectangles();

    }

    protected void updateEdgeRectangles() {
        leftEdge.y = actualBounds.y + 1;
        leftEdge.x = actualBounds.x;
        leftEdge.height = edgeHeight;
        leftEdge.width = 1;

        rightEdge.y = actualBounds.y + 1;
        rightEdge.x = actualBounds.x + actualBounds.width - 1;
        rightEdge.height = edgeHeight;
        rightEdge.width = 1;
    }
}
