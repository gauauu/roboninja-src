package net.tolberts.android.roboninja.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.NPC;

import java.util.Map;

public class PortableRespawnCharacter extends NPC {

    final static Vector2 frameSize = new Vector2(64, 64);

    public PortableRespawnCharacter() {
        textureScale.x = 0.25f;
        textureScale.y = 0.25f;
        initGraphics();
        setCurrentAnimation("respawn");
    }

    @Override
    public void collideWithMc(GameState state) {
        //do nothing
    }

    @Override
    protected String getTextureName() {
        return "respawn";
    }

    @Override
    protected Vector2 getFrameSize() {
        return frameSize;
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {
        Animation anim = new Animation(0.25f, textureRegions[0][0]);
        anim.setPlayMode(Animation.PlayMode.LOOP);
        animations.put("respawn", anim);

    }

    @Override
    public void updateCharacter(float delta) {
        //do nothing

    }
}
