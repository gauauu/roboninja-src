package net.tolberts.android.roboninja.characters;

import java.util.Map;

import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.Enemy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;

public class Boulder extends Enemy {

	final static Vector2 frameSize = new Vector2(32, 32);
	public static final String ID = "boulder";
	public static final String FALL_RATE = "fallRate";

	protected float fallRate = -25;

	public Boulder() {
		initGraphics();
		setCurrentAnimation("main");
	}

	@Override
	public void collideWithMc(GameState state) {
		state.mc.die();
	}

	@Override
	protected String getTextureName() {
		return "boulder";
	}

	@Override
	protected Vector2 getFrameSize() {
		return frameSize;
	}

	@Override
	protected void initAnimations(Map<String, Animation> animations) {
		Animation anim = new Animation(1.0f, textureRegions[0][0]);
		anim.setPlayMode(Animation.PlayMode.LOOP);
		animations.put("main", anim);
	}

	@Override
	public void updateCharacter(float delta) {
        bounds.width = 2.0f;
        bounds.height = 2.0f;
		velocity.y = fallRate;
		applyPhysics(delta, true);
		if (bounds.y < -1) {
			bounds.y = gameState.level.getHeight();
		}
	}

	@Override
	public String getId() {
		return ID;
	}

	@Override
	public void setProperty(String prop, String value) {
		if (FALL_RATE.equals(prop)) {
			try {
				fallRate = Float.parseFloat(value);
			} catch (NumberFormatException e) {
				Gdx.app.error("robo-ninja", e.getMessage());
			}
		}

	}

    @Override
    public boolean doesRespawn() {
        return true;
    }
}
