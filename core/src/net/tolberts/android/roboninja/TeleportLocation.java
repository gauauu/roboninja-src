package net.tolberts.android.roboninja;

import com.badlogic.gdx.Gdx;

public class TeleportLocation {
    public String name;
    public String levelId;
    public float minimapX;
    public float minimapY;
    public float actualY;   //when we're teleporting to the level, we don't know the level height to reverse
                            //the Y direction with. So just save it.


    @Override
    public String toString() {
        return name + "/" + levelId + "/" + minimapX + "/" + minimapY + "/" + actualY;
    }

    public TeleportLocation() {

    }

    public TeleportLocation(String fromString) {
        String[] split = fromString.split("/");
        if (split.length != 5) {
            Gdx.app.error(RoboNinjaGame.TAG, "Could not parse teleporter location: " + fromString);
        } else {
            name = split[0];
            levelId = split[1];
            minimapX = Float.parseFloat(split[2]);
            minimapY = Float.parseFloat(split[3]);
            actualY = Float.parseFloat(split[4]);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof TeleportLocation)) {
            return false;
        }
        if (name == null) {
            return false;
        }
        if (levelId == null) {
            return false;
        }
        TeleportLocation other = (TeleportLocation) o;
        return (name.equals(other.name) && levelId.equals(other.levelId) && minimapX == other.minimapX && minimapY == minimapY);
    }

    @Override
    public int hashCode() {
        int result = ((name == null) ? 0 : name.hashCode());
        result = 31 * result + ((levelId == null) ? 0 : levelId.hashCode());
        result = 31 * result + (minimapX != +0.0f ? Float.floatToIntBits(minimapX) : 0);
        result = 31 * result + (minimapY != +0.0f ? Float.floatToIntBits(minimapY) : 0);
        return result;
    }
}
