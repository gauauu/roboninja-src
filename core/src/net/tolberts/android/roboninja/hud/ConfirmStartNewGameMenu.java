package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.*;

abstract public class ConfirmStartNewGameMenu extends Group {

	public final static int MARGIN = 50;

	public ConfirmStartNewGameMenu() {

		ActorFrame frame = new ActorFrame();
		frame.setBoundsByMargin(MARGIN);
		frame.setStyle(ActorFrame.STYLE_NONE);
		addActor(frame);

        ParagraphActor text = new ParagraphActor("Starting a new game will erase\n" +
                "your currently saved progress!\n" +
                "\n" +
                "Are you sure you\n want to start over?"
                , Fonts.HEADER_FONT, 8);
        text.setFontScale(Fonts.FONT_SCALE_TINY);
        text.centerAt(frame.getWidth() / 2, 180);

		MenuActor menuActor = new MenuActor();
        menuActor.setChildFontScale(Fonts.FONT_SCALE_HEADER_BUTTONS);
        menuActor.setButtonWidth(350);
		menuActor.setPosition(MARGIN + 10, RoboNinjaGame.HEIGHT - MARGIN * 4 - 20);
		menuActor.setWidth(RoboNinjaGame.WIDTH - (MARGIN + 10) * 2);
		menuActor.addMenuItem(new MenuItemActor("Yes, Start Over", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
                        onSelect(true);
					}
				}));
		menuActor.addMenuItem(new MenuItemActor("No, Nevermind", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
                        onSelect(false);
					}
				}));

        frame.addActor(text);
		addActor(menuActor);

	}

	protected abstract void onSelect(boolean result);

}
