package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class BackButtonHandler implements InputProcessor {
	
	static BackButtonHandler handler;
	
	private Hud hud;
	
	public static BackButtonHandler getInstance(Hud hud) {
		
		Gdx.input.setCatchBackKey(true);
		
		if (handler == null || (handler.hud != hud)) {
			handler = new BackButtonHandler(hud);
		}
		return handler;
		
	}
	
	private BackButtonHandler(Hud hud) {
		this.hud = hud;
	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Keys.BACK || keycode == Keys.ESCAPE){
			hud.showConfirmExit();
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
