package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import net.tolberts.android.game.loaders.Art;

public class BigTitleAnimatedNinja extends Actor {

    Texture ninjaAnimation;
    private TextureRegion[][] textureRegions;
    private Animation animation;
    private float frameTick = 0.0f;

    public BigTitleAnimatedNinja() {
        initTextures();
    }

    @Override
    public void act(float delta) {
        frameTick += delta;
    }

    protected void initTextures() {
        ninjaAnimation = Art.getTexture("roboninja");
        ninjaAnimation.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        textureRegions = TextureRegion.split(ninjaAnimation, 128, 128);

        TextureRegion[][] tr = textureRegions;

        animation = new Animation(0.10f, tr[3][0],
                tr[3][1],
                tr[3][2],
                tr[3][3],
                tr[3][4],
                tr[3][5],
                tr[3][6],
                tr[3][7]
        );

    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureRegion keyFrame = animation.getKeyFrame(frameTick, true);
        batch.draw(keyFrame, getX(), getY(), 100, 100);
    }
}
