package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.ParagraphActor;
import net.tolberts.android.roboninja.screens.actors.TextActor;

public abstract class HudMessagePanel extends Group {

    protected static final float MINIMUM_SHOW_TIME = 1.5f;
    protected final static int BORDER_X = 40;
    protected final static int BORDER_Y = 40;
    protected ActorFrame frame;
    protected float shownCounter;
    TextActor title;
    ParagraphActor detailText;


    public HudMessagePanel() {
        super();
        initPanel();
    }

    public HudMessagePanel(String title, String body) {
        super();
        initPanel();
        setTitle(title);
        setBody(body);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setBody(String body) {
        detailText.setText(body);
    }



    public void initPanel() {
        frame = new ActorFrame();
        frame.setStyle(ActorFrame.STYLE_GREEN_FRAME);

        setWidth(RoboNinjaGame.WIDTH - BORDER_X * 2);
        setHeight(RoboNinjaGame.HEIGHT - BORDER_Y * 2);
        setPosition(BORDER_X, BORDER_Y);

        title = new TextActor("title", Fonts.HEADER_FONT);
        title.setPosition(50, getHeight() - 50);
        title.setScale(Fonts.FONT_SCALE_HEADER);

        detailText = new ParagraphActor("body", Fonts.SMALL_FONT_BLACK, 8);
        detailText.setPosition(50, getHeight() - 200);
        detailText.setFontScale(Fonts.FONT_SCALE_SMALLER);

        addActor(frame);
        addActor(title);
        addActor(detailText);

        addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (shownCounter > MINIMUM_SHOW_TIME) {
                    dismissed();
                }
            }
        });
    }

    public void show() {
        setVisible(true);
        shownCounter = 0;
        AudioPlayer.playSound(Audio.ACQUIRE_ITEM);
    }

    protected abstract void dismissed();

    @Override
    public void act(float delta) {

        shownCounter += delta;

        if (!isVisible()) {
            return;
        }

        super.act(delta);
        frame.setBounds(0, 0, getWidth(), getHeight());
        title.setFontScale(0.85f);
        title.centerAt(getWidth() / 2 + 4, getHeight() / 4 * 3);
        detailText.centerAt(getWidth() / 2, getHeight() / 2);
        if (title.getText().length() > 1 && detailText.getHeight() > getHeight() / 2) {
            detailText.setY(detailText.getY() - 25);

        }

        // wait a bit to make sure this isn't accidentally dismissed
        if (Input.wasSpacebarPressed() && (shownCounter > MINIMUM_SHOW_TIME)) {
            dismissed();
        }

    }
}
