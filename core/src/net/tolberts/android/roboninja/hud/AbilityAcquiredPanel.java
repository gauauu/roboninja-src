package net.tolberts.android.roboninja.hud;

import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.mc.AbilityFactory;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.mc.abilities.consumable.ConsumableAbility;
import net.tolberts.android.roboninja.mc.abilities.ground.JumpAbility;

public abstract class AbilityAcquiredPanel extends HudMessagePanel {


    public AbilityAcquiredPanel(String itemId) {
        super();

        if (itemId != null && itemId.startsWith(RoboNinjaProgress.COLLECTIBLES)) {
            String[] split = itemId.split("/");
            if (split.length == 2) {
                String message = "Found Collectible Module #" + split[1] + " of " + RoboNinjaProgress.MAX_COLLECTIBLES;
                setTitle("Collectible");
                if (split[1].equals(Integer.toString(RoboNinjaProgress.MAX_COLLECTIBLES))) {
                    setBody(message + "\n\nYou found all the collectibles!");
                } else {
                    setBody(message);
                }
            }
            return;
        }

        McAbility ability = AbilityFactory.getAbility(itemId);

        setTitle(ability.getName());
        String message = ability.getAcquiredMessage();
        if (!(ability instanceof JumpAbility) && !(ability instanceof ConsumableAbility) ) {
            message = message + "\nYou can change your selected abilities by\ntapping the icon in the upper-left corner.\n";
        }
        setBody(message);
    }

    @Override
    public void show() {
        super.show();
        AudioPlayer.playSound(Audio.ACQUIRE_ITEM);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        detailText.setFontScale(Fonts.FONT_SCALE_TINY);
    }
}
