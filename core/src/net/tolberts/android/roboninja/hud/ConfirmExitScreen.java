package net.tolberts.android.roboninja.hud;

import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.MenuActor;
import net.tolberts.android.roboninja.screens.actors.MenuItemActor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;

abstract public class ConfirmExitScreen extends Group {
	
	public final static int MARGIN = 50;

	public ConfirmExitScreen() {

		ActorFrame frame = new ActorFrame();
		frame.setBoundsByMargin(MARGIN);
		frame.setStyle(ActorFrame.STYLE_NONE);
		addActor(frame);

		MenuActor menuActor = new MenuActor();
        menuActor.setButtonWidth(350);
		menuActor.setPosition(MARGIN + 10, RoboNinjaGame.HEIGHT - MARGIN * 3);
		menuActor.setWidth(RoboNinjaGame.WIDTH - (MARGIN + 10) * 2);
		menuActor.setChildFontScale(Fonts.FONT_SCALE_HEADER_BUTTONS);
		menuActor.addMenuItem(new MenuItemActor("Quit Robo-Ninja", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
						Gdx.app.exit();
					}
				}));
		menuActor.addMenuItem(new MenuItemActor("Main Menu", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
						mainMenu();
					}
				}));
		menuActor.addMenuItem(new MenuItemActor("Keep Playing", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
						dismiss();
					}
				}));
		addActor(menuActor);

	}

	protected abstract void dismiss();
	protected abstract void mainMenu();

}
