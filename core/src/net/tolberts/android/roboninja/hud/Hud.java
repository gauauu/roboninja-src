package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.tolberts.android.game.GameState;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.screens.GameScreen;
import net.tolberts.android.roboninja.screens.MinimapScreen;
import net.tolberts.android.roboninja.screens.TeleportScreen;
import net.tolberts.android.roboninja.screens.actors.RoboNinjaStage;

public class Hud {

    public static final String HUD_SHOW_TYPE_ITEM = "item";
    public static final String HUD_SHOW_GENERAL_PANEL = "panel";

    RoboNinjaGameState gameState;
    GameScreen gameScreen;
    Stage stage;
    ItemSelectorFrame itemFrame;
    ConsumableItemSelectorFrame consumableFrame;
    ReadyBox readyBox;
    Countdown countdown;

    Group exitScreen;

    private AbilityAcquiredPanel abilityAcquiredPanel;
    private HudMessagePanel generalPanel;

    private int tempGameState;

    private InputMultiplexer inputMultiplexer;

    public Hud(GameScreen gameScreen) {
        gameState = (RoboNinjaGameState) gameScreen.gameState;
        this.gameScreen = gameScreen;
        stage = new RoboNinjaStage();


        itemFrame = new ItemSelectorFrame(gameState);
//        itemFrame.setBounds(-20, RoboNinjaGame.HEIGHT - 80, 80, 100);

        consumableFrame = new ConsumableItemSelectorFrame(gameState);
//        consumableFrame.setBounds(RoboNinjaGame.WIDTH + 20, RoboNinjaGame.HEIGHT - 80, 80, 100);

        readyBox = new ReadyBox() {
            protected void dismissed() {
                gameState.currentMode = GameState.MODE_COUNTDOWN;
            }

            protected void clearCheckpoint() {
                if (gameState.getProgress().isPortableRespawning) {
                    gameState.getProgress().isPortableRespawning = false;
                }
            }

            @Override
            protected void viewMinimap() {
                Screen currentScreen = gameState.game.getScreen();
                MinimapScreen minimap = new MinimapScreen(gameState);
                minimap.setPreviousScreen(currentScreen);
                gameState.game.setScreen(minimap);
            }

        };
        readyBox.setPosition(0, 0);
        readyBox.setSize(RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT);
        readyBox.setVisible(false);

        stage.addActor(readyBox);
        stage.addActor(itemFrame);
        stage.addActor(consumableFrame);


        countdown = new Countdown() {
            protected void timerFinished() {
                if (gameState.currentMode == GameState.MODE_COUNTDOWN) {
                    gameState.currentMode = GameState.MODE_ACTION;
                }
            }
        };
        countdown.setPosition(0, 0);
        stage.addActor(countdown);

        inputMultiplexer = new InputMultiplexer(BackButtonHandler.getInstance(this), stage);
        Gdx.input.setInputProcessor(inputMultiplexer);

    }

    public void render(float delta) {
        stage.draw();
        stage.act();
    }

    public void update(float delta) {
        Gdx.input.setInputProcessor(inputMultiplexer);

        //see if we need to show the ready box
        readyBox.setVisible(gameState.currentMode == GameState.MODE_READY);
        readyBox.setShowClearCheckpoint(gameState.getProgress().isPortableRespawning);
        readyBox.setDeaths(gameState.getProgress().getNumDeaths());
        readyBox.setTime(gameState.getProgress().getPlayTime());

        //see if we need to show the countdown
        if (gameState.currentMode == GameState.MODE_COUNTDOWN) {
            countdown.doCountdown();
        }

        //if we need to show some sort of hud/ui box...
        if (gameState.currentMode == GameState.MODE_HUD) {
            processAndShowModeHud(gameState.hudType, gameState.hudDetail);
        }

        //or else the teleporter controls
        if (gameState.currentMode == GameState.MODE_TELEPORTING) {
            showTeleportScreen();
        }
    }

    private void showTeleportScreen() {
        Screen currentScreen = gameState.game.getScreen();
        final TeleportScreen teleportScreen = new TeleportScreen(gameState);
        teleportScreen.setPreviousScreen(currentScreen);
        teleportScreen.setOnClose(new HudCallback() {
            @Override
            public void callback() {
                gameState.currentMode = GameState.MODE_COUNTDOWN;
                // this shouldn't be here, but I don't have a handle to the teleporter enemy
                // at this point. I'm not going to refactor just for this, but this might be a symptom
                // of a bad design -- if it gets uglier, then this should be the first to be moved.
                gameState.mc.freezeHorizontal = false;
                gameState.mc.visible = true;
                if (teleportScreen.wasCanceled) {
                    gameState.setRespawnAtCurrent();
                }

            }
        });
        gameState.game.setScreen(teleportScreen);
    }


    private void processAndShowModeHud(String hudType, String hudDetail) {
        if (Hud.HUD_SHOW_TYPE_ITEM.equals(hudType)) {
            showAbilityAcquiredPanel(hudDetail);
        } else if (Hud.HUD_SHOW_GENERAL_PANEL.equals(hudType)) {
            //i don't like this hack. I should have designed the interface between the gamestate and hud differently
            //(Using a cleaner interface, instead of the gamestate having a few variables to hold the hud state)
            String[] titleAndDetail = hudDetail.split("/");
            showGeneralPanel(titleAndDetail[0], titleAndDetail[1]);
        } else {
            gameState.currentMode = GameState.MODE_COUNTDOWN;
        }
    }



    public void showGeneralPanel(String title, String detail) {
        if ((generalPanel != null) && generalPanel.isVisible()) {
            return;
        }

        String next = null;
        if (detail.contains("--EASY--")) {
            String[] parts = detail.split("--EASY--");
            detail = parts[0];
            if (gameState.speedMode == GameState.SPEED_MODE_EASY) {
                next = parts[1];
            }
        }
        final String nextPart = next;

        generalPanel = new HudMessagePanel(title, detail) {
            @Override
            protected void dismissed() {
                gameState.currentMode = GameState.MODE_COUNTDOWN;
                remove();
                setVisible(false);

                //if there's chained messages...
                if (nextPart != null) {
                    gameState.currentMode = GameState.MODE_PAUSE;
                    showGeneralPanel("", nextPart);
                } else if (gameState.hudOnClose != null) {
                    //otherwise, call the onClose if there is one....
                    gameState.hudOnClose.callback();
                    gameState.hudOnClose = null;
                }
            }
        };
        generalPanel.show();
        stage.addActor(generalPanel);

    }

    private void showAbilityAcquiredPanel(String hudDetail) {
        if ((abilityAcquiredPanel != null) && abilityAcquiredPanel.isVisible()) {
            return;
        }
        abilityAcquiredPanel = new AbilityAcquiredPanel(hudDetail) {
            @Override
            protected void dismissed() {
                gameState.currentMode = GameState.MODE_COUNTDOWN;
                remove();
                setVisible(false);
            }
        };
        abilityAcquiredPanel.show();
        stage.addActor(abilityAcquiredPanel);

    }

    public void showConfirmExit() {
        if (exitScreen != null) {
            return;
        }
        tempGameState = gameState.currentMode;
        gameState.currentMode = GameState.MODE_HUD_MENU;
        exitScreen = new ConfirmExitScreen() {
            protected void dismiss() {
                gameState.currentMode = tempGameState;
                if (gameState.currentMode == GameState.MODE_ACTION || gameState.currentMode == GameState.MODE_COUNTDOWN) {
                    gameState.currentMode = GameState.MODE_COUNTDOWN;
                }
                remove();
                exitScreen = null;
            }

            @Override
            protected void mainMenu() {
                remove();
                exitScreen = null;
                gameState.currentMode = GameState.MODE_TRIGGER_MAIN_MENU;
            }
        };
        stage.addActor(exitScreen);
    }

}
