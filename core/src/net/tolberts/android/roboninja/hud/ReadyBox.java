package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.SmallButton;
import net.tolberts.android.roboninja.screens.actors.TextActor;

public abstract class ReadyBox extends Group {

    private final TextActor deathsLabel;
    private final TextActor timeLabel;
    private final SmallButton cancelButton;
    TextActor label1;
    TextActor label2;
    ActorFrame frame;

    private boolean shouldShowClearCheckpoint;

    //    private int deaths = 0;
    private float time;

    Image deathsIcon;
    Image timeIcon;

    public ReadyBox() {

        label1 = new TextActor("Ready", Fonts.HEADER_FONT);
        label1.setPosition(0, 0);

        label2 = new TextActor("Tap to continue", Fonts.SMALL_FONT_BLACK);
        label2.setPosition(50, getHeight() - 200);
        label2.setFontScale(Fonts.FONT_SCALE_MED);

        deathsLabel = new TextActor("Deaths", Fonts.HEADER_FONT);
        deathsLabel.setPosition(50, getHeight() - 250);
        deathsLabel.setFontScale(Fonts.FONT_SCALE_SMALLER);

        timeLabel = new TextActor("Time", Fonts.HEADER_FONT);
        timeLabel.setPosition(50, getHeight() - 300);
        timeLabel.setFontScale(Fonts.FONT_SCALE_SMALLER);

        deathsIcon = new Image(Art.getTexture("deaths-icon"));
        timeIcon = new Image(Art.getTexture("time-icon"));

        frame = new ActorFrame();
        frame.setStyle(ActorFrame.STYLE_GREEN_FRAME);
        frame.setBounds(50, 50, getWidth() - 100, getHeight() - 100);

        addActor(frame);
        frame.addActor(label1);
        frame.addActor(label2);
        frame.addActor(deathsLabel);
        frame.addActor(timeLabel);
        frame.addActor(deathsIcon);
        frame.addActor(timeIcon);


        HudTapListener dismissListener = new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                dismissed();
            }
        };
        frame.addListener(dismissListener);
        label1.addListener(dismissListener);
        label2.addListener(dismissListener);

//		clearPortableCheckpointBox = new ActorFrame();
//		checkpointLabel = new TextActor("Clear Checkpoint", Fonts.SMALL_FONT);
//		checkpointLabel.setFontScale(0.6f);
//		clearPortableCheckpointBox.setVisible(false);
//		addActor(clearPortableCheckpointBox);
//		clearPortableCheckpointBox.addActor(checkpointLabel);
//        clearPortableCheckpointBox.addListener(new HudTapListener() {
//            @Override
//            public void onTap(float x, float y) {
//                clearCheckpoint();
//            }
//        });
//        cancelImage = new Image(Art.getAsDrawable("cancel-respawn"));
//        clearPortableCheckpointBox.addActor(cancelImage);
//        cancelImage.scaleBy(0.75f);


        SmallButton mapButton = new SmallButton("map-icon", new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                viewMinimap();
            }
        });
        mapButton.setStyle(SmallButton.STYLE_CLEAN);
        mapButton.setIconOffset(20, 9);
        mapButton.setPosition(0, 0);
        mapButton.setScale(0.8f);

        addActor(mapButton);

        cancelButton = new SmallButton("cancel-respawn-btn", new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                clearCheckpoint();
            }
        });
        cancelButton.setStyle(SmallButton.STYLE_CLEAN);
        cancelButton.setIconOffset(20, 9);
        cancelButton.setPosition(RoboNinjaGame.WIDTH - cancelButton.getWidth(), 20);
        cancelButton.setScale(0.8f);
        addActor(cancelButton);

    }

    protected abstract void dismissed();

    protected abstract void clearCheckpoint();

    protected abstract void viewMinimap();

    @Override
    public void act(float delta) {

        if (!isVisible()) {
            return;
        }
        cancelButton.setPosition(RoboNinjaGame.WIDTH - cancelButton.getWidth() + 5, 0);
        cancelButton.setIconScale(0.65f);
        cancelButton.setIconOffset(35, 15);
        cancelButton.setVisible(shouldShowClearCheckpoint);

        frame.setPosition(50, 75);
        frame.setWidth(RoboNinjaGame.WIDTH - 100);
        frame.setHeight(160);

        //this is here also so we can
        //adjust things if the width/height of the readybox changes
        super.act(delta);
        label1.setFontScale(Fonts.FONT_SCALE_MED);
        label1.centerAt(frame.getWidth() / 2, 110);
        label2.setFontScale(Fonts.FONT_SCALE_SMALL);
        label2.centerAt(frame.getWidth() / 2, 80);

        deathsLabel.setPosition(90, 30);

        deathsIcon.setPosition(40, 25);
        deathsIcon.setSize(40, 40);
        deathsIcon.setScaling(Scaling.stretch);

        timeIcon.setPosition(225, 25);
        timeIcon.setSize(40, 40);
        timeIcon.setScaling(Scaling.stretch);

        int seconds = (int) time;
        String timeStr = MiscUtils.getTimeString(seconds);


        timeLabel.setText(timeStr);
        timeLabel.setPosition(285, 30);

        if (Input.wasSpacebarPressed()) {
            Input.ignoreUntilRelease();
            dismissed();
        }

//		clearPortableCheckpointBox.setVisible(shouldShowClearCheckpoint);
//		clearPortableCheckpointBox.setBounds(140, -200, getWidth() + 1, getHeight() + 10);
//		checkpointLabel.setPosition(60, getHeight() - 22);
//        cancelImage.setPosition(10,  clearPortableCheckpointBox.getHeight() - 45);
//        cancelImage.setScale(0.5f);


    }

    public void setShowClearCheckpoint(boolean isPortableRespawning) {
        shouldShowClearCheckpoint = isPortableRespawning;
    }


    public void setDeaths(int numDeaths) {
        deathsLabel.setText("" + numDeaths);
    }

    public void setTime(float time) {
        this.time = time;
    }
}
