package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;

public class GeneralBackButtonHandler implements InputProcessor {


	private  HudCallback callback;

	public GeneralBackButtonHandler(HudCallback callback)
	{
		this.callback = callback;
        Gdx.input.setCatchBackKey(true);
	}

	@Override
	public boolean keyDown(int keycode) {
        if (callback == null) {
            return false;
        }
		if(keycode == Keys.BACK || keycode == Keys.ESCAPE){
            callback.callback();
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

    public void setCallback(HudCallback callback) {
        this.callback = callback;
    }
}
