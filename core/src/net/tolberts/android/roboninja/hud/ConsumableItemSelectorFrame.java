package net.tolberts.android.roboninja.hud;

import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.mc.abilities.consumable.ConsumableAbility;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;

import java.util.HashMap;
import java.util.Map;

public class ConsumableItemSelectorFrame extends ActorFrame {

    public static final int ICON_SIZE = 32;
    public final int ICON_MARGIN_X = 32;
    public final int ICON_MARGIN_Y = 27;

    private Map<String, ConsumableItemIconSelector> selectors = new HashMap<String, ConsumableItemIconSelector>();

    private RoboNinjaGameState gameState;

    public ConsumableItemSelectorFrame(RoboNinjaGameState gameState) {
        setStyle(ActorFrame.STYLE_NONE);
        this.gameState = gameState;
        String[] allConsumableAbilities = ConsumableAbility.getAllConsumableAbilities();
        for (String abilityName : allConsumableAbilities) {
            selectors.put(abilityName, new ConsumableItemIconSelector(abilityName, gameState));
            addActor(selectors.get(abilityName));
        }

        updateUi();
    }

    @Override
    public void act(float delta) {
        updateUi();
        super.act(delta);
    }

    public void updateUi() {
        int frameWidth = 0;
        // see how many ability slots we have
        int i = 0;
        String[] allConsumableAbilities = ConsumableAbility.getAllConsumableAbilities();
        for (String abilityName : allConsumableAbilities) {
            int frameSize = updateAbilityAndGetFrameSize(abilityName, i);
            frameWidth += frameSize;
            if (frameSize > 0) {
                i++;
            }
        }

        if (frameWidth == 0) {
            setVisible(false);
        } else {
            setVisible(true);
            setBounds(RoboNinjaGame.WIDTH - (ICON_SIZE + ICON_MARGIN_X) * frameWidth, RoboNinjaGame.HEIGHT - (ICON_SIZE + ICON_MARGIN_Y * 2),
                    16 + (ICON_MARGIN_X / 2) + (frameWidth * (ICON_SIZE + ICON_MARGIN_X)), 100);
//            setBounds(20, RoboNinjaGame.HEIGHT - (ICON_SIZE + ICON_MARGIN_Y * 2),  16 + (ICON_MARGIN_X / 2) + (frameWidth * ICON_SIZE + ICON_MARGIN_X), 100);
        }
    }

    private int updateAbilityAndGetFrameSize(String consumableName, int positionIndex) {
        if (hasConsumable(consumableName)) {
            ConsumableItemIconSelector selector = selectors.get(consumableName);
            selector.setVisible(true);

            selector.setPosition(getWidth() - ((ICON_MARGIN_X) + (positionIndex + 1) * (ICON_SIZE + ICON_MARGIN_X)), ICON_MARGIN_Y);
            selector.update();

            return 1;
        } else {
            selectors.get(consumableName).setVisible(false);
        }
        return 0;

    }


    private boolean hasConsumable(String consumableName) {
        int numberOfConsumable = gameState.getProgress().getMaxNumberOfConsumables(consumableName);
        return (numberOfConsumable > 0);
    }

}
