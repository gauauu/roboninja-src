package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Scaling;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.loaders.AbilityIcons;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.mc.AbilityFactory;
import net.tolberts.android.roboninja.mc.abilities.consumable.Slowmo;
import net.tolberts.android.roboninja.screens.actors.SmallButton;
import net.tolberts.android.roboninja.screens.actors.TextActor;

public class ConsumableItemIconSelector extends Group {

    public final static float RESPAWN_WIDTH = ConsumableItemSelectorFrame.ICON_SIZE;

    private String itemType;
    private RoboNinjaGameState state;
    private TextActor quantityActor;
    private Image potentialBar;
    private Image filledBar;

    private SmallButton button;

    private final static float QTY_SCALE = Fonts.FONT_SCALE_TINY;


    public ConsumableItemIconSelector(String type, RoboNinjaGameState gameState) {
        super();

        button = new SmallButton(null, new InputListener() {

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!shouldDisableForBoss()) {
                    state.useConsumable(itemType);
                }
                Input.ignoreTap(true);
                return true;
            }
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Input.ignoreTap(false);
            }


        });
        button.setStyle(SmallButton.STYLE_CLEAN);
        button.setIconScale(0.15f);
        button.setIconOffset(170, 105);
        button.setScale(0.85f);

        this.itemType = type;
        this.state = gameState;
        /*
        this.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                state.useConsumable(itemType);
                Input.ignoreTap(true);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Input.ignoreTap(false);
            }
        });
        */
        addActor(button);
        addPicture();
        addQuantity();
        addRespawnProgress();
        setHeight(ConsumableItemSelectorFrame.ICON_SIZE);
        setWidth(ConsumableItemSelectorFrame.ICON_SIZE);
//        setSize(400, 400);
    }

    private void addRespawnProgress() {
        potentialBar = new Image();
        potentialBar.setDrawable(Art.getAsDrawable("red-square"));
        potentialBar.setWidth(RESPAWN_WIDTH);
        potentialBar.setHeight(5.0f);
        potentialBar.setPosition(22.5f,
                ConsumableItemSelectorFrame.ICON_SIZE * (-1.0f / 5.0f) + 15);
        potentialBar.setTouchable(Touchable.disabled);
        addActor(potentialBar);

        filledBar = new Image();
        filledBar.setDrawable(Art.getAsDrawable("greenProgressBar"));
        filledBar.setHeight(5.0f);
        filledBar.setPosition(22.5f,
                ConsumableItemSelectorFrame.ICON_SIZE * (-1.0f / 5.0f) + 15);
        filledBar.setWidth(0.0f);
        filledBar.setTouchable(Touchable.disabled);
        addActor(filledBar);

        updateRespawnWidth();

    }

    private void updateRespawnWidth() {
        if (state.speedMode == GameState.SPEED_MODE_EASY) {
            potentialBar.setVisible(false);
            filledBar.setVisible(false);
            return;
        }
        Float consumableRespawn = state.getProgress().getConsumableRespawn(itemType);
        if (consumableRespawn == null || consumableRespawn < .001f) {
            potentialBar.setVisible(false);
            filledBar.setVisible(false);
        } else {
            potentialBar.setVisible(true);
            filledBar.setVisible(true);
            float respawnTime = AbilityFactory.getConsumable(itemType).getRespawnTime();
            float percent = consumableRespawn / respawnTime;
            filledBar.setWidth(percent * RESPAWN_WIDTH);

        }
    }

    private void addQuantity() {
        quantityActor = new TextActor(getQuantityString(), Fonts.SMALL_FONT);
        quantityActor.setPosition(ConsumableItemSelectorFrame.ICON_SIZE * 0.33f, ConsumableItemSelectorFrame.ICON_SIZE);
        quantityActor.setFontScale(QTY_SCALE);
        //noinspection SpellCheckingInspection
        //quantityActor.setColor(com.badlogic.gdx.graphics.Color.valueOf("00ffff"));
        quantityActor.setColor(Color.WHITE);
        quantityActor.setTouchable(Touchable.disabled);
        addActor(quantityActor);
        updateQuantity();
    }

    private String getQuantityString() {
        if (state.speedMode == GameState.SPEED_MODE_EASY) {
            return "";
        }
        int numberOfConsumable = getQuantity();
        int maxNumberOfConsumables = state.getProgress().getMaxNumberOfConsumables(itemType);
        return "" + numberOfConsumable + " / " + maxNumberOfConsumables;
    }

    private int getQuantity() {
        return state.getProgress().getNumberOfConsumable(itemType);
    }

    private void addPicture() {
        Image image = new Image();
        image.setDrawable(getIconForConsumableType(itemType));
        float iconSize = ConsumableItemSelectorFrame.ICON_SIZE;
        if (Slowmo.ID.equals(itemType)) {
            iconSize *= 0.65f;
            image.setPosition(28, 20);
        } else {
            image.setPosition(24, 13);
        }
        image.setHeight(iconSize);
        image.setWidth(iconSize);
        image.setScaling(Scaling.fit);
        image.setTouchable(Touchable.disabled);
        addActor(image);
    }

    private Drawable getIconForConsumableType(String consumableType) {

        return AbilityIcons.getAsDrawable(consumableType);
    }

    public void updateQuantity() {

        quantityActor.setText(getQuantityString());
//        quantityActor.centerAt(ConsumableItemSelectorFrame.ICON_SIZE / 2, 39);
        quantityActor.font.setScale(QTY_SCALE);
        BitmapFont.TextBounds bounds = quantityActor.font.getBounds(getQuantityString());
        quantityActor.setPosition(button.getWidth()/ 2 - bounds.width / 2, 45);


        if (state.speedMode == GameState.SPEED_MODE_EASY) {
            button.setDisabled(false);
        } else {
            button.setDisabled(getQuantity() == 0);
        }

        //always disable on boss fight
        if (shouldDisableForBoss()) {
            button.setDisabled(true);
        }

    }

    private boolean shouldDisableForBoss() {

        return ("boss".equals(state.getProgress().levelId));
    }

    public void update() {
        updateQuantity();
        updateRespawnWidth();
    }
}
