package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.scenes.scene2d.Group;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.MenuActor;
import net.tolberts.android.roboninja.screens.actors.MenuItemActor;
import net.tolberts.android.roboninja.screens.actors.ParagraphActor;

abstract public class NewGameSelectSpeedMenu extends Group {

	public final static int MARGIN = 50;

	public NewGameSelectSpeedMenu(GameState gameState) {

		ActorFrame frame = new ActorFrame();
		frame.setBoundsByMargin(MARGIN);
		frame.setStyle(ActorFrame.STYLE_NONE);
		addActor(frame);

        ParagraphActor text = new ParagraphActor("Select game mode" , Fonts.HEADER_FONT, 8);
        text.setFontScale(Fonts.FONT_SCALE_SMALL);
        text.centerAt(RoboNinjaGame.WIDTH / 3 + 30, 190);

		MenuActor menuActor = new MenuActor();
		menuActor.setButtonWidth(250);
		menuActor.setPosition(MARGIN + 10, RoboNinjaGame.HEIGHT - MARGIN * 3);
		menuActor.setWidth(RoboNinjaGame.WIDTH - (MARGIN + 10) * 2);
		menuActor.setChildFontScale(Fonts.FONT_SCALE_HEADER_BUTTONS);

		menuActor.addMenuItem(new MenuItemActor("Beginner", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
						onSelect(GameState.SPEED_MODE_EASY);
					}
				}));
		menuActor.addMenuItem(new MenuItemActor("Normal", Fonts.HEADER_FONT,
				new MenuItemActor.SelectedCallback() {
					@Override
					public void selected() {
						onSelect(GameState.SPEED_MODE_NORMAL);
					}
				}));

        if (((RoboNinjaGame)gameState.game).adapterHasFlag(PlatformAdapter.FLAG_ALLOW_FAST_MODE)) {

			menuActor.addMenuItem(new MenuItemActor("Faster", Fonts.HEADER_FONT,
					new MenuItemActor.SelectedCallback() {
						@Override
						public void selected() {
							onSelect(GameState.SPEED_MODE_FAST);
						}
					}));
		}

        frame.addActor(text);
		addActor(menuActor);

	}

	protected abstract void onSelect(int result);

}
