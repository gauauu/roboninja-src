package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.IntMap;
import net.tolberts.android.game.Input;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;

import java.util.LinkedList;

public class ItemSelectorFrame extends ActorFrame {

    public final int ICON_SIZE = 48;
    public final int ICON_MARGIN_X = 28;
    public final int ICON_MARGIN_Y = 15;

    private IntMap<ItemIconSelector> selectors;

    RoboNinjaGameState gameState;

//    private Texture deco;

    public ItemSelectorFrame(RoboNinjaGameState gameState) {

//        deco = Art.getTexture("obj_ui_deco");

        setStyle(ActorFrame.STYLE_NONE);
        this.gameState = gameState;
        selectors = new IntMap<ItemIconSelector>();
        selectors.put(McAbility.TYPE_GROUND, new ItemIconSelector(McAbility.TYPE_GROUND, this));
        addActor(selectors.get(McAbility.TYPE_GROUND));

        selectors.put(McAbility.TYPE_JUMP, new ItemIconSelector(McAbility.TYPE_JUMP, this));
        addActor(selectors.get(McAbility.TYPE_JUMP));

        selectors.put(McAbility.TYPE_WALL, new ItemIconSelector(McAbility.TYPE_WALL, this));
        addActor(selectors.get(McAbility.TYPE_WALL));

        //if clicked anywhere on the frame, ignore it for main events...
        this.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Input.ignoreTap(true);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Input.ignoreTap(false);
            }
        });

        updateUi();
    }

    @Override
    public void act(float delta) {
        updateUi();
        super.act(delta);
    }

    public void updateUi() {
        int frameWidth = 0;
        // see how many ability slots we have
        frameWidth += updateAbilityAndGetFrameSize(McAbility.TYPE_GROUND, frameWidth);
        frameWidth += updateAbilityAndGetFrameSize(McAbility.TYPE_JUMP, frameWidth);
        frameWidth += updateAbilityAndGetFrameSize(McAbility.TYPE_WALL, frameWidth);

        if (frameWidth == 0) {
            setVisible(false);
        } else {
            setVisible(true);
            setBounds(-20, RoboNinjaGame.HEIGHT - (ICON_SIZE + ICON_MARGIN_Y * 2),
                    20 + (ICON_MARGIN_X / 2) + (frameWidth * (ICON_SIZE + ICON_MARGIN_X)), 100);
//            setBounds(-20, -17, 20 + (ICON_MARGIN_X / 2) + (frameWidth * (ICON_SIZE + ICON_MARGIN_X)), 100);
        }

    }

    private int updateAbilityAndGetFrameSize(int abilityType, int frameWidth) {
        if (hasAbilities(abilityType)) {
            ItemIconSelector selector = selectors.get(abilityType);
            selector.setVisible(true);
            selector.setPosition(-5 + (ICON_MARGIN_X / 2) + frameWidth * (ICON_SIZE + ICON_MARGIN_X), ICON_MARGIN_Y + 3);
            String icon = getIconForAbilityType(abilityType);
            selector.setIcon(icon);
            selector.setDisabled(icon == null || icon.endsWith("Blank"));

//            selector.setHeight(ICON_SIZE);
//            selector.setWidth(ICON_SIZE);
            return 1;
        } else {
            selectors.get(abilityType).setVisible(false);
        }
        return 0;

    }

    private String getIconForAbilityType(int abilityType) {
        McAbility currentlySelected = gameState.mc.abilities[abilityType];
        if (currentlySelected == null) {
            gameState.mc.toggleAbility(abilityType);
            currentlySelected = gameState.mc.abilities[abilityType];
        }

        assert currentlySelected != null;
        String id = "wallBlank";
        if (currentlySelected != null) {
            id = currentlySelected.getId();
        }
//        return AbilityIcons.getAsDrawable(id);
        return id;
    }

    private boolean hasAbilities(int abilityType) {
        LinkedList<McAbility> groundAbilities = gameState.getProgress().getAbilitiesOfType(abilityType);
        if (groundAbilities == null) {
            return false;
        }
        if (groundAbilities.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
//        batch.draw(deco, -100, 0, 300, 60);
        super.draw(batch, parentAlpha);
    }

    public void toggleAbility(int type) {
        gameState.mc.toggleAbility(type);
    }
}
