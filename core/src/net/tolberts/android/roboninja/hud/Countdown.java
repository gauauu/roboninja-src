package net.tolberts.android.roboninja.hud;

import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.TextActor;

import com.badlogic.gdx.scenes.scene2d.Group;

public abstract class Countdown extends Group {

	public final static float START_TIME_INACTIVE = -1;
	public final static float START_TIME_START_NOW = -2;

	public final static float TICK_PER = 0.15f;
    //public final static float TICK_PER = 0.0001f;

	TextActor label;
	float timer = START_TIME_INACTIVE;

	public Countdown() {
		label = new TextActor("", Fonts.HEADER_FONT);
		addActor(label);
	}

	@Override
	public void act(float delta) {
		super.act(delta);
        timerFinished();
        setVisible(false);

//		if (timer == START_TIME_INACTIVE) {
//			setVisible(false);
//		} else if (timer == START_TIME_START_NOW) {
//			calcTextEffect(0, 0);
//			setVisible(true);
//			timer = 0.01f;
//		} else {
//			timer += delta;
//			if (timer > (TICK_PER * 5)) {
//				timer = START_TIME_INACTIVE;
//				setVisible(false);
//			} else if (timer > (TICK_PER * 3)) {
//				label.setText("Go!");
//				calcTextEffect(TICK_PER * 3, TICK_PER * 5);
//				timerFinished();
//			} else if (timer > (TICK_PER * 2)) {
//				calcTextEffect(TICK_PER * 2);
//				label.setText("1");
//			} else if (timer > TICK_PER) {
//				calcTextEffect(TICK_PER * 1);
//				label.setText("2");
//			} else if (timer > 0) {
//				calcTextEffect(0);
//				label.setText("3");
//			}
//		}
	}

	private void calcTextEffect(float lastKeyFrame) {
		calcTextEffect(lastKeyFrame, lastKeyFrame + TICK_PER);
	}

	private void calcTextEffect(float lastKeyFrame, float nextKeyFrame) {
		// float percentOfWay = (timer - lastKeyFrame) / (nextKeyFrame -
		// lastKeyFrame);
		// float scaleMin = 0.5f;
		// float scaleMax = 1.0f;
		// float currentScale = (scaleMax - scaleMin) * percentOfWay + scaleMin;
		// label.setFontScale(currentScale);
		// label.alpha = Math.min(1.0f, 2.0f - percentOfWay * 2);
		label.centerAt(RoboNinjaGame.WIDTH / 2 - 15, RoboNinjaGame.HEIGHT - 50);
	}

	public void doCountdown() {
		if (timer < 0) {
			timer = START_TIME_START_NOW;
		}
	}

	protected abstract void timerFinished();

}
