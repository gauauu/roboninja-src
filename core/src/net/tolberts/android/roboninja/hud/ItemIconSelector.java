package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import net.tolberts.android.game.Input;
import net.tolberts.android.roboninja.screens.actors.SmallButton;

public class ItemIconSelector extends SmallButton {




    public ItemIconSelector(final int type, final ItemSelectorFrame parent) {

        super(null, new InputListener() {

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                parent.toggleAbility(type);
                Input.ignoreTap(true);
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Input.ignoreTap(false);
            }

        });


        setStyle(STYLE_CLEAN);
        setScale(0.9f);
        setIconScale(0.12f);
        setIconOffset(207, 128);
	}

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

}