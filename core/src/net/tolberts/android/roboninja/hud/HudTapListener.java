package net.tolberts.android.roboninja.hud;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.util.AudioPlayer;

/**
 * An extension of InputListener that handles making sure the tap inputs don't get passed
 * on to the main game engine input controller, and condenses the touchDown/touchUp
 * into a single onTap method.
 */
public abstract class HudTapListener extends InputListener {

    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        Input.ignoreTap(true);
        return true;
    }

    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        Input.ignoreTap(false);
        AudioPlayer.playSound(Audio.UI_CLICK);
        onTap(x, y);
    }

    public abstract void onTap(float x, float y);


}
