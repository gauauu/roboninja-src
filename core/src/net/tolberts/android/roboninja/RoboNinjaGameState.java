package net.tolberts.android.roboninja;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectIntMap;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.levels.Level;
import net.tolberts.android.game.levels.LevelFactory;
import net.tolberts.android.game.loaders.Maps;
import net.tolberts.android.game.platformadapters.Achievement;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.characters.ItemCharacter;
import net.tolberts.android.roboninja.characters.PortableRespawnCharacter;
import net.tolberts.android.roboninja.characters.Teleporter;
import net.tolberts.android.roboninja.cutscene.Cutscene;
import net.tolberts.android.roboninja.cutscene.CutsceneFinishedCallback;
import net.tolberts.android.roboninja.hud.Hud;
import net.tolberts.android.roboninja.mc.AbilityFactory;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.mc.abilities.air.BlankAirAbility;
import net.tolberts.android.roboninja.mc.abilities.consumable.ConsumableAbility;
import net.tolberts.android.roboninja.mc.abilities.wall.BlankWallAbility;
import net.tolberts.android.roboninja.screens.CutsceneScreen;
import net.tolberts.android.roboninja.screens.EndingStatsScreen;
import net.tolberts.android.roboninja.screens.GameScreen;

public class RoboNinjaGameState extends GameState {

    public final static String STARTING_LEVEL = "temple_1";


    private PortableRespawnCharacter portableRespawn = new PortableRespawnCharacter();
    public TeleportLocation currentTeleportLocation;
    private boolean almostEnding;
    private float showEndingIn;

    // public final static String STARTING_LEVEL = "aaron";

    public void newGame(int speedMode) {
        almostEnding = false;
        String overriddenStart = getGame().platformAdapter.overrideStartingLevel();
        if (overriddenStart != null) {
            newGame(overriddenStart, speedMode);
        } else {
            newGame(STARTING_LEVEL, speedMode);
        }
    }

    public void newGame(String levelId, int speedMode) {
        gameProgress = new RoboNinjaProgress();
        gameProgress.setSpeedMode(speedMode);
        this.speedMode = speedMode;
        mc = new MainCharacter(this, getGame().platformAdapter.usesOldGraphics());
        Level level = LevelFactory.getLevel(levelId);
        setLevel(level, null, 1);


    }

    public RoboNinjaProgress getProgress() {
        return (RoboNinjaProgress) gameProgress;

    }

    @Override
    public void preloadAsync() {
        Maps.loadInBackground(getLevelToLoad());
        Maps.loadInBackground("temple-background");
        Maps.loadInBackground("blank-background");
        Maps.loadInBackground("cave-background");
    }

    protected String getLevelToLoad() {
        if (RoboNinjaProgress.hasProgress()) {

            // this is a bit messy -- creating a progress just to load and
            // discard,
            // when we'll immediately reload progress in a minute if they hit
            // continue.
            // probably should fix this at some point
            RoboNinjaProgress progress = new RoboNinjaProgress();
            progress.loadProgress();
            speedMode = progress.getSpeedMode();
            return progress.levelId;

        } else {
            return STARTING_LEVEL;
        }

    }

    @Override
    public void update(float delta) {
        super.update(delta);

        // update visibility of the portable respawn
        RoboNinjaProgress rnProgress = (RoboNinjaProgress) gameProgress;
        portableRespawn.visible = (rnProgress.isPortableRespawning);
        if (portableRespawn.visible) {
            portableRespawn.bounds.set(rnProgress.portableRespawnX, rnProgress.portableRespawnY, 16, 16);
        }

        updateMinimapTracker();

        if (currentMode == MODE_ACTION) {
            updateConsumablesRespawn(delta);
        }

        if (almostEnding) {
            showEndingIn -= delta;
            if (showEndingIn <= 0 && (mc.getPosition().x > 13.9 && mc.getPosition().x < 14.4 && mc.getFacing() == 1)) {
                almostEnding = false;

                Cutscene intro = new Cutscene("ending", new CutsceneFinishedCallback() {
                    @Override
                    public void cutsceneFinished() {
                        afterEndingCutscene();
                    }
                });
                CutsceneScreen csScreen = new CutsceneScreen(this, intro);
                game.setScreen(csScreen);
            }
        }


    }

    private void afterEndingCutscene() {
        game.setScreen(new EndingStatsScreen(this));

    }

    private void updateConsumablesRespawn(float delta) {
        RoboNinjaProgress progress = getProgress();
        for (ObjectIntMap.Entry<String> abilityEntry : progress.consumablesMax) {
            String consumableKey = abilityEntry.key;
            updateConsumableRespawn(consumableKey, delta);
        }
    }

    private void updateConsumableRespawn(String consumableKey, float delta) {
        RoboNinjaProgress progress = getProgress();

        int current = progress.consumables.get(consumableKey, 0);
        int max = progress.consumablesMax.get(consumableKey, 0);
        if (current == max) {
            progress.consumableRespawn.put(consumableKey, 0.0f);
        } else {
            float respawnTime = AbilityFactory.getConsumable(consumableKey).getRespawnTime();
            //ugh, not happy about this auto-boxing. might want to consider some sort
            //of different data structure to store a float primitive?
            Float soFar = progress.consumableRespawn.get(consumableKey);
            soFar += delta;

            if (soFar != null && soFar >= respawnTime) {
                progress.gainConsumable(consumableKey, 1);
                progress.consumableRespawn.put(consumableKey, 0.0f);
            } else {
                progress.consumableRespawn.put(consumableKey, soFar);
            }
        }

    }


    public void updateMinimapTracker() {
        Screen screen = game.getScreen();
        if (screen instanceof GameScreen) {
            Rectangle cameraRectangle = ((GameScreen) screen).getCameraRectangle();

            float positionX = level.minimapPosition.x + cameraRectangle.x;
            float positionY = level.minimapPosition.y + level.getHeight() - cameraRectangle.y - cameraRectangle.height;
            getProgress().minimapTracker.uncoverRegion(positionX, positionY, cameraRectangle.width,
                    cameraRectangle.height);
            getProgress().minimapTracker.setCurrentPosition(level.minimapPosition.x + mc.getPosition().x, level.minimapPosition.y + level.getHeight() - mc.getPosition().y);
        }


    }

    public void setLevelWithoutSave(String levelId, float x, float y, int facing) {
        Level levelLoaded = LevelFactory.getLevel(levelId);
        Vector2 position = new Vector2(x, y);
        setLevel(levelLoaded, position, facing);
    }

    public void setPortableCheckpointAtCurrent() {
        getProgress().setPortableRespawn(mc.getPosition().x, mc.getPosition().y, mc.getFacing());
    }

    public void setRespawnAtCurrent() {
        setRespawnPoint(level.mapName, mc.getPosition().x, mc.getPosition().y, mc.getFacing());
    }

    @Override
    public void setLevel(Level level, Vector2 position, int facing) {
        currentMode = MODE_READY;
        getGame().timedDebug("RoboNinjaGameState.setLevel");
        super.setLevel(level, position, facing);
        if (position == null) {
            position = level.startingPoint;
        }
        setRespawnPoint(level.mapName, position.x, position.y, facing);
        /*
         * ((RoboNinjaProgress) gameProgress).levelId = level.mapName;
		 * ((RoboNinjaProgress) gameProgress).respawnX = position.x;
		 * ((RoboNinjaProgress) gameProgress).respawnY = position.y;
		 * ((RoboNinjaProgress) gameProgress).respawnFacing = facing;
		 */


        characters.add(portableRespawn);
        getGame().timedDebug("done RoboNinjaGameState.setLevel");
    }

    // because the above version of setLevelWithoutSave also sets the respawn point, this
    // one only sets your position
    public void setPosition(Vector2 position, int facing) {
        super.setLevel(level, position, facing);
        currentMode = MODE_READY;
        characters.add(portableRespawn);
    }

    public RoboNinjaGameState(RoboNinjaGame roboNinjaGame) {
        gameProgress = new RoboNinjaProgress();
        this.game = roboNinjaGame;
    }

    public void setRespawnPoint(String level, float x, float y, int speed) {
        getProgress().setRespawnPoint(level, x, y, speed);
    }

    //
    // public void acquireAbility(McAbility ability) {
    // acquireAbility(ability, null);
    // }

    public void acquireAbility(McAbility ability, ItemCharacter itemCharacter) {

        if (speedMode == RoboNinjaGameState.SPEED_MODE_EASY && ability instanceof ConsumableAbility) {
            int numAcquired = getProgress().acquireCollectible(itemCharacter);
            hudType = Hud.HUD_SHOW_TYPE_ITEM;
            hudDetail = RoboNinjaProgress.COLLECTIBLES + "/" + numAcquired;

            //unlock achievement if necessary
            if (numAcquired == RoboNinjaProgress.MAX_COLLECTIBLES) {
                PlayServicesAdapter playServicesAdapter = getGame().platformAdapter.getPlayServicesAdapter();
                if (playServicesAdapter != null && playServicesAdapter.isLoggedIn()) {
                    playServicesAdapter.unlockAchievement(Achievement.COLLECTIBLES);
                }
            }

            currentMode = MODE_HUD;
            return;
        }

        getProgress().acquireAbility(ability, itemCharacter);

        //bitbucket bug #6 -- if you pick up the first wall/air ability, shouldn't immediately
        //go into the blank mode.
        int type = ability.getType();
        McAbility current = mc.abilities[type];
        if (current == null) {
            mc.toggleAbility(type);
            current = mc.abilities[type];
        }
        if (current instanceof BlankAirAbility || current instanceof BlankWallAbility) {
            mc.toggleAbility(type);
        }
        hudType = Hud.HUD_SHOW_TYPE_ITEM;
        hudDetail = ability.getId();
        currentMode = MODE_HUD;
    }


    public void resumeGame() {
        super.resumeGame();
        setLevelWithoutSave(getProgress().levelId, getProgress().respawnX, getProgress().respawnY, getProgress().respawnFacing);
        respawn();
    }

    public void useConsumable(String itemType) {
        ConsumableAbility consumable = AbilityFactory.getConsumable(itemType);
        MiscUtils.assertFalse(consumable == null);
        if (consumable != null) {
            if (consumable.canUse(mc)) {
                consumable.trigger(mc);
            }
        }
    }

    public void respawn() {
        RoboNinjaGame.timedDebug("start respawn");
        if (getProgress().isPortableRespawning) {
            setPosition(new Vector2(getProgress().portableRespawnX, getProgress().portableRespawnY),
                    getProgress().portableRespawnFacing);
        } else {
            RoboNinjaGame.timedDebug("before setLevel (in respawn)");
            setLevel(level, new Vector2(getProgress().respawnX, getProgress().respawnY), getProgress().respawnFacing);
            RoboNinjaGame.timedDebug("after setLevel (in respawn)");
        }
        for (GameCharacter character : characters) {
            character.onMcRespawn();
        }
        for (GameCharacter gameCharacter : toRespawn) {
            gameCharacter.respawn();
        }
        toRespawn.clear();
        RoboNinjaGame.timedDebug("done respawn");

    }

    public void setSlowMo(int slowmoTime) {
        slowMotion = slowmoTime;
    }

    public void enterTeleporter(final Teleporter teleporter, TeleportLocation teleportLocation) {

        getProgress().discoverTeleporter(teleportLocation);
        if (getProgress().getTeleportOptions().size() > 1) {
            currentTeleportLocation = teleportLocation;
            currentMode = GameState.MODE_TELEPORTING;
        }

    }

    public void teleportTo(TeleportLocation teleportLocation) {
        //see note in TeleporterLocation about actualY
        setLevelWithoutSave(teleportLocation.levelId, teleportLocation.minimapX, teleportLocation.actualY, 1);
        Teleporter teleporter = level.getTeleporterIfExists();
        teleporter.setGameState(this);
        teleporter.teleportTo();
        setRespawnAtCurrent();

    }

    public void acquireAllAbilities() {
        McAbility[] allAbilities = AbilityFactory.getAllAbilities();
        for (McAbility ability : allAbilities) {
            acquireAbility(ability, null);
        }
    }

    public RoboNinjaGame getGame() {
        return (RoboNinjaGame) game;
    }

    public void showEndingIn(float i) {
        almostEnding = true;
        showEndingIn = i;
    }

}
