package net.tolberts.android.roboninja.minimap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.GdxRuntimeException;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class MinimapTracker {

    public static final int SCALE = 2;
    public Vector2 currentPosition = new Vector2(0, 0);
    Pixmap sourceMinimap;
    Pixmap currentMap;
    Rectangle lastRectangle = new Rectangle(-10000f, -10000f, 0, 0);

    public MinimapTracker() {
        sourceMinimap = Art.getPixmap("minimap");
        currentMap = new Pixmap(sourceMinimap.getWidth(), sourceMinimap.getHeight(), Format.RGBA8888);

    }

    public Texture getAsTexture() {
        return new Texture(currentMap);
    }

    public String serializeToString() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Gdx.files.local("minimap").moveTo(Gdx.files.local("minimap-bkp"));
                } catch (Exception e) {
                    //not worried about it.
                }
                try {
                    FileHandle out = Gdx.files.local("minimap");
                    PixmapIO.writeCIM(out, currentMap);
                    Gdx.files.local("minimap-bkp").delete();
                } catch (Exception e) {
                    Gdx.app.error(RoboNinjaGame.TAG, "Error saving minimap", e);

                }
            }
        }).run();

        return "x";
    }

    public void initializeFromString(String encoded) {
        RoboNinjaGame.timedDebug("start loadMinimap");


        FileHandle out = Gdx.files.local("minimap-bkp");
        if (!out.exists()) {
            out = Gdx.files.local("minimap");
        }
        if (out.exists()) {
            try {
                currentMap = PixmapIO.readCIM(out);
            } catch (GdxRuntimeException e) {
                Gdx.app.error(RoboNinjaGame.TAG, "Could not reload saved minimap");
                Gdx.app.error(RoboNinjaGame.TAG, e.getMessage());
            }
        }
        RoboNinjaGame.timedDebug("done loadMinimap");
    }


    public void uncoverRegion(float x, float y, float width, float height) {

        if (y == lastRectangle.y && height == lastRectangle.height) {
            float diff = x - lastRectangle.x;
            float newX;

            if (diff == 0) {
                return;
            }

            if (diff > 0) {
                newX = lastRectangle.x + lastRectangle.width;
            } else {
                newX = lastRectangle.x - diff;
            }
            if (diff < 1) {
                diff = 1;
            }
            currentMap.drawPixmap(sourceMinimap, Math.round(newX * SCALE), Math.round(y * SCALE),
                    Math.round(newX * SCALE), Math.round(y * SCALE), Math.round(diff * SCALE),
                    Math.round(height * SCALE));

        } else {

            currentMap.drawPixmap(sourceMinimap, Math.round(x * SCALE), Math.round(y * SCALE), Math.round(x * SCALE),
                    Math.round(y * SCALE), Math.round(width * SCALE), Math.round(height * SCALE));
        }
        lastRectangle.set(x, y, width, height);

    }

    public void setCurrentPosition(float x, float y) {
        currentPosition.set(x, y);
    }

}
