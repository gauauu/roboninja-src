package net.tolberts.android.roboninja.mc.abilities.ground;


import net.tolberts.android.roboninja.mc.MainCharacter;

public class DashAbility extends GroundAbility {

    public final static String ID = "dash";
    public final static float DASH_TIME = 0.5f;

    private float stateTimer;


    @Override
    public String getName() {
        return "Dash";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {
        if (!mc.isDashing()) {
            return;
        }

        stateTimer -= tick;

        if (stateTimer > 0) {
            return;
        }

        mc.setDashing(false);

    }

    @Override
    public String getAcquiredMessage() {
        return "Robo-Ninja has installed the dash servos.\n " +
                "Tap to sprint quickly while running.\n";

    }


    @Override
    protected void triggerGroundAbility(MainCharacter mc) {
        mc.setDashing(true);

        stateTimer = DASH_TIME;
    }

    @Override
    public void cancelOnLevelChange(MainCharacter mc) {
        mc.setDashing(false);
        stateTimer = 0;

    }
}
