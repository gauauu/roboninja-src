package net.tolberts.android.roboninja.mc.abilities;

import net.tolberts.android.roboninja.mc.MainCharacter;

public interface McAbility {
	
	public final static int TYPE_GROUND = 0;
	public final static int TYPE_JUMP = 1;
	public final static int TYPE_WALL = 2;
    public final static int TYPE_CONSUMABLE = 3;
	
	public String getName();

	public String getId();
	
	public int getType();
	
	public boolean canUse(MainCharacter mc);
	
	public void trigger(MainCharacter mc);
	
	public void update(MainCharacter mc, float tick, boolean isSelected);
	public String getAcquiredMessage();

    public void cancelOnLevelChange(MainCharacter mainCharacter);

    public void notTouched(MainCharacter mainCharacter);


}
