package net.tolberts.android.roboninja.mc.abilities.air;

import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.ground.JumpAbility;

public class JetPackAbility extends AirAbility {

    public final static String ID = "jetpack";

    boolean isActive = false;

    @Override
    protected void triggerAirAbility(MainCharacter mc) {
//        if (!isActive) {
        isActive = true;
        mc.velocity.y = Math.max(mc.velocity.y, 8);
        mc.doAnimation(ID);
//        }
    }

    @Override
    public String getName() {
        return "Rocket pack";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void updateAirAbility(MainCharacter mc, float tick, boolean isSelected) {
        if (!isActive && isSelected) {
            if (mc.state == MainCharacter.STATE_JUMPING) {
                mc.doAnimation(JumpAbility.ID);
            } else if (mc.state == MainCharacter.STATE_RUNNING) {
                //mc.doAnimation(null);
            }
        }
        if (isActive && mc.state == MainCharacter.STATE_RUNNING) {
            isActive = false;
            mc.doAnimation(null);
        }
        isActive = false;
    }

    @Override
    public String getAcquiredMessage() {
        return "\n\nRobo-Ninja has installed\nrocket jets.\nTap and hold in the air\nto fly.";
    }
}
