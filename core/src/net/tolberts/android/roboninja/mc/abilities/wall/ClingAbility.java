package net.tolberts.android.roboninja.mc.abilities.wall;

import net.tolberts.android.game.Input;
import net.tolberts.android.roboninja.mc.MainCharacter;

public class ClingAbility extends WallAbility {

    public final static String ID = "cling";


    @Override
    public String getName() {
        return "Cling";
    }

    @Override
    public String getId() {
        return ID;
    }


    @Override
    public String getAcquiredMessage() {
        return "You have acquired Suction Grips\nYou can cling to walls in the air\nby tapping and holding.\n";
    }

    @Override
    public boolean internalOnWallCollision(float oldX, float oldY, MainCharacter mc) {
        mc.bounds.y = oldY;
        mc.bounds.x = oldX;
        mc.velocity.y = mc.oldYVelocity;
        return false;
    }
}
