package net.tolberts.android.roboninja.mc.abilities.air;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class BlankAirAbility extends AirAbility{

    public final static String ID = "airBlank";


    @Override
    protected void triggerAirAbility(MainCharacter mc) {

    }

    @Override
    protected void updateAirAbility(MainCharacter mc, float tick, boolean isSelected) {

    }

    @Override
    public String getName() {
        return ID;
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getAcquiredMessage() {
        return "";
    }
}
