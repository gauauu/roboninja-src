package net.tolberts.android.roboninja.mc.abilities.ground;

import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.McAbility;

public abstract class GroundAbility implements McAbility {

    @Override
    public int getType() {
        return McAbility.TYPE_GROUND;
    }

    @Override
    public boolean canUse(MainCharacter mc) {
        return mc.state == MainCharacter.STATE_RUNNING;
    }

    @Override
    public void trigger(MainCharacter mc) {
        if (!canUse(mc)) {
            return;
        }
        triggerGroundAbility(mc);

    }

    public void cancelOnLevelChange(MainCharacter mainCharacter) {

    }


    protected abstract void triggerGroundAbility(MainCharacter mc);

    @Override
    public void notTouched(MainCharacter mainCharacter) {

    }
}
