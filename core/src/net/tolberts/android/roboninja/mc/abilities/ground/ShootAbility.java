package net.tolberts.android.roboninja.mc.abilities.ground;


import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.characters.PlayerLaser;
import net.tolberts.android.roboninja.mc.MainCharacter;

public class ShootAbility extends GroundAbility {

    public final static String ID = "shoot";

    private float stateTimer = -1;


    @Override
    public String getName() {
        return "Shoot";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public boolean canUse(MainCharacter mc) {
        boolean canUse = super.canUse(mc);
        if (!canUse) {
            return false;
        }

        return stateTimer <= 0;
    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {

        if (stateTimer > 0) {
            stateTimer -= tick;
        }
        if (stateTimer <= 0.5f) {
            if (ID.equals(mc.getCurrentAnimation())) {
                mc.doAnimation(null);
            }
        }


    }

    @Override
    public String getAcquiredMessage() {
        return "Robo-Ninja has installed the laser cannon.\n " +
                "You can fire lasers by tapping\nwhile on the ground.\n";

    }


    @Override
    protected void triggerGroundAbility(MainCharacter mc) {
        PlayerLaser activeLaser = new PlayerLaser();
        if (mc.getFacing() > 0) {
            activeLaser.bounds.x = mc.bounds.x + 0.7f;
        } else {
            activeLaser.bounds.x = mc.bounds.x - 1.5f;
        }
        if (mc.useOldGraphics) {
            activeLaser.bounds.y = mc.bounds.y + 1.25f;
        } else {
            activeLaser.bounds.y = mc.bounds.y + 0.8f;
        }
        activeLaser.setSpeed(mc.getFacing() * 20.0f);
        mc.gameState.addCharacter(activeLaser);
        AudioPlayer.playSound(Audio.PLAYER_LASER);
        stateTimer = 1.0f;
        mc.setCurrentAnimation(ID);


    }

    @Override
    public void cancelOnLevelChange(MainCharacter mc) {


    }
}
