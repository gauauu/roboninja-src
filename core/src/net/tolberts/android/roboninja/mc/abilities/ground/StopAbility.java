package net.tolberts.android.roboninja.mc.abilities.ground;


import net.tolberts.android.game.characters.AnimatedCharacter;
import net.tolberts.android.roboninja.mc.MainCharacter;

public class StopAbility extends GroundAbility {

    public final static String ID = "stop";
    private boolean isTriggered;

    @Override
    public String getName() {
        return "Stop";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {


        if (isTriggered) {
            isTriggered = false;
            mc.freezeHorizontal = false;
            mc.animationPaused = false;
        }



    }

    @Override
    public String getAcquiredMessage() {
        return "Robo-Ninja has inertial dampers.\n " +
                "Tap and hold to stop.\n";

    }


    @Override
    protected void triggerGroundAbility(MainCharacter mc) {
        mc.freezeHorizontal = true;
        mc.animationPaused = true;
        isTriggered = true;



    }


}
