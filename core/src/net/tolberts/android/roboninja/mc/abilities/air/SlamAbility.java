package net.tolberts.android.roboninja.mc.abilities.air;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class SlamAbility extends AirAbility{

    public final static String ID = "slam";

    boolean isActive = false;

    @Override
    protected void triggerAirAbility(MainCharacter mc) {
        if (!isActive) {
            isActive = true;
            mc.doAnimation(ID);
            mc.freezeHorizontal = true;
        }
    }

    @Override
    public String getName() {
        return "Slam";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void updateAirAbility(MainCharacter mc, float tick, boolean isSelected) {
        if (isActive && mc.state == MainCharacter.STATE_RUNNING) {
            isActive = false;
            mc.freezeHorizontal = false;
            mc.doAnimation(null);
        }
    }

    @Override
    public String getAcquiredMessage() {
        return "\n\nYou found the\nslam-smasher module.\nTap in the air\nto slam to the ground.";
    }
}
