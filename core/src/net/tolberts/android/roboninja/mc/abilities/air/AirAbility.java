package net.tolberts.android.roboninja.mc.abilities.air;

import net.tolberts.android.game.Input;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.McAbility;

public abstract class AirAbility implements McAbility {

    boolean hasBeenReleased = false;

    @Override
    public int getType() {
        return McAbility.TYPE_JUMP;
    }

    @Override
    public boolean canUse(MainCharacter mc) {
        if (Input.wasSpacebarOrScreenTouch()) {
            if (hasBeenReleased) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void cancelOnLevelChange(MainCharacter mainCharacter) {

    }

    @Override
    public void trigger(MainCharacter mc) {
        if (!canUse(mc)) {
            return;
        }
        triggerAirAbility(mc);

    }

    protected abstract void triggerAirAbility(MainCharacter mc);

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {
        if (mc.state == MainCharacter.STATE_RUNNING) {
            hasBeenReleased = false;
        }
        updateAirAbility(mc, tick, isSelected);

    }

    @Override
    public void notTouched(MainCharacter mainCharacter) {
        if (mainCharacter.state == MainCharacter.STATE_JUMPING) {
            hasBeenReleased = true;
        }
    }

    protected abstract void updateAirAbility(MainCharacter mc, float tick, boolean isSelected);

}
