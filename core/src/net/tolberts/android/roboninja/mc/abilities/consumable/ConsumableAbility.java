package net.tolberts.android.roboninja.mc.abilities.consumable;

import net.tolberts.android.game.GameState;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.McAbility;

public abstract class ConsumableAbility implements McAbility {


    public final static float DEFAULT_ITEM_RESPAWN = 300.0f; //5 minutes

    @Override
    public int getType() {
        return McAbility.TYPE_CONSUMABLE;
    }

    @Override
    public boolean canUse(MainCharacter mc) {
        return (mc.gameState.speedMode == GameState.SPEED_MODE_EASY || mc.hasConsumable(getId()));
    }

    @Override
    public void trigger(MainCharacter mc) {
        mc.consume(getId());
        triggerConsumable(mc);
    }

    public static String[] getAllConsumableAbilities() {
        return new String[]{
          PortableRespawn.ID,
                Slowmo.ID
        };
    }

    public void cancelOnLevelChange(MainCharacter mainCharacter) {

    }

    protected abstract void triggerConsumable(MainCharacter mc);


    public int getNumberGained() {
        return 1;
    }

    @Override
    public void notTouched(MainCharacter mainCharacter) {

    }

    public float getRespawnTime() {
        return DEFAULT_ITEM_RESPAWN;
    }
}
