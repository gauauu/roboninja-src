package net.tolberts.android.roboninja.mc.abilities.wall;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class BlankWallAbility extends WallAbility {

    public final static String ID = "wallBlank";

    @Override
    public String getName() {
        return "Wall Blank";
    }

    @Override
    public String getId() {
        return ID;
    }


    @Override
    public String getAcquiredMessage() {
        return "";
    }

    @Override
    public boolean internalOnWallCollision(float oldX, float oldY, MainCharacter mc) {
        return true;
    }
}
