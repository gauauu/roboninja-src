package net.tolberts.android.roboninja.mc.abilities.wall;

import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.ground.JumpAbility;

public class WallJumpAbility extends WallAbility {

    public final static String ID = "walljump";


    @Override
    public String getName() {
        return "Wall Jump";
    }

    @Override
    public String getId() {
        return ID;
    }


    @Override
    public String getAcquiredMessage() {
        return "You found Surface-triggered Servos\nYou can jump off walls\nby tapping and holding.\n";
    }

    @Override
    public boolean internalOnWallCollision(float oldX, float oldY, MainCharacter mc) {
        mc.velocity.y = JumpAbility.JUMP_VELOCITY;
        mc.acceleration.y = -65.0f;
        mc.state = MainCharacter.STATE_JUMPING;
        return true;
    }
}
