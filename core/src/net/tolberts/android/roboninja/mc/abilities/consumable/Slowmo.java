package net.tolberts.android.roboninja.mc.abilities.consumable;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class Slowmo extends ConsumableAbility {


    public static final String ID = "slowmo";
    public static final int SLOWMO_TIME = 5;

    @Override
    protected void triggerConsumable(MainCharacter mc) {
        mc.gameState.setSlowMo(SLOWMO_TIME);
    }

    @Override
    public String getName() {
        return "Slow-Mo";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {
        //no need to do anything on updates
    }

    @Override
    public String getAcquiredMessage() {
        return "\n\nYou have picked up a time distorter.\nTap the icon to temporarily slow time\n" +
                "\n\nConsumable items slowly regenerate\nafter you use them!";
    }
}
