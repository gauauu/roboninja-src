package net.tolberts.android.roboninja.mc.abilities.consumable;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class PortableRespawn extends ConsumableAbility {


    public static final String ID = "respawn";

    @Override
    protected void triggerConsumable(MainCharacter mc) {
        mc.gameState.setPortableCheckpointAtCurrent();
    }



    @Override
    public String getName() {
        return "Checkpoint";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {
        //no need to do anything on updates
    }

    @Override
    public String getAcquiredMessage() {
        return "\n\nYou have picked up a portable checkpoint.\nYou may tap the icon to deploy a checkpoint\n" +
                "anywhere you wish.\n\nConsumable items slowly regenerate\nafter you use them.";
    }
}
