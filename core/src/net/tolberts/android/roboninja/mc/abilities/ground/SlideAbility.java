package net.tolberts.android.roboninja.mc.abilities.ground;


import net.tolberts.android.roboninja.mc.MainCharacter;

public class SlideAbility extends GroundAbility {

    public final static String ID = "slide";
    public final static float SLIDE_TIME = 0.5f;

    private float stateTimer;
    private double dontAllowTimer;


    @Override
    public String getName() {
        return "Slide";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {

        if (dontAllowTimer > 0) {
            dontAllowTimer -= tick;
        }


        if (!mc.isSliding()) {
            return;
        }

        stateTimer -= tick;

        if (stateTimer > 0) {
            return;
        }

        if (!mc.canFinishSliding()) {
            return;
        }

        mc.setSliding(false);
        mc.doAnimation(null);

    }

    @Override
    public String getAcquiredMessage() {
        return "Robo-Ninja has installed the slide jets.\n ";

    }


    @Override
    protected void triggerGroundAbility(MainCharacter mc) {
        if (dontAllowTimer > 0) {
            return;
        }
        mc.setSliding(true);

        stateTimer = SLIDE_TIME;
    }

    @Override
    public void cancelOnLevelChange(MainCharacter mc) {
        mc.setSliding(false);
        stateTimer = 0;
        dontAllowTimer = 0.25;

    }
}
