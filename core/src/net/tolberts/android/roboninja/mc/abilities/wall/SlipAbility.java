package net.tolberts.android.roboninja.mc.abilities.wall;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class SlipAbility extends WallAbility {

    public final static String ID = "slip";

    public final static float SLIP_RATE = 3.50f;


    @Override
    public String getName() {
        return "Slip";
    }

    @Override
    public String getId() {
        return ID;
    }

    protected String getAnimationName() {
        return SlipAbility.ID;
    }

    @Override
    public String getAcquiredMessage() {
        return "You have acquired Slip Claws\nYou can slowly slide down walls\nby tapping and holding.\n";
    }

    @Override
    public boolean internalOnWallCollision(float oldX, float oldY, MainCharacter mc) {
        mc.bounds.x = oldX;
        mc.velocity.y = -SLIP_RATE;
        mc.isSlipping = true;
        return false;
    }
}
