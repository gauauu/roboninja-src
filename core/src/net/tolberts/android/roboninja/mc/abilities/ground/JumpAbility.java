package net.tolberts.android.roboninja.mc.abilities.ground;

import net.tolberts.android.roboninja.mc.MainCharacter;

public class JumpAbility extends GroundAbility {

    public final static String ID = "jump";

    public final static float JUMP_VELOCITY = 20;

    @Override
    public String getName() {
        return "Jump";
    }

    public void triggerGroundAbility(MainCharacter mc) {
        mc.velocity.y = JUMP_VELOCITY;
        mc.acceleration.y = -65.0f;
        mc.state = MainCharacter.STATE_JUMPING;
        mc.hasBeenJumping = 0;
    }

    @Override
    public void update(MainCharacter mc, float delta, boolean isSelected) {

    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public String getAcquiredMessage() {
        return "Robo-Ninja has installed the Jump Servos.\nYou may now jump by tapping the screen.";
    }

}
