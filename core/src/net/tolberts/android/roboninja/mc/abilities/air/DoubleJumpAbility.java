package net.tolberts.android.roboninja.mc.abilities.air;

import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.ground.JumpAbility;

public class DoubleJumpAbility extends AirAbility{

    public final static String ID = "doublejump";

    boolean isActive = false;

    @Override
    protected void triggerAirAbility(MainCharacter mc) {
        if (!isActive) {
            isActive = true;
            mc.velocity.y = Math.max(mc.velocity.y, JumpAbility.JUMP_VELOCITY);
            mc.doAnimation(ID);
        }
    }

    @Override
    public String getName() {
        return "Double-Jump";
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void updateAirAbility(MainCharacter mc, float tick, boolean isSelected) {
        if (isActive && mc.state == MainCharacter.STATE_RUNNING) {
            isActive = false;
            mc.doAnimation(null);
        }
    }

    @Override
    public String getAcquiredMessage() {
        return "\n\nRobo-Ninja has installed\njump jets.\nTap in the air\nto double-jump.";
    }
}
