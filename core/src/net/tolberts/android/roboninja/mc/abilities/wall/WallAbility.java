package net.tolberts.android.roboninja.mc.abilities.wall;

import net.tolberts.android.game.Input;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.McAbility;

public abstract class WallAbility implements McAbility {

    private boolean isPotentiallyActive = false;
    private boolean isActuallyActive = false;

    protected float frameDelta = 0;

    @Override
    public int getType() {
        return McAbility.TYPE_WALL;
    }


    @Override
    public boolean canUse(MainCharacter mc) {
        //actually we can "use" this during a jump, so it's ready once we hit the wall
        return (mc.state == MainCharacter.STATE_JUMPING);
    }

    @Override
    public void trigger(MainCharacter mc) {
        if (!canUse(mc)) {
            return;
        }
        triggerWallAbility(mc);

    }

    @Override
    public void update(MainCharacter mc, float tick, boolean isSelected) {
        frameDelta = tick;
        if (isPotentiallyActive) {
            if (!Input.wasSpacebarOrScreenTouch()) {
                isPotentiallyActive = false;
                if (isActuallyActive) {
                    mc.doAnimation(MainCharacter.ANIM_WALK);
                }
                isActuallyActive = false;
            }
        }
    }


    protected void triggerWallAbility(MainCharacter mc) {
        isPotentiallyActive = true;
    }

    public boolean onWallCollision(float oldX, float oldY, MainCharacter mc) {
        if (isPotentiallyActive) {
            isActuallyActive = true;
            mc.doAnimation(getAnimationName());
            return internalOnWallCollision(oldX, oldY, mc);
        }
        return true;
    }

    protected String getAnimationName() {
        return getId();
    }

    /**
     * What should happen when we DO hit a wall while this is active
     * @return whether the normal bounce should occur
     */
    protected abstract boolean internalOnWallCollision(float oldX, float oldY, MainCharacter mc);

    public void cancelOnLevelChange(MainCharacter mainCharacter) {

    }

    @Override
    public void notTouched(MainCharacter mainCharacter) {

    }
}
