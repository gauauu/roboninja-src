package net.tolberts.android.roboninja.mc;

import com.badlogic.gdx.Gdx;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.mc.abilities.air.BlankAirAbility;
import net.tolberts.android.roboninja.mc.abilities.air.DoubleJumpAbility;
import net.tolberts.android.roboninja.mc.abilities.air.JetPackAbility;
import net.tolberts.android.roboninja.mc.abilities.air.SlamAbility;
import net.tolberts.android.roboninja.mc.abilities.consumable.ConsumableAbility;
import net.tolberts.android.roboninja.mc.abilities.consumable.PortableRespawn;
import net.tolberts.android.roboninja.mc.abilities.consumable.Slowmo;
import net.tolberts.android.roboninja.mc.abilities.ground.*;
import net.tolberts.android.roboninja.mc.abilities.wall.*;

import java.util.HashMap;
import java.util.Map;

public class AbilityFactory {

    private static Map<String,McAbility> cache = new HashMap<String,McAbility>();

	public static McAbility getAbility(String itemId) {
        if (cache.containsKey(itemId)) {
            return cache.get(itemId);
        }
        McAbility created = null;
		if (JumpAbility.ID.equals(itemId)) {
            created = new JumpAbility();
		} else if (SlideAbility.ID.equals(itemId)) {
            created = new SlideAbility();
        } else if (PortableRespawn.ID.equals(itemId)) {
            created = new PortableRespawn();
        } else if (Slowmo.ID.equals(itemId)) {
            created = new Slowmo();
        } else if (ClingAbility.ID.equals(itemId)) {
            created = new ClingAbility();
        } else if (BlankWallAbility.ID.equals(itemId)) {
            created = new BlankWallAbility();
        } else if (SlamAbility.ID.equals(itemId)) {
            created = new SlamAbility();
        } else if (BlankAirAbility.ID.equals(itemId)) {
            created = new BlankAirAbility();
        } else if (DoubleJumpAbility.ID.equals(itemId)) {
            created = new DoubleJumpAbility();
        } else if (SlipAbility.ID.equals(itemId)) {
            created = new SlipAbility();
        } else if (ShootAbility.ID.equals(itemId)) {
            created = new ShootAbility();
        } else if (WallJumpAbility.ID.equals(itemId)) {
            created = new WallJumpAbility();
        } else if (JetPackAbility.ID.equals(itemId)) {
            created = new JetPackAbility();
        } else if (DashAbility.ID.equals(itemId)) {
            created = new DashAbility();
        } else if (StopAbility.ID.equals(itemId)) {
            created = new StopAbility();
        }
        if (created == null) {
            Gdx.app.error(RoboNinjaGame.TAG, "AbilityFactory missing constructor for " + itemId);
            throw new NullPointerException();
        }
		cache.put(itemId, created);
        return created;
	}

    public static ConsumableAbility getConsumable(String itemType) {
        if (PortableRespawn.ID.equals(itemType)) {
            return new PortableRespawn();
        } else if (Slowmo.ID.equals(itemType)) {
            return new Slowmo();
        }
        return null;
    }

    public static McAbility[] getAllAbilities() {
        return new McAbility[]{
                new JumpAbility(),
                new SlideAbility(),
                new PortableRespawn(),
                new Slowmo(),
                new ClingAbility(),
                //new BlankWallAbility(),
                new SlamAbility(),
                //new BlankAirAbility(),
                new DoubleJumpAbility(),
                new SlipAbility(),
                new ShootAbility(),
                new WallJumpAbility(),
                new JetPackAbility(),
                new StopAbility(),
                new DashAbility(),
        };
    }
}
