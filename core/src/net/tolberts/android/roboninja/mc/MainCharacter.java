package net.tolberts.android.roboninja.mc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.characters.AnimatedCharacter;
import net.tolberts.android.game.characters.Explosion;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.levels.Level;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.characters.WallCollisionCharacter;
import net.tolberts.android.roboninja.mc.abilities.McAbility;
import net.tolberts.android.roboninja.mc.abilities.air.DoubleJumpAbility;
import net.tolberts.android.roboninja.mc.abilities.air.JetPackAbility;
import net.tolberts.android.roboninja.mc.abilities.air.SlamAbility;
import net.tolberts.android.roboninja.mc.abilities.ground.ShootAbility;
import net.tolberts.android.roboninja.mc.abilities.ground.SlideAbility;
import net.tolberts.android.roboninja.mc.abilities.wall.ClingAbility;
import net.tolberts.android.roboninja.mc.abilities.wall.SlipAbility;
import net.tolberts.android.roboninja.mc.abilities.wall.WallAbility;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class MainCharacter extends AnimatedCharacter {

    public final static int STATE_RUNNING = 0;
    public final static int STATE_JUMPING = 1;
    public final static int STATE_WALL = 2;

    public final static int NUM_ABILITY_TYPES = 4;

    public final static int SPEED_DEFAULT = 10;

    public final static float BOUNDS_NORMAL_HEIGHT = 1.5f;
    public final static float BOUNDS_NORMAL_WIDTH = 0.5f;

    public final static float BOUNDS_SLIDING_HEIGHT = 0.75f;
    public final static float BOUNDS_SLIDING_WIDTH = 1.5f;

    public final static String ANIM_WALK = "walk";
    public final static String ANIM_JUMP = "jump";
    public final static String ANIM_FALL = "fall";
    public final static String ANIM_LANDING = "landing";

    public RoboNinjaGameState gameState;

    public int state;
    private int speed;
    private Explosion deathExplosion;
    private boolean isSliding = false;
    private boolean dieNextUpdate = false;


    //this marks the currently selected abilities, not the total inventory of abilities
    public McAbility[] abilities = new McAbility[NUM_ABILITY_TYPES];
    public float oldYVelocity;

    //some state for tracking if we're glued to a moving platform
    public WallCollisionCharacter glueToPlatform;
    public boolean glueToPlatformVerticalOnly;
    public float glueToPlatformFrames = 0;
    public boolean freezeHorizontal = false;

    public float secondsSinceRespawn = 0;
    private boolean isDashing;

    public boolean useOldGraphics = false;
    private float timeLanding;
    public float hasBeenJumping = 0;
    public boolean isSlipping = false;


    private MainCharacter(boolean oldGraphics) {
        this.useOldGraphics = oldGraphics;
        gameSettings = GameSettings.getInstance();
        visible = true;
        bounds.y = 4;
        bounds.height = 1.5f;
        bounds.width = 0.5f;
        velocity.y = 0;
        //acceleration.y = -29.5f;
        acceleration.y = GameSettings.getInstance().gravity;
        if (oldGraphics) {
            textureScale.x = 0.36f;
            textureScale.y = 0.36f;
            textureOffset.x = -0.80f;
        } else {
            textureScale.x = 0.26f;
            textureScale.y = 0.26f;
            textureOffset.x = -0.80f;
            textureOffset.y = -0.3f;
        }
        speed = SPEED_DEFAULT;
        state = STATE_RUNNING;
        initGraphics();
        setCurrentAnimation(ANIM_WALK);
    }

    public MainCharacter(GameState gameState, boolean useOldGraphics) {
        this(useOldGraphics);
        this.gameState = (RoboNinjaGameState) gameState;
    }

    public void toggleAbility(int type) {

        McAbility currentAbility = abilities[type];

        RoboNinjaProgress rnProgress = (RoboNinjaProgress) gameState.gameProgress;
        LinkedList<McAbility> abilityList = rnProgress.getAbilitiesOfType(type);

        if (abilityList.size() == 0) {
            return;
        }

        //if nothing is currently selected (probably should never happen)
        //, select the first.
        if (currentAbility == null) {
            setAbility(type, abilityList.get(0));
            return;
        }
        currentAbility = abilities[type];

        assert currentAbility != null;
        String id = currentAbility.getId();

        Iterator<McAbility> iterator = abilityList.iterator();
        while (iterator.hasNext()) {
            McAbility abil = iterator.next();
            //if/when we find a match, try to get the next one.
            if (id.equals(abil.getId())) {
                if (iterator.hasNext()) {
                    setAbility(type, iterator.next());
                    return;
                } else {
                    //if it doesn't exist, use the first.
                    setAbility(type, abilityList.get(0));
                    return;
                }
            }
        }

        //if we never found one, use the first.
        setAbility(type, abilityList.get(0));
    }

    public void setAbility(int type, McAbility ability) {
        MiscUtils.assertFalse((type < 0) || (type > NUM_ABILITY_TYPES));
        abilities[type] = ability;
    }

    @Override
    public void updateCharacter(float delta) {
        secondsSinceRespawn += delta;


        //toggle between walking and jumping animation,
        //but don't switch if we're in a weird item-based animation state

        String currAnim = getCurrentAnimation();
        if (STATE_JUMPING == state) {
            hasBeenJumping += delta;
            if (currAnim.equals(ANIM_WALK) || currAnim.equals(ANIM_LANDING) || (currAnim.equals(ANIM_FALL) && velocity.y >= 0)) {
                setCurrentAnimation(ANIM_JUMP);
            } else if (currAnim.equals(ANIM_JUMP) && velocity.y <= 0) {
                doAnimation(ANIM_FALL);
            }
        } else {
            if (ANIM_JUMP.equals(currAnim) || ANIM_FALL.equals(currAnim)) {
                if (hasBeenJumping > 0.5f) {
                    setCurrentAnimation(ANIM_LANDING);
                    timeLanding = 0.40f;
                } else {
                    setCurrentAnimation(ANIM_WALK);
                }
            } else if (currAnim.equals(ANIM_LANDING)) {
                timeLanding -= delta;
                if (timeLanding <= 0) {
                    setCurrentAnimation(ANIM_WALK);
                }
            }
            hasBeenJumping = 0.0f;
        }
        if (SlipAbility.ID.equals(currAnim) && !isSlipping) {
            setCurrentAnimation(ANIM_WALK);
        }

        // if we're in dying mode, don't do anything other than maybe respawn
        if (gameState.currentMode == GameState.MODE_DYING) {
            if (deathExplosion.isFinished()) {
                mcRespawn();
            }
            return;
        }

        if (dieNextUpdate) {
            dieNextUpdate = false;
            doDeath();
        }
        dieNextUpdate = false;


        float maxTick = 0.01f;
        float tmpDelta = delta;
        while (tmpDelta > maxTick) {
            moveAndCheckCollisions(maxTick);
            tmpDelta -= maxTick;
        }
        moveAndCheckCollisions(tmpDelta);
        hFlip = (speed < 0);
        updateAbilities(delta);

        handleInput();

    }

    @Override
    protected boolean updatesDuringMode(int gameMode) {
        if (gameMode == GameState.MODE_DYING) {
            return true;
        }
        return super.updatesDuringMode(gameMode);
    }

    public void die() {
        dieNextUpdate = true;
        velocity.y = 0;
    }

    public void doDeath() {
        visible = false;
        deathExplosion = Explosion.McExplosion(bounds.x, bounds.y);
        gameState.getProgress().flagDeath();
        gameState.addCharacter(deathExplosion);
        gameState.currentMode = GameState.MODE_DYING;
    }

    protected void mcRespawn() {


        visible = true;
        gameState.respawn();
        secondsSinceRespawn = 0;
        cancelAbilitiesOnLevelChange();
        velocity.y = 0;
        glueToPlatform = null;

        dieNextUpdate = false;

    }

    public void setFacing(int facing) {
        speed = facing * SPEED_DEFAULT;
        hFlip = (speed < 0);
    }

    public int getFacing() {
        if (speed > 0) {
            return 1;
        } else {
            return -1;
        }
    }

    public void doAnimation(String name) {
        if (isSliding && (name == null || ANIM_WALK.equals(name))) {
            Gdx.app.error(RoboNinjaGame.TAG, "slide error");
        }
        if (name == null) {
            setCurrentAnimation(ANIM_WALK);
            return;
        }
        setCurrentAnimation(name);
    }

    public boolean canFinishSliding() {
        return (gameState.level.isCollision(bounds.x + (bounds.width / 4) - 0.1f, bounds.y + bounds.height + 0.5f) == Level.COLLISION_NONE) &&
                (gameState.level.isCollision(bounds.x + (bounds.width / 4 * 3) + 0.1f, bounds.y + bounds.height + 0.5f) == Level.COLLISION_NONE);

    }

    public boolean isDashing() {
        return isDashing;
    }

    public void setDashing(boolean isDashing) {
        this.isDashing = isDashing;
    }

    public boolean isSliding() {
        return isSliding;
    }


    public void setSliding(boolean isSliding) {
        //avoid weirdness where it keeps trying to nudge the character if we hold the button
        if (this.isSliding && isSliding) {
            return;
        }

        this.isSliding = isSliding;
        if (isSliding) {
            doAnimation(SlideAbility.ID);
            bounds.height = BOUNDS_SLIDING_HEIGHT;
            /*
            bounds.width = BOUNDS_SLIDING_WIDTH;
            boolean collideOnLeft = (gameState.level.isCollision(bounds.x, bounds.y + 0.25f) == Level.COLLISION_WALL);
            boolean collideOnRight = (gameState.level.isCollision(bounds.x + bounds.width, bounds.y + 0.25f) == Level.COLLISION_WALL);
            //if there's not room for the sliding to occur, don't do it.
            if (collideOnLeft && collideOnRight) {
                setSliding(false);
                return;
            }

            //if we collide as soon as we start sliding, nudge one way or the other to prevent weird collisions
            if (collideOnRight) {
                bounds.x = bounds.x - (bounds.width - BOUNDS_NORMAL_WIDTH);
            }

            if (collideOnLeft) {
                bounds.x = bounds.x + (bounds.width - BOUNDS_NORMAL_WIDTH);
            }
            */


        } else {
            doAnimation(null);
            bounds.height = BOUNDS_NORMAL_HEIGHT;
            /*
            bounds.width = BOUNDS_NORMAL_WIDTH;
            bounds.x = bounds.x + ((BOUNDS_SLIDING_WIDTH / 2) - (BOUNDS_NORMAL_WIDTH / 2));
            */
        }
    }

    public boolean hasConsumable(String id) {
        return gameState.getProgress().hasConsumable(id);
    }

    public void consume(String id) {
        gameState.getProgress().gainConsumable(id, -1);
    }

    public void cancelAbilitiesOnLevelChange() {
        if (abilities[STATE_RUNNING] != null) {
            abilities[STATE_RUNNING].cancelOnLevelChange(this);
        }
        if (abilities[STATE_JUMPING] != null) {
            abilities[STATE_JUMPING].cancelOnLevelChange(this);
        }
        RoboNinjaProgress rnProgress = (RoboNinjaProgress) gameState.gameProgress;
        LinkedList<McAbility> abilityList = rnProgress.getAbilitiesOfType(STATE_RUNNING);
        for (McAbility mcAbility : abilityList) {
            mcAbility.cancelOnLevelChange(this);
        }

    }

    public void checkAndUnstick() {
        //NLT
        for (int i = 0; i < 20; i++) {
            checkCollisions(vCollInfo, gameState.level);
            if (vCollInfo.isAnyPointCollidingWith(Level.COLLISION_WALL)) {
                bounds.x -= .01f;
            } else {
                return;
            }
        }
        for (int i = 0; i < 40; i++) {
            checkCollisions(vCollInfo, gameState.level);
            if (vCollInfo.isAnyPointCollidingWith(Level.COLLISION_WALL)) {
                bounds.x += .01f;
            } else {
                return;
            }
        }

    }


    // //////////////////////////////////////////////////////////
    // Helper class for collision info, to separate
    // things out a bit
    // //////////////////////////////////////////////////////////

    private class CollisionInfo {
        int topLeft;
        int topRight;
        int midLeft;
        int midRight;
        int botLeft;
        int botRight;
        int midNearBottom;
        int midBelowBottom;
        int wholePointBelow;
        public boolean isMovingWall;
        public Rectangle movingCollisionBlockBounds;
        public WallCollisionCharacter collidingCharacter;

        public boolean isAnyPointCollidingWith(int type) {
            return (topLeft == type || topRight == type || midLeft == type || midRight == type || botLeft == type || botRight == type || midNearBottom == type);
        }

        public boolean isRampInvolved() {
            return (isAnyPointCollidingWith(Level.COLLISION_RAMP_LEFT) || isAnyPointCollidingWith(Level.COLLISION_RAMP_RIGHT) || isRampInvolvedWithFeet());
        }


        public boolean isRampInvolvedWithFeet() {
            return (midNearBottom == Level.COLLISION_RAMP_LEFT || midNearBottom == Level.COLLISION_RAMP_RIGHT
                    || midBelowBottom == Level.COLLISION_RAMP_LEFT || midBelowBottom == Level.COLLISION_RAMP_RIGHT
                    || botLeft == Level.COLLISION_RAMP_LEFT || botLeft == Level.COLLISION_RAMP_RIGHT
                    || botRight == Level.COLLISION_RAMP_LEFT || botRight == Level.COLLISION_RAMP_RIGHT
                    || midLeft == Level.COLLISION_RAMP_LEFT || midLeft == Level.COLLISION_RAMP_RIGHT
                    || midRight == Level.COLLISION_RAMP_LEFT || midRight == Level.COLLISION_RAMP_RIGHT);

        }


        public int backFoot() {
            if (speed > 0) {
                return botLeft;
            } else {
                return botRight;
            }
        }

        public int frontFoot() {
            if (speed < 0) {
                return botLeft;
            } else {
                return botRight;
            }
        }

        public int backMid() {
            if (speed > 0) {
                return midLeft;
            } else {
                return midRight;
            }
        }

        public int frontMid() {
            if (speed < 0) {
                return midLeft;
            } else {
                return midRight;
            }
        }


        public boolean isRampUpward() {

            if (frontFoot() == upwardRamp() || frontMid() == upwardRamp() || backFoot() == upwardRamp()
                    || backMid() == upwardRamp() || midBelowBottom == upwardRamp() || wholePointBelow == upwardRamp()) {
                return true;
            }
            return false;
        }

    }

    // we just use a single collisionInfo and rewrite it to avoid allocation.
    private CollisionInfo vCollInfo = new CollisionInfo();
    private CollisionInfo hCollInfo = new CollisionInfo();

    // this initializes it with collision information
    private void checkCollisions(CollisionInfo colInfo, Level level) {
        float top = bounds.y + bounds.height;
        float mid = bounds.y + (bounds.height / 2);
        float right = bounds.x + bounds.width;
        colInfo.isMovingWall = false;
        colInfo.topLeft = level.isCollision(bounds.x, top);
        colInfo.topRight = level.isCollision(right, top);
        colInfo.midLeft = level.isCollision(bounds.x, mid);
        colInfo.midRight = level.isCollision(right, mid);
        colInfo.botLeft = level.isCollision(bounds.x, bounds.y);
        colInfo.botRight = level.isCollision(right, bounds.y);
        colInfo.midNearBottom = level.isCollision(bounds.x + bounds.width / 2, bounds.y + 0.2f);
        colInfo.midBelowBottom = level.isCollision(bounds.x + bounds.width / 2, bounds.y - 0.2f);
        colInfo.wholePointBelow = level.isCollision(bounds.x + bounds.width / 2, bounds.y - 0.7f);

        //check for collisions with enemies that count as a wall collision
        for (GameCharacter character : gameState.characters) {
            if (character instanceof WallCollisionCharacter) {
                WallCollisionCharacter wcChar = (WallCollisionCharacter) character;
                Rectangle collisionBlockBounds = wcChar.getCollisionBlockBounds(this);
                if (collisionBlockBounds != null) {

                    //I'm not sure if we need to be more careful in actually selecting
                    //which points to report
                    colInfo.botLeft = Level.COLLISION_WALL;
                    colInfo.botRight = Level.COLLISION_WALL;
                    colInfo.midLeft = Level.COLLISION_WALL;
                    colInfo.midRight = Level.COLLISION_WALL;
                    colInfo.topLeft = Level.COLLISION_WALL;
                    colInfo.topRight = Level.COLLISION_WALL;
                    colInfo.midNearBottom = Level.COLLISION_WALL;
                    colInfo.midBelowBottom = Level.COLLISION_WALL;
                    colInfo.wholePointBelow = Level.COLLISION_WALL;
                    colInfo.isMovingWall = true;
                    colInfo.movingCollisionBlockBounds = collisionBlockBounds;
                    colInfo.collidingCharacter = wcChar;

                }
            }
        }
    }

    // /////////////////////////////////////////////////////////////////////////////

    private boolean intersectsWithGluedPlatformX(){
        if (glueToPlatform == null) {
            return false;
        }
        Rectangle platBounds = glueToPlatform.bounds;
        if (platBounds == null) {
            return false;
        }
        if (bounds.x > platBounds.x + platBounds.width) {
            return false;
        }
        if (bounds.x + bounds.width < platBounds.x) {
            return false;
        }
        return true;
    }

    public void moveAndCheckCollisions(float delta) {

        if (glueToPlatformFrames > 0) {
            //if we're glued vertically, and have moved off the X intersection, cancel the gluing
            if (glueToPlatformVerticalOnly && !intersectsWithGluedPlatformX()) {
                glueToPlatform = null;
            }
            glueToPlatformFrames -= delta;
        } else {
            glueToPlatform = null;
        }


        // first check vertical
        float oldY = bounds.y;
        oldYVelocity = velocity.y;
        applyPhysics(delta);

        float yDirection = bounds.y - oldY;

        checkCollisions(vCollInfo, gameState.level);


        // the only thing that really matters for moving up is a collision on
        // top-left or top-right, right? Mid and bottom shouldn't collide if top
        // doesn't.
        if (yDirection > 0) {
            // check for bumping head on spike. (we don't want to check bumping
            // feet on spike on the way up, or else we get too painful of
            // collisions
            // jumping right before a spike)
            if (vCollInfo.topLeft == Level.COLLISION_SPIKE_DEATH || vCollInfo.topRight == Level.COLLISION_SPIKE_DEATH) {
                die();
                return;
            } else if (vCollInfo.topLeft != Level.COLLISION_NONE || vCollInfo.topRight != Level.COLLISION_NONE) {
                // if we hit anything besides a spike with our head, kill
                // upward momentum and start moving back down.
                // (and reset to last frame's Y)
                velocity.y = 0;
                bounds.y = oldY;
            }
            // and if we've been moving up, we're still considered jumping.
            state = STATE_JUMPING;
            glueToPlatform = null;
        } else if (yDirection < 0) {
            // now downward motion

            // first, most obvious, spikes:
            if (vCollInfo.botLeft == Level.COLLISION_SPIKE_DEATH || vCollInfo.botRight == Level.COLLISION_SPIKE_DEATH) {
                die();
                return;
            }

            // if we're not dealing with ramps, do a simple check to see if
            // there's a wall below us. If so, mark feet on ground, and
            // end any vertical movement (reset y to last frame).
            if (!vCollInfo.isRampInvolved()) {
                if (vCollInfo.botLeft == Level.COLLISION_WALL || vCollInfo.botRight == Level.COLLISION_WALL) {
                    velocity.y = 0;
                    if (vCollInfo.isMovingWall) {
                        bounds.y = vCollInfo.movingCollisionBlockBounds.y + vCollInfo.movingCollisionBlockBounds.height;
                        glueToPlatform = vCollInfo.collidingCharacter;
                        glueToPlatformVerticalOnly = true;
                        glueToPlatformFrames = 0.25f;
                    } else {
                        bounds.y = (float) Math.floor(oldY);
                    }

                    if (isSlipping) {
                        speed = -speed;
                    }

                    state = STATE_RUNNING;
                } else {
                    if (glueToPlatformFrames > 0) {
                        //don't set state to jumping if we're glued.
                    } else {
                        state = STATE_JUMPING;
                    }
                }
            } else {
                // but if we are dealing with ramps...boy howdy. We'll deal with
                // that below
                bounds.y = oldY;
            }
        }
        isSlipping = false;

        float multiplier = 1;
        if (isDashing()) {
            multiplier = 2.5f;
        }
        float baseDelta = delta;
        while (multiplier > 0) {
            delta = delta * Math.min(multiplier, 1.0f);
            // ok, now horizontal movement. Again, first the simple stuff.
            float oldX = bounds.x;
            if (!freezeHorizontal) {
                float deltaX = delta * speed;
//                if (isDashing()) {
//                    deltaX *= 2.5;
//                }
                bounds.x += deltaX;
            }

            checkCollisions(hCollInfo, gameState.level);

            // if anything hits a spike, we're dead.
            if (hCollInfo.isAnyPointCollidingWith(Level.COLLISION_SPIKE_DEATH)) {
                //noinspection StatementWithEmptyBody
                if (velocity.y > 0) {
                    // if you are on your upward part of your jump, the spike won't
                    // hurt you.
                } else {
                    die();
                    return;
                }
            }

            // if there's no ramp involved with our feet,
            // bounce our horizontal direction for any sort
            // of horizontal-based collision with a wall.
            // Also, if head collides, we don't care about feet!
            if (!hCollInfo.isRampInvolvedWithFeet()) {
                if (hCollInfo.isAnyPointCollidingWith(Level.COLLISION_WALL)) {
                    // a few things might make us not bounce (special powerups when
                    // jumping)
                    boolean shouldBounce = checkBounceConditions(oldX, oldY, this);
                    if (shouldBounce) {
                        bounds.x = oldX;
                        speed = -speed;
                    } else {
                        if (hCollInfo.isMovingWall) {
                            glueToPlatform = hCollInfo.collidingCharacter;
                            glueToPlatformVerticalOnly = false;
                        }
                    }
                }
            }

            dealWithRamps(yDirection);

            multiplier = multiplier - 1.0f;
            delta = baseDelta;
        }



    }

    // gets the tile for an upward ramp
    private int upwardRamp() {
        if (speed < 0) {
            return Level.COLLISION_RAMP_LEFT;
        } else {
            return Level.COLLISION_RAMP_RIGHT;
        }
    }

    private boolean isRampCollision(int collision) {
        return (collision == Level.COLLISION_RAMP_LEFT || collision == Level.COLLISION_RAMP_RIGHT);
    }

    // gets the vertical "ground" point of a ramp based on the character's
    // center x
    private float getYBasedOnRamp(boolean upward) {
        float centerX = bounds.x + bounds.width / 2;
        float tileX = (float) Math.floor(centerX);
        float tileY = (float) Math.floor(bounds.y + 0.1f);

        //we look directly below the character for a ramp.
        int collision = gameState.level.isCollision(tileX, tileY);
        //if it isn't there, we look even further below. Because if
        // we aren't centered over a ramp, we don't bother adjusting
        if (!isRampCollision(collision)) {
            tileY = tileY - 1;
            collision = gameState.level.isCollision(tileX, tileY);
        }
        //if we are, start adjusting
        if (isRampCollision(collision)) {

            float xPart = centerX - tileX;
            if (upward) {
                if (speed > 0) {
                    return tileY + MiscUtils.clamp(xPart, 0, 1) + 0.1f;
                } else {
                    return tileY + MiscUtils.clamp(1 - xPart, 0, 1) + 0.1f;
                }
            } else {
                if (speed < 0) {
                    return tileY + MiscUtils.clamp(xPart, 0, 1) + 0.1f;
                } else {
                    return tileY + MiscUtils.clamp(1 - xPart, 0, 1) + 0.1f;
                }
            }

        } else {
            //this hack is to help clear the top of a ramp.
            if (upward) {
                if (bounds.y - (float) Math.floor(bounds.y) > 0.9f) {
                    return bounds.y + 0.2f;
                }
            }
            //if we aren't centered, don't mess with anything.
            return bounds.y;
        }

    }


    private void dealWithRamps(float yDirection) {
        // figure out if we're going up or down
        boolean isUpward = vCollInfo.isRampUpward() || hCollInfo.isRampUpward();

        if (vCollInfo.isRampInvolvedWithFeet() || hCollInfo.isRampInvolvedWithFeet()) {

            float rampY = getYBasedOnRamp(isUpward);
            if (state == STATE_RUNNING) {
                // if moving toward the downward ramp (on ground), potentially
                // ignore back foot,
                // and move down ramp smoothly
                bounds.y = rampY;

            } else {
                // if landing on downward ramp, do the same as far as collision,
                // but don't force
                // vertical movement until we make contact.
                if (bounds.y <= rampY) {
                    state = STATE_RUNNING;
                    bounds.y = rampY;
                } else {
                    //we didn't apply physics in the normal section
                    //because I hadn't yet calculated whether ramp magic
                    //was occurring. But do it here, because ramp magic ISN'T occurring.
                    bounds.y += yDirection;
                }
            }
        }
    }


    private void handleInput() {
        // if button tapped, do an action based on selected item and current
        // state
        if (Input.wasSpacebarOrScreenTouch()) {
            if (abilities[state] != null) {
                if (abilities[state].canUse(this)) {
                    abilities[state].trigger(this);
                }
            }
            //special case for state wall, as this happens during jumping state but
            //if we're doing a wall collision.
            if (state == STATE_JUMPING) {
                if (abilities[STATE_WALL] != null && abilities[STATE_WALL].canUse(this)) {
                    abilities[STATE_WALL].trigger(this);
                }
            }
        } else {
            if (abilities[state] != null) {
                abilities[state].notTouched(this);
            }
        }
    }

    private void updateAbilities(float delta) {

        // update current abilities
        for (int i = 0; i < NUM_ABILITY_TYPES; i++) {
            LinkedList<McAbility> abilitiesOfType = gameState.getProgress().getAbilitiesOfType(i);
            for (McAbility ability : abilitiesOfType) {
                if (ability != null) {
                    ability.update(this, delta, abilities[i] == ability);
                }

            }
//            McAbility ability = abilities[i];

        }
    }

    /**
     * For a horizontal bounce, determines whether character should actually
     * bounce (ie if jumping and can wall cling, then don't bounce!)
     *
     * @return whether character should bounce
     */
    private boolean checkBounceConditions(float oldX, float oldY, MainCharacter mainCharacter) {
        if (abilities[STATE_WALL] != null) {
            return ((WallAbility) abilities[STATE_WALL]).onWallCollision(oldX, oldY, mainCharacter);
        }
        return true;
    }

    @Override
    protected boolean shouldInitGraphicsOnCreate() {
        return false;
    }

    @Override
    protected String getTextureName() {

        if (useOldGraphics) {
            return "mc";
        }
        return "roboninja";
    }

    @Override
    protected Vector2 getFrameSize() {
        if (useOldGraphics) {
            return new Vector2(100, 100);
        }
        return new Vector2(128, 128);
    }

    @Override
    protected void initAnimations(Map<String, Animation> animations) {


        if (useOldGraphics) {
            initOldAnimations(animations);
            return;
        }

        TextureRegion[][] tr = textureRegions;

        Animation walkAnimation = new Animation(0.15f, tr[3][0],
                tr[3][1],
                tr[3][2],
                tr[3][3],
                tr[3][4],
                tr[3][5],
                tr[3][6],
                tr[3][7]

        );


        walkAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ANIM_WALK, walkAnimation);

        Animation jumpAnimation = new Animation(1.0f, tr[1][5]);
        jumpAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ANIM_JUMP, jumpAnimation);

        Animation slideAnimation = new Animation(0.15f, tr[5][7], tr[6][0], tr[6][1], tr[6][2]);
        slideAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(SlideAbility.ID, slideAnimation);

        Animation clingAnimation = new Animation(0.15f, tr[6][3], tr[6][4], tr[6][5], tr[6][6]);
        clingAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ClingAbility.ID, clingAnimation);
        animations.put(SlipAbility.ID, clingAnimation);


        Animation slamAnimation = new Animation(0.05f, tr[4][7], tr[5][0], tr[5][1]);
        slamAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(SlamAbility.ID, slamAnimation);

        Animation doubleJumpAnimation = new Animation(0.3f, tr[1][6]);
        doubleJumpAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(DoubleJumpAbility.ID, doubleJumpAnimation);

        Animation jetpackAnimation = new Animation(0.3f, tr[2][4], tr[2][5], tr[2][6], tr[2][7]);
        jetpackAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(JetPackAbility.ID, jetpackAnimation);

        Animation shootAnimation = new Animation(0.09f, tr[4][1], tr[4][2], tr[4][3], tr[4][4], tr[4][5]);
        shootAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(ShootAbility.ID, shootAnimation);

        Animation landingAnimation = new Animation(0.075f, tr[2][0], tr[2][1], tr[2][2], tr[2][3]);
        landingAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(ANIM_LANDING, landingAnimation);

        Animation fallingAnimation = new Animation(1.0f, tr[1][7]);
        fallingAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(ANIM_FALL, fallingAnimation);

    }

    protected void initOldAnimations(Map<String, Animation> animations) {
        Animation walkAnimation = new Animation(0.15f, textureRegions[1][0], textureRegions[1][1],
                textureRegions[1][2], textureRegions[2][0]);

        walkAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ANIM_WALK, walkAnimation);

        Animation jumpAnimation = new Animation(1.0f, textureRegions[1][2]);
        jumpAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ANIM_JUMP, jumpAnimation);

        Animation slideAnimation = new Animation(0.15f, textureRegions[3][0], textureRegions[3][1]);
        slideAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(SlideAbility.ID, slideAnimation);

        Animation clingAnimation = new Animation(0.15f, textureRegions[0][1]);
        clingAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(ClingAbility.ID, clingAnimation);
        animations.put(SlipAbility.ID, clingAnimation);

        Animation slamAnimation = new Animation(0.15f, textureRegions[2][1]);
        slamAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(SlamAbility.ID, slamAnimation);

        Animation doubleJumpAnimation = new Animation(0.3f, textureRegions[2][2], textureRegions[1][2], textureRegions[1][2], textureRegions[1][2]);
        doubleJumpAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        animations.put(DoubleJumpAbility.ID, doubleJumpAnimation);

        Animation jetpackAnimation = new Animation(0.3f, textureRegions[2][2]);
        doubleJumpAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animations.put(JetPackAbility.ID, jetpackAnimation);

        animations.put(ShootAbility.ID, walkAnimation);
        animations.put(ANIM_LANDING, walkAnimation);
        animations.put(ANIM_FALL, jumpAnimation);
    }

    @Override
    protected void initTextures(String name, int framePxWidth, int framePxHeight) {
        // resize all the regions slightly to avoid bleedover of pixels at the
        // top
        super.initTextures(name, framePxWidth, framePxHeight);
        for (TextureRegion[] textureRegion : textureRegions) {
            for (TextureRegion frame : textureRegion) {
                frame.setRegionY(frame.getRegionY() + 4);
                frame.setRegionHeight(frame.getRegionHeight() - 4);
            }
        }

    }

}
