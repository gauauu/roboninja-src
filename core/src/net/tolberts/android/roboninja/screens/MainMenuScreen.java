package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.cutscene.Cutscene;
import net.tolberts.android.roboninja.cutscene.CutsceneFinishedCallback;
import net.tolberts.android.roboninja.hud.BigTitleAnimatedNinja;
import net.tolberts.android.roboninja.hud.ConfirmStartNewGameMenu;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.hud.NewGameSelectSpeedMenu;
import net.tolberts.android.roboninja.screens.actors.MenuItemActor;
import net.tolberts.android.roboninja.screens.actors.TextActor;

public class MainMenuScreen extends MenuScreen {

    public final static int CHEAT_FIRST_NUM = 6;
    public final static int CHEAT_SECOND_NUM = 10;
    public final static String MAIN_MENU_MUSIC = "pulse2";
    private TextActor titleActor;
    private boolean menuShowing = true;
    private int cheatCounter = 0;
    private Texture titleTexture;
    private Texture deco;
    private Texture dragon;
    private boolean notShown = true;
    private BigTitleAnimatedNinja ninja;
    private HudCallback exitGameCallback;

    public MainMenuScreen(GameState state) {
        super(state);
    }

    @Override
    public void show() {
        super.show();
        AudioPlayer.playMusic(MAIN_MENU_MUSIC);
        gameState.preloadAsync();
        exitGameCallback = new HudCallback() {
            @Override
            public void callback() {
                Gdx.app.exit();
            }
        };
        setBackButtonCallback(exitGameCallback);
    }

    @Override
    protected void buildScreen() {

        titleActor = new TextActor("Robo      Ninja", Fonts.HEADER_FONT);
        titleActor.centerAt(RoboNinjaGame.WIDTH / 2, RoboNinjaGame.HEIGHT - (RoboNinjaGame.HEIGHT / 5));
//        titleActor.setPosition(38, RoboNinjaGame.HEIGHT
//                - (RoboNinjaGame.HEIGHT / 4));
        stage.addActor(titleActor);

        ninja = new BigTitleAnimatedNinja();
        ninja.setPosition(219, 215);
        stage.addActor(ninja);


        //titleTexture = Art.getTexture("logo");
        deco = Art.getTexture("obj_ui_deco");
        dragon = Art.getTexture("obj_dragonL");



    }

    protected void addMenuItems() {
        menu.setButtonWidth(250);
        if (RoboNinjaProgress.hasProgress()) {
            menu.addMenuItem("Continue",
                    new MenuItemActor.SelectedCallback() {
                        @Override
                        public void selected() {
                            gameState.resumeGame();
                            checkCheat();
                            setScreen(new GameScreen(gameState));
                        }
                    });

        }
        menu.addMenuItem("New Game", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                newGameButton();
            }
        });

        menu.addMenuItem("Options", new MenuItemActor.SelectedCallback() {

            @Override
            public void selected() {
                OptionsScreen optionsScreen = new
                        OptionsScreen(gameState);
                optionsScreen.setBackScreen(MainMenuScreen.this);
                setScreen(optionsScreen);
                //FOR TESTING ENDING SCREEN:
//                EndingStatsScreen screen = new EndingStatsScreen((RoboNinjaGameState)gameState);
//                setScreen(screen);
            }
        });
//
//        menu.addMenuItem("Aaron Game", new MenuItemActor.SelectedCallback() {
//            @Override
//            public void selected() {
//                ((RoboNinjaGameState) gameState).mc = new MainCharacter(gameState, true);
//                ((RoboNinjaGameState) gameState).newGame("aaron");
//                setScreen(new GameScreen(gameState));
//            }
//        });

//        menu.addMenuItem("Cutscene", new MenuItemActor.SelectedCallback() {
//            @Override
//            public void selected() {
////                Cutscene intro = new Cutscene("intro", new CutsceneFinishedCallback(){
////
////                Cutscene intro = new Cutscene("ending", new CutsceneFinishedCallback() {
////                    @Override
////                    public void cutsceneFinished() {
////                        newGameButton();
////                    }
////                });
////                CutsceneScreen csScreen = new CutsceneScreen(gameState, intro);
//                Screen csScreen = new EndingStatsScreen((RoboNinjaGameState)gameState);
//                setScreen(csScreen);
//            }
//        });

        ///////////////////////////
        // sets up the cheat code
        ///////////////////////////
        final TextActor ta = new TextActor("   ", Fonts.SMALL_FONT);
        ta.addListener(new ClickListener() {
                           @Override
                           public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                               if (cheatCounter < CHEAT_FIRST_NUM + 1) {
                                   cheatCounter++;
                                   if (cheatCounter == CHEAT_FIRST_NUM) {
                                       AudioPlayer.playSound(Audio.ACQUIRE_ITEM);
                                   }
                               }
                               return super.touchDown(event, x, y, pointer, button);
                           }
                       }
        );
        stage.addActor(ta);

        final TextActor t2 = new TextActor("   ", Fonts.SMALL_FONT);
        t2.addListener(new ClickListener() {
                           @Override
                           public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                               if (cheatCounter > CHEAT_FIRST_NUM - 1) {
                                   cheatCounter++;
                                   if (cheatCounter >= CHEAT_SECOND_NUM) {
                                       AudioPlayer.playSound(Audio.PLAYER_LASER, true);
                                   }
                               }
                               return super.touchDown(event, x, y, pointer, button);
                           }
                       }
        );
        t2.setPosition(0, stage.getHeight() - 15);
        stage.addActor(t2);

    }

    private void checkCheat() {
        if (cheatCounter > CHEAT_SECOND_NUM) {
            ((RoboNinjaGameState) gameState).acquireAllAbilities();
        }
    }

    protected void newGameButton() {

        if (RoboNinjaProgress.hasProgress()) {
            confirmStartNewGame();
        } else {
            selectSpeedForNewGame();
        }
    }

    protected void selectSpeedForNewGame() {
//        if (((RoboNinjaGame)gameState.game).adapterHasFlag(PlatformAdapter.FLAG_ALLOW_FAST_MODE)) {
//            showSpeedSelectMenu();
//        } else {
//            startNewGame(GameState.SPEED_MODE_NORMAL);
//        }
            showSpeedSelectMenu();
    }

    private void showSpeedSelectMenu() {

        final NewGameSelectSpeedMenu newMenu = new NewGameSelectSpeedMenu(gameState) {
            protected void onSelect(int result) {
                setMainMenuVisibility(true);
                menu.setVisible(true);
                startNewGame(result);
            }
        };


        setMainMenuVisibility(false);
        menu.setVisible(false);
        stage.addActor(newMenu);
        setBackButtonCallback(new HudCallback() {
            @Override
            public void callback() {
                menu.setVisible(true);
                newMenu.remove();
                newMenu.setVisible(false);
                setBackButtonCallback(exitGameCallback);
                setMainMenuVisibility(true);
            }
        });
    }

    private void confirmStartNewGame() {
        final ConfirmStartNewGameMenu newMenu = new ConfirmStartNewGameMenu() {
            @Override
            protected void onSelect(boolean result) {
                setMainMenuVisibility(true);
                menu.setVisible(true);
                if (result) {
                    this.remove();
                    selectSpeedForNewGame();
                } else {
                    this.remove();
                }
            }
        };

        setMainMenuVisibility(false);
        menu.setVisible(false);
        stage.addActor(newMenu);
        setBackButtonCallback(new HudCallback() {
            @Override
            public void callback() {
                menu.setVisible(true);
                newMenu.remove();
                newMenu.setVisible(false);
                setBackButtonCallback(exitGameCallback);
                setMainMenuVisibility(true);
            }
        });
    }

    public void setMainMenuVisibility(boolean visible) {
        menu.setVisible(visible);
        frame.setVisible(visible);
        titleActor.setVisible(visible);
        ninja.setVisible(visible);
    }



    protected void startNewGame(final int speedMode) {

        Cutscene intro = new Cutscene("intro", new CutsceneFinishedCallback() {
            @Override
            public void cutsceneFinished() {
                GameScreen newScreen = new GameScreen(gameState);
                gameState.newGame(speedMode);
                checkCheat();
                setScreen(newScreen);

            }
        });
        CutsceneScreen csScreen = new CutsceneScreen(gameState, intro);
        setScreen(csScreen);
    }


    protected void handleInput() {
        if (!menuShowing) {
            if (Input.wasSpacebarOrScreenTapped()) {
                menuShowing = true;
            }
        } else {
            if (Input.wasSpacebarPressed()) {
                newGameButton();
            }
        }
    }


    protected void renderMenu() {

//        menu.setVisible(true);
//        float alpha = Math.min(1.0f, 0.33f * tick);
//        titleActor.alpha = alpha;
//        if (alpha > 0.99f) {
//            menuShowing = true;
//        }
//        if (menuShowing && notShown) {
//            titleActor.alpha = 1.0f;
//            menu.setVisible(true);
//            notShown = false;
//        }
    }

    @Override
    protected void drawMovingBackground() {
        super.drawMovingBackground();
        batch.begin();
        batch.draw(deco, 0, 0, RoboNinjaGame.WIDTH, 43);
        batch.draw(dragon, 7, 15, 0, 0, 69, 47, 0.8f, 0.8f, 0.0f, 0, 0, 69, 47, false, false);
        batch.draw(dragon, RoboNinjaGame.WIDTH - 63, 15, 0, 0, 69, 47, 0.8f, 0.8f, 0.0f, 0, 0, 69, 47, true, false);
        batch.end();


    }

    @Override
    protected void renderAboveFrame() {
        if (titleActor.isVisible()) {
            batch.begin();
            //batch.draw(titleTexture, 50, 190, 0, 0, 717, 172, 0.55f, 0.55f, 0, 0, 0, 717, 172, false, false);
            batch.end();

        }
    }

}
