package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Screen;
import net.tolberts.android.game.GamePrefs;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.screens.actors.MenuItemActor;
import net.tolberts.android.roboninja.screens.actors.TextActor;

public class ChooseIfPlayIntegrationScreen extends MenuScreen implements Screen {


    private final TextActor titleActorA;
    private final TextActor titleActorB;

    public ChooseIfPlayIntegrationScreen(GameState state) {
        super(state);


        titleActorA = new TextActor("Enable Google", Fonts.SMALL_FONT);
        titleActorA.centerAt(RoboNinjaGame.WIDTH / 2, RoboNinjaGame.HEIGHT - (RoboNinjaGame.HEIGHT / 4) + 20);
        stage.addActor(titleActorA);

        titleActorB = new TextActor("Play Games Service?", Fonts.SMALL_FONT);
        titleActorB.centerAt(RoboNinjaGame.WIDTH / 2, RoboNinjaGame.HEIGHT - (RoboNinjaGame.HEIGHT / 3) + 10);
        stage.addActor(titleActorB);

    }

    @Override
    protected void handleInput() {

    }

    @Override
    protected void renderMenu() {

        batch.begin();
        batch.draw(background, 0, -100);
        batch.end();

//        titleActorA.setPosition(130, RoboNinjaGame.HEIGHT
//                - (RoboNinjaGame.HEIGHT / 4) + 12);
//        titleActorB.setPosition(80, RoboNinjaGame.HEIGHT
//                - (RoboNinjaGame.HEIGHT / 3) + 12);

    }

    @Override
    protected void addMenuItems() {
        menu.addMenuItem("Yes", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                pick(true);
            }
        });
        menu.addMenuItem("No", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                pick(false);
            }
        });
    }

    private void pick(boolean choice) {
        GamePrefs.setBool(PlayServicesAdapter.ALLOW_PLAY_SERVICES, choice);
        if (choice) {
            final PlayServicesAdapter playServicesAdapter = ((RoboNinjaGame) gameState.game).platformAdapter.getPlayServicesAdapter();
            if (playServicesAdapter != null) {
                playServicesAdapter.attemptLogin();
            }
        }
        setScreen(new MainMenuScreen(gameState));
    }

}