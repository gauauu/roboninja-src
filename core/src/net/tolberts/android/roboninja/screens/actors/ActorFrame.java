package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.RoboNinjaGame;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;

public class ActorFrame extends Group {

    public final static int STYLE_NORMAL = 0;
    public final static int STYLE_NONE = 1;
	public final static int STYLE_GRAY_FRAME = 2;
    public final static int STYLE_GREEN_FRAME = 3;

	NinePatch ninePatch;
    private int style = STYLE_NORMAL;
    Texture frameDeco;
    private Texture dragonL;
    private Texture dragonR;

    public ActorFrame() {
//		ninePatch = new NinePatch(Art.getTexture("blankPanelScaled"), 48, 48, 48, 48);
		ninePatch = new NinePatch(Art.getTexture("frame2"), 32, 32, 32, 32);
        frameDeco = Art.getTexture("obj_ui_deco");
        dragonL = Art.getTexture("obj_dragonL");
        dragonR = Art.getTexture("obj_dragonR");
	}

	public void setStyle(int style) {
        this.style = style;
		if (style == STYLE_GRAY_FRAME) {
			ninePatch = new NinePatch(Art.getTexture("gray-frame"), 32, 32, 32, 32);
		} else if (style == STYLE_GREEN_FRAME) {
            ninePatch = new NinePatch(Art.getTexture("green-frame"), 32, 32, 32, 32);
        }
    }

    @Override
	public void draw(Batch batch, float parentAlpha) {
		drawFrame(batch);
		super.draw(batch, parentAlpha);
	}
	
	public void setBoundsByMargin(float margin) {
		setBounds(margin, margin, RoboNinjaGame.WIDTH - margin * 2, RoboNinjaGame.HEIGHT - margin * 2);
	}

	private void drawFrame(Batch batch) {
        if (style != STYLE_NONE) {
            ninePatch.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
        if (style == STYLE_GRAY_FRAME || style == STYLE_GREEN_FRAME) {
            batch.draw(frameDeco, getX() - 20, getY() + getHeight() - 10, getWidth() + 40, 40);
            batch.draw(dragonL, getX() - 10, getY() + getHeight() + 7, 34.5f, 23.5f);
            batch.draw(dragonR, getX() + getWidth() - 20, getY() + getHeight() + 7, 34.5f, 23.5f);
            batch.draw(frameDeco, getX() - 20, getY() - 23, getWidth() + 40, 40, 0, 0, frameDeco.getWidth(), frameDeco.getHeight(), false, true);
        }
	}

}
