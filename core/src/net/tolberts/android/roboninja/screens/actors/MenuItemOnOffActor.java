package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import net.tolberts.android.game.GamePrefs;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.hud.HudCallback;

public class MenuItemOnOffActor extends Group {

    private final String prefId;
    private MenuButtonActor onButton;
    private MenuButtonActor offButton;

    public MenuItemOnOffActor(String text, String fontName, String prefId, boolean defaultValue) {

        TextActor label = new TextActor(text, fontName);
        label.setX(0);
        label.setY(10);
        label.setFontScale(Fonts.FONT_SCALE_HEADER_BUTTONS);
        addActor(label);

        this.prefId = prefId;
        final boolean prefValue = GamePrefs.getBool(prefId, defaultValue);



        onButton = new MenuButtonActor("On", new HudCallback() {
            @Override
            public void callback() {
                setOn();
            }
        }, 110);
        offButton = new MenuButtonActor("Off", new HudCallback() {
            @Override
            public void callback() {
                setOff();
            }
        }, 110);

        onButton.setPosition(200, 0);
        offButton.setPosition(305, 0);

        onButton.setHighlighted(prefValue);
        offButton.setHighlighted(!prefValue);


        addActor(onButton);
        addActor(offButton);

    }

    private void setOn() {
        onButton.setHighlighted(true);
        offButton.setHighlighted(false);
        GamePrefs.setBool(prefId, true);
    }

    private void setOff() {
        onButton.setHighlighted(false);
        offButton.setHighlighted(true);
        GamePrefs.setBool(prefId, false);
    }


}
