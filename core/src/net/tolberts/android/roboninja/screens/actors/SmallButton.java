package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import net.tolberts.android.game.loaders.AbilityIcons;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.hud.HudTapListener;

public class SmallButton extends Actor {

    public final static String BACK = "ico_x";
    public static final int STYLE_FANCY = 0;
    public static final int STYLE_CLEAN = 1;

    protected Texture buttonFrame;
    protected Texture buttonFrameDisabled;
    protected Texture iconTexture;

    protected float scale = 1.0f;

    protected final static float BASE_HEIGHT = 69;
    protected final static float BASE_WIDTH = 93;

    protected float xIconOffset = 30.0f;
    protected float yIconOffset = 18.0f;
    protected Drawable icon;
    private float iconScale = -1;

    protected final static float HOTZONE_OFFSET_X = 0;
    protected final static float HOTZONE_OFFSET_Y = 0;

    protected boolean disabled = false;

    public SmallButton(String icon, final InputListener onTap) {
        super();
        setHeight(BASE_HEIGHT + HOTZONE_OFFSET_Y);
        setWidth(BASE_WIDTH + HOTZONE_OFFSET_X);
        initButtonGraphics();
        if (icon != null) {
            iconTexture = Art.getTexture(icon);
        }
        addListener(onTap);
        setStyle(STYLE_CLEAN);
    }

    protected void initButtonGraphics() {
        buttonFrame = Art.getTexture("btn_bg1");
        buttonFrameDisabled = Art.getTexture("btn_bg1_disabled");
    }

    public void setIconOffset(float x, float y) {
        xIconOffset = x;
        yIconOffset = y;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public void setScale(float scale) {
        this.scale = scale;
        setHeight(scale * BASE_HEIGHT + HOTZONE_OFFSET_Y);
        setWidth(scale * BASE_WIDTH + HOTZONE_OFFSET_X);
    }


    public void setIconScale(float iconScale) {
        this.iconScale = iconScale;
    }

    public static SmallButton NewBackButton(HudTapListener onTap) {
        return new SmallButton(BACK, onTap);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float localIconScale = iconScale;
        if (localIconScale == -1) {
            localIconScale = scale;
        }

        super.draw(batch, parentAlpha);
        Texture toDraw = buttonFrame;
        if (disabled) {
            toDraw = buttonFrameDisabled;
        }
        batch.draw(toDraw, getX(), getY(), BASE_WIDTH * scale, BASE_HEIGHT * scale);
        if (iconTexture != null) {
            batch.draw(iconTexture, getX() + (xIconOffset * localIconScale), getY() + (yIconOffset * localIconScale), iconTexture.getWidth() * localIconScale, iconTexture.getHeight() * localIconScale);
        }
    }

    public void setIcon(String icon) {
        if (icon == null) {
            iconTexture = null;
        } else {
            iconTexture = AbilityIcons.getAsTexture(icon);
        }
    }

    public void setStyle(int style) {
        if (style == STYLE_FANCY) {
            buttonFrame = Art.getTexture("btn_bg1");
            buttonFrameDisabled = Art.getTexture("btn_bg1_disabled");
        } else if (style == STYLE_CLEAN){
            buttonFrame = Art.getTexture("btn_2");
            buttonFrameDisabled = Art.getTexture("btn_2_disabled");
        }
    }
}
