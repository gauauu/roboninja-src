package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import net.tolberts.android.roboninja.RoboNinjaGame;

/**
 * A stage that simulates the older version of the stage class before it got more complicated with viewports.
 */
public class RoboNinjaStage extends Stage{

    private Group baseGroup;

    public RoboNinjaStage() {
        super();
        setViewport(new ScalingViewport(Scaling.stretch, RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT));
        getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        baseGroup = new Group();
        super.addActor(baseGroup);
        baseGroup.setPosition(-RoboNinjaGame.WIDTH / 2, -RoboNinjaGame.HEIGHT / 2);

    }

    @Override
    public void addActor(Actor actor) {
        baseGroup.addActor(actor);
    }
}
