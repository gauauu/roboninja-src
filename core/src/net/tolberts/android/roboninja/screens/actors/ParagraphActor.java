package net.tolberts.android.roboninja.screens.actors;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Group;

public class ParagraphActor extends Group {

    protected final String fontName;
    float spacing;
    float fontScale = 1.0f;

	// don't ACTUALLY need to track this separately from children,
	// but iteration through children is a little weird
	// (it's some sort of special collection with a weird iteration mechanism)
	// so we just track it separately
	List<TextActor> lineActors;

	public ParagraphActor(String text, String fontName, float spacing) {
		super();
        this.fontName = fontName;
        this.spacing = spacing;

        setText(text);

    }

    public void setText(String text) {
        if (text == null) {
            text = "";
        }

		String[] split = text.split("\\n");
        clearChildren();
		lineActors = new LinkedList<TextActor>();

		for (String line : split) {
			TextActor lineActor = new TextActor(line, fontName);
			addActor(lineActor);
			lineActors.add(lineActor);
		}
        setFontScale(fontScale);
	}

	public void setFontScale(float scale) {
        fontScale = scale;
		for (TextActor lineActor : lineActors) {
			lineActor.setFontScale(scale);
		}
	}

	public float getTotalLineHeight() {
		float total = 0;
		for (TextActor lineActor : lineActors) {
			float lineHeight = lineActor.getBounds().height;
			total += lineHeight + spacing;
		}
		return total;
	}

	public void centerAt(float x, float y) {
		setPosition(0,  0);
		setWidth(x * 2);
		setHeight(y * 2);
		float lineY = y + (getTotalLineHeight() / 2);
		for (TextActor lineActor : lineActors) {
			lineActor.centerAt(x, lineY - spacing);
			lineY -= lineActor.getBounds().height + spacing;
		}
	}

    public void centerOn(ActorFrame frame) {
        centerAt(frame.getWidth() / 2, frame.getHeight() / 2);
    }
}
