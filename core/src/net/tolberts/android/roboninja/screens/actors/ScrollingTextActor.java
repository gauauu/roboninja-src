package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.LinkedList;
import java.util.List;

public class ScrollingTextActor extends Group {

    protected final String fontName;
    private final Texture arrowUp;
    float spacing;
    float fontScale = 0.4f;

    float scrollPosition = 0.0f;

    float leftMargin = 10;

    // don't ACTUALLY need to track this separately from children,
    // but iteration through children is a little weird
    // (it's some sort of special collection with a weird iteration mechanism)
    // so we just track it separately
    List<TextActor> lineActors;
    private int maxScrollPosition;
    private float flingVelocity = 0;
    private boolean bottomShowing;

    public ScrollingTextActor(List<String> text, String fontName, float spacing) {
        super();
        this.fontName = fontName;
        this.spacing = spacing;
        flingVelocity = 0;
        bottomShowing = false;

        arrowUp = Art.getTexture("arrow-up");

        setText(text);
    }

    public void setupInput(InputMultiplexer inputMultiplexer) {

        inputMultiplexer.addProcessor(new com.badlogic.gdx.input.GestureDetector(new com.badlogic.gdx.input.GestureDetector.GestureListener() {


            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                flingVelocity = velocityY;
                return true;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                float panY = deltaY * RoboNinjaGame.ACTUAL_RATIO_Y;
                    scrollBy(panY);

                    return true;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }
        }));
        inputMultiplexer.addProcessor(new InputProcessor() {

            @Override
            public boolean keyDown(int keycode) {
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return true;
            }
        });

    }

    public void scrollBy(float delta) {
        if (bottomShowing && delta < 0) {
            return;
        }
        scrollPosition += delta;
        if (scrollPosition > maxScrollPosition) {
            scrollPosition = maxScrollPosition;
        }

        layout();
    }

    public void setLeftMargin(float margin) {
        leftMargin = margin;
        layout();
    }

    public void setText(List<String> text) {

        clearChildren();
        lineActors = new LinkedList<TextActor>();

        for (String line : text) {
            TextActor lineActor = new TextActor(line, fontName);
            addActor(lineActor);
            lineActors.add(lineActor);
        }
        setFontScale(fontScale);
    }

    public void setFontScale(float scale) {
        fontScale = scale;
        for (TextActor lineActor : lineActors) {
            lineActor.setFontScale(scale);
        }
    }


    public void layout() {
        float lineY = getHeight() - scrollPosition;
        bottomShowing = true;
        for (TextActor lineActor : lineActors) {
            if (lineY < 10) {
                lineY = -500;
                bottomShowing = false;
            }
            lineActor.setPosition(leftMargin, lineY);
            lineY -= lineActor.getBounds().height + spacing;
        }
    }

    public void setScrollPosition(float scrollPosition) {
        this.scrollPosition = scrollPosition;
        layout();
    }


    public void setMaxScrollPosition(int maxScrollPosition) {
        this.maxScrollPosition = maxScrollPosition;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (flingVelocity > 0.01f) {
            scrollBy(flingVelocity * delta/2);
            flingVelocity -= delta * 5000;
            if (flingVelocity < 0) {
                flingVelocity = 0;
            }
        }
        if (flingVelocity < 0.01f) {
            scrollBy(flingVelocity * delta/2);
            flingVelocity += delta * 5000;
            if (flingVelocity > 0) {
                flingVelocity = 0;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        final float xPosition = getWidth() + 20;
        //TODO: these hardcoded numbers should be relative to size, not hardcoded....
        if (scrollPosition < maxScrollPosition) {
            batch.draw(arrowUp, xPosition, 220, 12, 11, 0,0, 212, 202, false, false);
        }
        if (!bottomShowing) {
            batch.draw(arrowUp, xPosition, 10, 12, 11, 0,0, 212, 202, false, true);
        }
    }
}
