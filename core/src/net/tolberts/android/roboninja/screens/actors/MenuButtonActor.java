package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.hud.HudCallback;

public class MenuButtonActor extends Group {

    public static final float margin = 10.0f;
    private final String text;
    private final NinePatch ninePatch;
    private final NinePatch ninePatchHighlight;
    private final TextActor textActor;
    private boolean highlighted = false;

    public MenuButtonActor(String text, final HudCallback callback, float width) {
        this.text = text;

        textActor = new TextActor(text, Fonts.HEADER_FONT, margin);
        textActor.setFontScale(Fonts.FONT_SCALE_HEADER_BUTTONS);

        this.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                callback.callback();
            }
        });
        ninePatch = new NinePatch(Art.getTexture("button-9-patch"), 42, 42, 1, 1);
        ninePatchHighlight = new NinePatch(Art.getTexture("button-9-patch-selected"), 42, 42, 1, 1);
        addActor(textActor);
        setWidth(width);
        setHeight(textActor.getHeight());
        layout();

    }

    @Override
    public void setWidth(float width) {
        super.setWidth(width);
        layout();
    }

    public void layout() {
        textActor.centerAt(getWidth() / 2.0f, textActor.getHeight() / 2.0f);
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (highlighted) {
            ninePatchHighlight.draw(batch, getX(), getY(), getWidth(), getHeight());
        } else {
            ninePatch.draw(batch, getX(), getY(), getWidth(), getHeight());
        }
        super.draw(batch, parentAlpha);

    }
}
