package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.scenes.scene2d.Actor;
import net.tolberts.android.game.loaders.Fonts;

public class TextActor extends Actor {

	public String text;
	public BitmapFont font;
	public float alpha = 1.0f;
	float fontScale = 1.0f;
	private float marginY = 0.0f;
	private float marginX = 0.0f;

	public TextActor(String text, String fontName) {
		this(text, fontName, 0);
	}
	
	public TextActor(String text, String fontName, float margin) {
		super();
		this.text = text;
		this.font = Fonts.getFont(fontName);
		this.marginY = margin;
		this.marginX = margin;
        this.setColor(1.0f, 1.0f, 1.0f, 1.0f);
		reLayout();
	}


	public void setMarginX(float marginX) {
        this.marginX = marginX;
    }

	
	public void setFontScale(float scale) {
		this.fontScale = scale;
		reLayout();
	}
	
	public void centerAt(float x, float y) {
		font.setScale(fontScale);
		TextBounds bounds = font.getBounds(text);
		float xPos = x - (bounds.width / 2);
		float yPos = y - (bounds.height / 2);
		setPosition(xPos - marginX, yPos - marginY);
		font.setScale(1.0f);
		reLayout();
	}

	private void reLayout() {
		TextBounds bounds = getBounds();
		this.setWidth(bounds.width + marginX * 2);
		this.setHeight(bounds.height + marginY * 2);
	}
	
	public TextBounds getBounds() {
		font.setScale(fontScale);
		TextBounds bounds = font.getBounds(text);
		font.setScale(1.0f);
		return bounds;
	}

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        font.setScale(fontScale);
        font.setColor(this.getColor());


        font.setColor(1.0f, 1.0f, 1.0f, alpha * parentAlpha);
        font.draw(batch, text, getX() + marginX, getY() + getHeight() - marginY);
        font.setScale(1.0f);
    }


	public void setText(String string) {
		text = string;
		reLayout();
	}

    public String getText() {
        return text;
    }

}
