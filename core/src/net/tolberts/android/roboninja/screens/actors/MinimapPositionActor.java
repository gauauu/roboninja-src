package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import net.tolberts.android.game.loaders.Art;

public class MinimapPositionActor extends Image {

    float totalTime = 0;

    public MinimapPositionActor() {
        super(Art.getAsDrawable("positionIndicator"));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (Math.round(totalTime) != Math.round(totalTime + delta)) {
            setVisible(!isVisible());
        }
        totalTime += delta;
    }

    public void centerOn(float x, float y) {
        setPosition(x - getWidth() / 2, y - getHeight() / 2);
    }
}
