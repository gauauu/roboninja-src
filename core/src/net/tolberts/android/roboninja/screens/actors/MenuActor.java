package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.SnapshotArray;

public class MenuActor extends Group {

    float spacing = 60;
    private float childFontScale = 1.0f;
    private String defaultFont;

    private float buttonWidth = -1;

    public Actor addMenuItem(Actor menuItemActor) {
        addActor(menuItemActor);
        if (menuItemActor instanceof MenuItemActor) {
            ((MenuItemActor) menuItemActor).setFontScale(childFontScale);
            if (buttonWidth > -1) {
                ((MenuItemActor) menuItemActor).setButtonWidth(buttonWidth);
            }
        }
        reLayout();
        return menuItemActor;
    }

    public void setButtonWidth(float buttonWidth) {
        this.buttonWidth = buttonWidth;
    }

    public void setDefaultFont(String defaultFont) {
        this.defaultFont = defaultFont;
    }

    public Actor addMenuItem(String label, MenuItemActor.SelectedCallback callback) {
        return addMenuItem(new MenuItemActor(label, defaultFont, callback));
    }

    public Actor addMenuItem(String label, String font, MenuItemActor.SelectedCallback callback) {
        return addMenuItem(new MenuItemActor(label, font, callback));
    }

    public Actor addOnOffMenuItem(String label, String prefId, boolean defaultVal) {
        return addMenuItem(new MenuItemOnOffActor(label, defaultFont, prefId, defaultVal));
    }

    protected float getStartingTop() {
        return 0;
    }

    public void reLayout() {
        SnapshotArray<Actor> children = getChildren();
        float top = getStartingTop();
        for (Actor child : children) {
            //this terrible design makes me cry.
            // But I'm too tired tonight to refactor and do it correctly.
            if (child instanceof MenuItemActor) {
                float boardCenter = getWidth() / 2;
                float halfChildWidth = child.getWidth() / 2;
                float childStart = boardCenter - halfChildWidth;
                child.setPosition(childStart, top);
                ((MenuItemActor)child).relayout();
            } else if (child instanceof MenuItemOnOffActor) {
                child.setPosition(40, top);
            }
            top -= spacing;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    public void setChildFontScale(float childFontScale) {
        this.childFontScale = childFontScale;
    }

}
