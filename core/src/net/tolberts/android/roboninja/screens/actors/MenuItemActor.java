package net.tolberts.android.roboninja.screens.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.*;
import net.tolberts.android.game.loaders.Art;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MenuItemActor extends Group {

	private NinePatch ninePatch;
    private float buttonWidth = -1;
    private TextActor textActor;

    public MenuItemActor(String text, String fontName, final SelectedCallback callback) {
        textActor = new TextActor(text, fontName, 10);
		textActor.setMarginX(32);
		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				callback.selected();
			}
		});
		ninePatch = new NinePatch(Art.getTexture("button-9-patch"), 42, 42, 1, 1);
        addActor(textActor);
	}

    public void setButtonWidth(float buttonWidth) {
        this.buttonWidth = buttonWidth;
        setWidth(buttonWidth);
        setPosition(getParent().getWidth() / 2 - buttonWidth / 2, getHeight());
    }

    public void setFontScale(float childFontScale) {
        textActor.setFontScale(childFontScale);
    }

    public void relayout() {
        textActor.setPosition(getWidth() / 2 - textActor.getWidth() / 2, 0);
    }

    public interface SelectedCallback {
		public void selected();
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
        setHeight(textActor.getHeight());
        if (buttonWidth == -1) {
            ninePatch.draw(batch, getX(), getY(), textActor.getWidth() + 20, 50);
        } else {
            ninePatch.draw(batch, getX(), getY(), buttonWidth, 50);
        }
        super.draw(batch, parentAlpha);
    }

//    public BitmapFont.TextBounds getBounds() {
//        BitmapFont.TextBounds bounds = textActor.getBounds();
//        bounds.height += (bounds.height * .50f);
//        return bounds;
//        return null;
//    }



}
