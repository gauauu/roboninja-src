package net.tolberts.android.roboninja.screens.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.screens.GameScreen;


/**
 * @deprecated. I decided I liked the VerticalThresholdFollower better
 */
public class StrictFollow implements CameraUpdater {
    @Override
    public void updateCamera(OrthographicCamera camera, GameScreen gameScreen, float delta) {
        MainCharacter mc = gameScreen.gameState.mc;

        camera.position.x = mc.bounds.x;
        camera.position.y = mc.bounds.y;

    }
}
