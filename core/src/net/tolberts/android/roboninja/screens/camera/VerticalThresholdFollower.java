package net.tolberts.android.roboninja.screens.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.screens.GameScreen;

/**
 * The camera updater I decided to use...
 */
public class VerticalThresholdFollower implements CameraUpdater {

    public final static float allowedOffset = 3.0f;


    @Override
    public void updateCamera(OrthographicCamera camera, GameScreen gameScreen, float delta) {

        MainCharacter mc = gameScreen.gameState.mc;

        camera.position.x = mc.bounds.x;

        camera.position.y = Math.min(camera.position.y, mc.bounds.y + allowedOffset);
        camera.position.y = Math.max(camera.position.y, mc.bounds.y - allowedOffset);

        //don't scroll past the sides
        if (camera.position.x - RoboNinjaGame.HALF_WIDTH_IN_TILES < 0) {
            camera.position.x = RoboNinjaGame.HALF_WIDTH_IN_TILES;
        }
        if (camera.position.x > gameScreen.currentLevel.getWidth() - RoboNinjaGame.HALF_WIDTH_IN_TILES) {
            camera.position.x = gameScreen.currentLevel.getWidth() - RoboNinjaGame.HALF_WIDTH_IN_TILES;
        }

        //don't scroll past the top or bottom
        if (camera.position.y - RoboNinjaGame.HALF_HEIGHT_IN_TILES < 0) {
            camera.position.y = RoboNinjaGame.HALF_HEIGHT_IN_TILES;
        }
        if (camera.position.y > gameScreen.currentLevel.getHeight() - RoboNinjaGame.HALF_HEIGHT_IN_TILES) {
            camera.position.y = gameScreen.currentLevel.getHeight() - RoboNinjaGame.HALF_HEIGHT_IN_TILES;
        }

    }
}
