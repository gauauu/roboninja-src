package net.tolberts.android.roboninja.screens.camera;

import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.screens.GameScreen;

import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * @deprecated decided I liked the VerticalThresholdFollower better
 */
public class MainCharFollow implements CameraUpdater {

	public final static float slowPerFrame = 2.71f;
	public final static float mediumPerFrame = 10.71f;
	public final static float fastPerFrame = 20.71f;
	public final static float verticalOffset = 7.0f;

	@Override
	public void updateCamera(OrthographicCamera camera, GameScreen gameScreen, float delta) {

		MainCharacter mc = gameScreen.gameState.mc;

		camera.position.x = mc.bounds.x;

		float ydiff = camera.position.y - mc.bounds.y - verticalOffset;
		//float abs = Math.abs(ydiff);

		int state = mc.state;

		if (state == MainCharacter.STATE_RUNNING) {
			if (ydiff > -0.5f) {
				camera.position.y += movementTowards(ydiff, delta, slowPerFrame);
			} else if (ydiff < -4) {
				camera.position.y += movementTowards(ydiff, delta, slowPerFrame);
			}
			/*
			 * if (abs > 10) { camera.position.y += movementTowards(ydiff,
			 * delta, fastPerFrame * 3); } else if (abs > 1) { camera.position.y
			 * += movementTowards(ydiff, delta, slowPerFrame); }
			 */
		}

		
		float adjustRate = 0;
		
		if (ydiff > 1.0f) {
			adjustRate = fastPerFrame;
		}
		if (ydiff < -8) {
			adjustRate = fastPerFrame;
		}
		
		camera.position.y += movementTowards(ydiff, delta, adjustRate);
		
		//don't scroll past the sides
		if (camera.position.x - RoboNinjaGame.HALF_WIDTH_IN_TILES < 0) {
			camera.position.x = RoboNinjaGame.HALF_WIDTH_IN_TILES;
		}
		if (camera.position.x > gameScreen.currentLevel.getWidth() - RoboNinjaGame.HALF_WIDTH_IN_TILES) {
			camera.position.x = gameScreen.currentLevel.getWidth() - RoboNinjaGame.HALF_WIDTH_IN_TILES;
		}
		
		//don't scroll past the top or bottom
		if (camera.position.y - RoboNinjaGame.HALF_HEIGHT_IN_TILES < 0) {
			camera.position.y = RoboNinjaGame.HALF_HEIGHT_IN_TILES;
		}
		if (camera.position.y > gameScreen.currentLevel.getHeight() - RoboNinjaGame.HALF_HEIGHT_IN_TILES) {
			camera.position.y = gameScreen.currentLevel.getHeight() - RoboNinjaGame.HALF_HEIGHT_IN_TILES;
		}


	}

	private float movementTowards(float ydiff, float delta, float perFrame) {
		if (perFrame == 0) {
			return 0;
		}
		float movement = delta * perFrame;
		movement = Math.min(movement, Math.abs(ydiff));
		movement *= -Math.signum(ydiff);
		return movement;
	}

}
