package net.tolberts.android.roboninja.screens.camera;

import net.tolberts.android.roboninja.screens.GameScreen;

import com.badlogic.gdx.graphics.OrthographicCamera;

public interface CameraUpdater {

	public void updateCamera(OrthographicCamera camera, GameScreen gameScreen, float delta);

}
