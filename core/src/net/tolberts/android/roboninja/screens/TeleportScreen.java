package net.tolberts.android.roboninja.screens;

import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.TeleportLocation;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.hud.HudTapListener;
import net.tolberts.android.roboninja.minimap.MinimapTracker;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.MenuButtonActor;
import net.tolberts.android.roboninja.screens.actors.SmallButton;
import net.tolberts.android.roboninja.screens.actors.TextActor;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class TeleportScreen extends MinimapScreen {


    private static final float SELECTOR_HEIGHT = 70;
    private static final int SELECTOR_WIDTH = 60;
    private static final float SELECTOR_POS_Y = 100;


    private TextActor title;
    private TeleportLocation selected;


    public TeleportScreen(GameState state) {
        super(state);
    }

    @Override
    protected void initializeMenuScreen() {
        super.initializeMenuScreen();
//        closeLabel.setText("Cancel");
//        closeLabelX = 45;


        ActorFrame titleFrame = new ActorFrame();
        titleFrame.setStyle(ActorFrame.STYLE_GREEN_FRAME);

        titleFrame.setBounds(-10, stage.getHeight() - 60, stage.getWidth() + 20, 80);
        stage.addActor(titleFrame);


        title = new TextActor("Teleport to...", Fonts.HEADER_FONT);
        title.setFontScale(Fonts.FONT_SCALE_SMALL);
        title.centerAt(RoboNinjaGame.WIDTH / 2, 0);
        title.setY(stage.getHeight() - 40);
        title.setScale(0.75f);
        stage.addActor(title);

        SmallButton rightArrow = new SmallButton("ico_forward", new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                selectNextTeleportLocation();
            }
        });
        stage.addActor(rightArrow);
        rightArrow.setPosition(RoboNinjaGame.WIDTH - rightArrow.getWidth() - 1, RoboNinjaGame.HEIGHT / 3);
        rightArrow.setIconScale(0.8f);
        rightArrow.setIconOffset(45, 25);


        SmallButton leftArrow = new SmallButton("ico_back", new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                selectPrevTeleportLocation();
            }
        });
        stage.addActor(leftArrow);
        leftArrow.setPosition(0, RoboNinjaGame.HEIGHT / 3);
        leftArrow.setIconScale(0.8f);
        leftArrow.setIconOffset(38, 25);


        MenuButtonActor teleportButton = new MenuButtonActor("Teleport", new HudCallback() {
            @Override
            public void callback() {
                teleportToSelected();
                if (onClose != null) {
                    onClose.callback();
                }
                gameState.game.setScreen(previousScreen);
            }


        }, 250);
        stage.addActor(teleportButton);
        teleportButton.layout();
        teleportButton.setPosition(RoboNinjaGame.WIDTH / 2 - teleportButton.getWidth() / 2, 10);


//        ActorFrame goFrame = new ActorFrame();
//        goFrame.setBounds(-60, -200, 200, 250);
//        stage.addActor(goFrame);
//
//        TextActor goLabel = new TextActor("Teleport", Fonts.SMALL_FONT);
//        goLabel.setFontScale(Fonts.FONT_SCALE_SMALL);
//        goLabel.setPosition(95, 220);
//        goFrame.addListener(new HudTapListener() {
//            @Override
//            public void onTap(float x, float y) {
//                teleportToSelected();
//                if (onClose != null) {
//                    onClose.callback();
//                }
//                gameState.game.setScreen(previousScreen);
//            }
//        });
//        goFrame.addActor(goLabel);


        selectTeleportLocation(((RoboNinjaGameState) gameState).currentTeleportLocation);

    }

    private void teleportToSelected() {
        ((RoboNinjaGameState) gameState).teleportTo(selected);
    }

    private LinkedHashSet<TeleportLocation> getTeleportLocations() {
        return ((RoboNinjaGameState) gameState).getProgress().getTeleportOptions();
    }

    private List<TeleportLocation> getTeleportLocationsAsList() {
        LinkedHashSet<TeleportLocation> teleportLocations = getTeleportLocations();
        ArrayList<TeleportLocation> asList = new ArrayList<TeleportLocation>(teleportLocations.size());
        asList.addAll(teleportLocations);
        return asList;
    }

    private void selectPrevTeleportLocation() {
        List<TeleportLocation> locs = getTeleportLocationsAsList();
        int i = locs.indexOf(selected);
        if (i < 1) {
            selectTeleportLocation(locs.get(locs.size() - 1));
        } else {
            selectTeleportLocation(locs.get(i - 1));
        }
    }

    private void selectNextTeleportLocation() {
        List<TeleportLocation> locs = getTeleportLocationsAsList();
        int i = locs.indexOf(selected);
        if (i == locs.size() - 1) {
            selectTeleportLocation(locs.get(0));
        } else {
            selectTeleportLocation(locs.get(i + 1));
        }
    }

    private void selectTeleportLocation(TeleportLocation selected) {
//        AudioPlayer.playSound(Audio.UI_CLICK);
        this.selected = selected;
        centerMapOnSelected();

    }

    private void centerMapOnSelected() {
        minimap.setScale(1.0f);
        float x = RoboNinjaGame.WIDTH / 2 - selected.minimapX * MinimapTracker.SCALE;
        float y = RoboNinjaGame.HEIGHT / 2 + selected.minimapY * MinimapTracker.SCALE - minimap.getHeight();
        minimap.setPosition(x, y);
    }
}
