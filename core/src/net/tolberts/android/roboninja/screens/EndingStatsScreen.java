package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.AsyncLoader;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.platformadapters.Achievement;
import net.tolberts.android.game.platformadapters.Leaderboard;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.game.util.MiscUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.MenuButtonActor;
import net.tolberts.android.roboninja.screens.actors.RoboNinjaStage;
import net.tolberts.android.roboninja.screens.actors.TextActor;


public class EndingStatsScreen extends BaseScreen implements PlayServicesAdapter.AdapterStringCallback {

    private final RoboNinjaStage stage;
    private final Texture background;
    private final SpriteBatch batch;
    private final ActorFrame frame;
    private MenuButtonActor submitButton;
    private final PlayServicesAdapter adapter;
    private TextActor leaderBoardStatus;

    public final static int MARGIN_X = 10;
    public final static int MARGIN_Y = 10;
    private float runningY;
    private final float xPosition;

    int leaderboardFinishedSubmitting = 0;


    public EndingStatsScreen(RoboNinjaGameState roboNinjaGameState) {
        super(roboNinjaGameState);

        stage = new RoboNinjaStage();
        Gdx.input.setInputProcessor(stage);
        background = Art.getTexture("titlebg");
        batch = getSimpleScreenBatch();
        /*
        menu = new MenuActor(){
            @Override
            protected float getStartingTop() {
                return getMenuTop();
            }
        };
        menu.setPosition(0, 150);
        menu.setWidth(RoboNinjaGame.WIDTH);
        menu.setChildFontScale(0.75f);
        menu.setDefaultFont(Fonts.SMALL_FONT);
        */

        frame = new ActorFrame();
        frame.setPosition(MARGIN_X, MARGIN_Y);
        frame.setWidth(RoboNinjaGame.WIDTH - MARGIN_X * 2);
        frame.setHeight(RoboNinjaGame.HEIGHT - MARGIN_Y * 2);
        stage.addActor(frame);
        frame.setStyle(ActorFrame.STYLE_NONE);
//        stage.addActor(menu);


        xPosition = 40;
        runningY = 50;

        TextActor header = new TextActor("Your Results:", Fonts.HEADER_FONT);
        header.setPosition(xPosition, frame.getHeight() - 55);
        frame.addActor(header);



        addLabel(getDeathsLabel());
        addLabel(getTimeLabel());
        addLabel(getAverageLabel());


        TextActor deathsLabel = new TextActor(getDeathsLabel(), Fonts.SMALL_FONT);
        deathsLabel.setPosition(xPosition, frame.getHeight() - 70);

        leaderboardFinishedSubmitting = 0;
        adapter = ((RoboNinjaGame) gameState.game).platformAdapter.getPlayServicesAdapter();
        if (adapter != null && adapter.isEnabled() && (gameState.speedMode != GameState.SPEED_MODE_EASY)) {
            runningY += 30;
            submitButton = new MenuButtonActor("Post to Leaderboard", new HudCallback() {
                @Override
                public void callback() {
                    submitScore(adapter);
                    submitButton.setVisible(false);
                    leaderBoardStatus.setText("Submitting...");
                    leaderBoardStatus.centerAt(frame.getWidth() / 2, 80);

                }
            }, 100);
            submitButton.setPosition(xPosition, frame.getHeight() - runningY);
            frame.addActor(submitButton);
            submitButton.setWidth(450);
            submitButton.setPosition(frame.getWidth() / 2 - 225, 75);
            runningY += 30;

            leaderBoardStatus = new TextActor("", Fonts.SMALL_FONT);
            leaderBoardStatus.setFontScale(Fonts.FONT_SCALE_TINY);
            frame.addActor(leaderBoardStatus);
            leaderBoardStatus.centerAt(frame.getWidth() / 2, 80);

        }

        runningY += 30;
        MenuButtonActor backButton = new MenuButtonActor("Exit", new HudCallback() {
            @Override
            public void callback() {
                backToMainMenu();
            }
        }, 50);
        backButton.setWidth(200);
        frame.addActor(backButton);
        backButton.setPosition(frame.getWidth() / 2 - 100, 0);

    }


    private void addLabel(String text) {

        TextActor label = new TextActor(text, Fonts.SMALL_FONT);
        label.setPosition(xPosition, frame.getHeight() - runningY);
        runningY += 30;
        frame.addActor(label);
        label.centerAt(label.getParent().getWidth() / 2, frame.getHeight() - runningY);
    }

    private int getDeaths(){
        return ((RoboNinjaProgress)gameState.gameProgress).getNumDeaths();
    }

    private float getTimeSeconds()
    {
        return ((RoboNinjaProgress)gameState.gameProgress).getPlayTime();
    }

    private String getDeathsLabel() {
        return "Deaths: " + getDeaths();
    }

    private String getTimeLabel() {
        String timeStr = MiscUtils.getTimeString(getTimeSeconds());
        String suffix = "";
        if (gameState.speedMode == GameState.SPEED_MODE_FAST) {
            suffix = " (fast)";
        }
        return "Time: " + timeStr + suffix;
    }

    private String getAverageLabel() {
        float seconds = getTimeSeconds();
        int deaths = getDeaths();

        float secondsPerDeath = seconds / (float)deaths;
        String formatted = String.format("%.1f", secondsPerDeath);
        String points = "";
        if (getDeaths() < 250) {
            points = points + "!";
            if (getDeaths() < 100) {
                points = points + "!";
                if (getDeaths() < 50) {
                    points = points + "!";
                    if (getDeaths() == 0) {
                        points = "!?";
                    }
                }
            }
        }
        return formatted + " seconds/death" + points;
    }


    @Override
    public void render(float delta) {

        AsyncLoader.update();
        AudioPlayer.update(delta);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(background, 0, -100);
        batch.end();

        stage.draw();
        stage.act();

    }

    private void submitScore(PlayServicesAdapter adapter) {
        int speedMode = gameState.gameProgress.getSpeedMode();
        if (speedMode == GameState.SPEED_MODE_NORMAL) {
            adapter.submitLeaderboardScore(Leaderboard.DEATHS, getDeaths(), this);
            adapter.submitLeaderboardScore(Leaderboard.TIME, (int) (getTimeSeconds() * 1000), this);
        } else {
            adapter.submitLeaderboardScore(Leaderboard.DEATHS_HARD, getDeaths(), this);
            adapter.submitLeaderboardScore(Leaderboard.TIME_HARD, (int) (getTimeSeconds() * 1000), this);
        }
    }

    private void backToMainMenu() {
        setScreen(new MainMenuScreen(gameState));
    }

    @Override
    public void show() {

        super.show();

        Gdx.input.setInputProcessor(stage);

        unlockEndingAchievements();
    }

    private void unlockEndingAchievements() {
        if (gameState.speedMode != GameState.SPEED_MODE_EASY) {
            PlayServicesAdapter playServicesAdapter = ((RoboNinjaGame) gameState.game).platformAdapter.getPlayServicesAdapter();
            if (playServicesAdapter != null && playServicesAdapter.isLoggedIn()) {
                playServicesAdapter.unlockAchievement(Achievement.COMPLETE);

                for (int completionAchievement : Achievement.COMPLETION_ACHIEVEMENTS) {
                    if (getDeaths() <= completionAchievement) {
                        String achivementKey = Achievement.COMPLETE + "_" + completionAchievement;
                        playServicesAdapter.unlockAchievement(achivementKey);
                    }
                }
            }
        }

    }

    @Override
    public void setResult(String result) {
        leaderboardFinishedSubmitting++;
        leaderBoardStatus.setText(leaderBoardStatus.getText() + "..");
        if (leaderboardFinishedSubmitting == 2) {
            leaderBoardStatus.setVisible(false);
            adapter.showAllLeaderboards();
        }
    }
}
