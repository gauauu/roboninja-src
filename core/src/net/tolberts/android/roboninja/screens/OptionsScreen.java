package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.screens.actors.MenuItemActor;

public class OptionsScreen extends MenuScreen {

    private BaseScreen backScreen;
    private Actor logoutButton;
    private Actor achievementsButton;
    private Actor playOnOffButton;
    private Actor loginButton;

    public OptionsScreen(GameState state) {
        super(state);
        setBackButtonCallback(new HudCallback() {
            @Override
            public void callback() {
                setScreen(backScreen);
            }
        });
    }

    @Override
    protected float getMenuTop() {
        return 95.0f;
    }

    @Override
    protected void handleInput() {

    }

    protected void renderMenu() {

    }

    protected void addMenuItems() {

        menu.setButtonWidth(420);
        menu.addOnOffMenuItem("Sound", AudioPlayer.PREF_AUDIO_ENABLED, true);
        final PlayServicesAdapter playServicesAdapter = ((RoboNinjaGame) gameState.game).platformAdapter.getPlayServicesAdapter();
        if (playServicesAdapter.isAvailable()) {
            if (playServicesAdapter.isLoggedIn()) {
                achievementsButton = menu.addMenuItem("Achievements/Boards", new MenuItemActor.SelectedCallback() {
                    @Override
                    public void selected() {
                        setScreen(new AchievementsBoardsScreen(OptionsScreen.this, gameState));
                    }
                });
                logoutButton = menu.addMenuItem("Sign Out of Google Play", new MenuItemActor.SelectedCallback() {
                    @Override
                    public void selected() {
                        playServicesAdapter.logOut();
                        removeLogoutButton();
                    }
                });
            } else {
                playOnOffButton = menu.addOnOffMenuItem("Google Play", PlayServicesAdapter.ALLOW_PLAY_SERVICES, false);
                loginButton = menu.addMenuItem("Log into Google Play", new MenuItemActor.SelectedCallback() {
                    @Override
                    public void selected() {
                        playServicesAdapter.attemptLogin();
                        removeLoginButtons();
                    }
                });
            }
        }
        menu.addMenuItem("Credits / Licenses", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                setScreen(new CreditsScreen(OptionsScreen.this, gameState));
            }
        });

        menu.addMenuItem("Back", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                setScreen(backScreen);
            }
        });
    }

    private void removeLoginButtons() {
        if (playOnOffButton != null) {
            playOnOffButton.remove();
        }
        if (loginButton != null) {
            loginButton.remove();
        }
    }

    private void removeLogoutButton() {
        if (logoutButton != null) {
            logoutButton.remove();
        }
        if (achievementsButton != null) {
            achievementsButton.remove();
        }
        menu.reLayout();
    }

    public void setBackScreen(MainMenuScreen mainMenuScreen) {
        this.backScreen = mainMenuScreen;
    }

}
