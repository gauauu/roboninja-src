package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.*;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.hud.HudTapListener;
import net.tolberts.android.roboninja.screens.actors.*;

public class CreditsScreen extends BaseScreen {

    public Stage stage;
    public Texture background;
    protected TextActor closeLabel;
    protected HudCallback onClose;
    protected int closeLabelX;
    protected Screen previousScreen;

    private InputMultiplexer inputMultiplexer;
    private ActorFrame closeFrame;
    private SpriteBatch batch;
    private ScrollingTextActor scrollingTextActor;

    private SmallButton backButton;


    public CreditsScreen(BaseScreen backScreen, GameState gameState) {
        super(gameState);
        previousScreen = backScreen;
        initScreen();
    }

    private void initScreen() {

        batch = getSimpleScreenBatch();

        stage = new RoboNinjaStage();
        Gdx.input.setInputProcessor(stage);


        background = Art.getTexture("titlebg");

        backButton = SmallButton.NewBackButton(new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                AudioPlayer.playSound(Audio.UI_CLICK);
                setScreen(previousScreen);
            }
        });
        backButton.setPosition(10, 10);
        stage.addActor(backButton);


        inputMultiplexer = new InputMultiplexer();


        /*
        closeFrame = new ActorFrame();
        closeLabel = new TextActor("Back", Fonts.SMALL_FONT);
        closeLabel.setFontScale(Fonts.FONT_SCALE_TINY);
        stage.addActor(closeFrame);
        closeFrame.addActor(closeLabel);
        closeFrame.setZIndex(100);
        closeFrame.addListener(new HudTapListener() {

            @Override
            public void onTap(float x, float y) {
                AudioPlayer.playSound(Audio.UI_CLICK);
                setScreen(previousScreen);
            }
        });

        closeFrame.setBounds(RoboNinjaGame.WIDTH - 90, -200, 200, 250);
        closeLabel.setPosition(RoboNinjaGame.WIDTH - 90, -22);
        closeLabelX = 30;
        */


        ActorFrame textFrame = new ActorFrame();
        textFrame.setBounds(- 50, 80, RoboNinjaGame.WIDTH + 100, RoboNinjaGame.HEIGHT - 30);
        stage.addActor(textFrame);

        scrollingTextActor = new ScrollingTextActor(TextData.loadTextLines("credits"), Fonts.SMALL_FONT, 2.0f);
        scrollingTextActor.setPosition(0, 0);
        scrollingTextActor.setHeight(RoboNinjaGame.HEIGHT);
        scrollingTextActor.setWidth(RoboNinjaGame.WIDTH);
        scrollingTextActor.setLeftMargin(60);
        scrollingTextActor.setMaxScrollPosition(90);
        scrollingTextActor.setScrollPosition(90);
        scrollingTextActor.setFontScale(Fonts.FONT_SCALE_TEENY_TINY);

        textFrame.addActor(scrollingTextActor);

        inputMultiplexer.addProcessor(stage);
        scrollingTextActor.setupInput(inputMultiplexer);
        Gdx.input.setInputProcessor(inputMultiplexer);

//        stage.addActor(positionIndicator);

    }

    @Override
    public void render(float delta) {
        AsyncLoader.update();
        AudioPlayer.update(delta);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.draw(background, 0, 0, RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT);
                batch.end();

//        closeFrame.setBounds(RoboNinjaGame.WIDTH - 100, -200, 200, 250);
//        closeLabel.setPosition(closeLabelX, 220);


        stage.draw();
        stage.act();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
