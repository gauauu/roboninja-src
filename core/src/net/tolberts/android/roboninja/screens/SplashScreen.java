package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GamePrefs;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.game.util.GfxUtils;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class SplashScreen extends BaseScreen {

    private static final float SHOW_CHILI_WHOLE = 1.5f;
    private static final float START_FADEOUT = 3.5f;
    private static final float TOTAL_TIME = 4.0f;
//    private static final int CHILI_PX_HEIGHT = 128;
//    private static final int CHILI_PX_WIDTH = 256;
    private static final int CHILI_PX_HEIGHT = 384;
    private static final int CHILI_PX_WIDTH = 480;
    private static final String HAS_SEEN_PLAY_MENU = "hasSeenPlayMenu";

    float tick = 0;
    private SpriteBatch batch;
    private TextureRegion chiliWhole;
    private TextureRegion chiliBitten;
    private boolean playedSound = false;
    private float CHILI_X = 65;

    public SplashScreen(GameState state) {
        super(state);
    }

    @Override
    public void show() {
        Texture texture = Art.getTexture("chili-hi");
        TextureRegion[][] parts = TextureRegion.split(texture, CHILI_PX_WIDTH, CHILI_PX_HEIGHT);
        chiliWhole = parts[0][0];
        chiliBitten = parts[1][0];

        batch = new SpriteBatch();
        batch.getProjectionMatrix().setToOrtho2D(0, 0, RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT);

    }

    @Override
    public void render(float delta) {
        tick += Math.min(delta, 0.25f);
        handleInput();
        render();
        if (tick > TOTAL_TIME) {
            nextScreen();
        }
    }

    private void render() {
        // AsyncLoader.getAssetManager().update();
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();

        if (tick < SHOW_CHILI_WHOLE) {
            renderChiliWhole();
        } else {
            if (!playedSound) {
                playBiteSound();
            }
            renderChiliBitten();
        }
        batch.end();
    }

    private void nextScreen() {
        final PlayServicesAdapter playServicesAdapter = ((RoboNinjaGame) gameState.game).platformAdapter.getPlayServicesAdapter();
        final boolean hasSeenMenu = GamePrefs.getBool(HAS_SEEN_PLAY_MENU, false);
        if (playServicesAdapter != null && playServicesAdapter.isAvailable() && !hasSeenMenu) {
            GamePrefs.setBool(HAS_SEEN_PLAY_MENU, true);
            setScreen(new ChooseIfPlayIntegrationScreen(gameState));
        } else {
            setScreen(new MainMenuScreen(gameState));
        }
    }

    private void handleInput() {
        if (Input.wasSpacebarOrScreenTouch()) {
            nextScreen();
        }
    }

    private void renderChiliBitten() {
        if (tick > START_FADEOUT) {
            float fadeDuration = TOTAL_TIME - START_FADEOUT;
            float fadeTimeSoFar = tick - START_FADEOUT;
            float alpha = Math.max(0, 1.0f - (fadeTimeSoFar / fadeDuration));
            GfxUtils.setBatchAlpha(batch, alpha);
        }
        batch.draw(chiliBitten, CHILI_X, 20, 0, 0, CHILI_PX_WIDTH, CHILI_PX_HEIGHT, 0.9f, 0.9f, 0);
        GfxUtils.setBatchAlpha(batch, 1.0f);
    }

    private void playBiteSound() {
        AudioPlayer.playSound(Audio.CHILI_BITE);
        playedSound = true;
    }

    private void renderChiliWhole() {
        batch.draw(chiliWhole, CHILI_X, 20, 0, 0, CHILI_PX_WIDTH, CHILI_PX_HEIGHT, 0.9f, 0.9f, 0);
    }

}
