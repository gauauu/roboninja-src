package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.AsyncLoader;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.hud.GeneralBackButtonHandler;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.MenuActor;
import net.tolberts.android.roboninja.screens.actors.RoboNinjaStage;

public abstract class MenuScreen extends BaseScreen {

	public Stage stage;
	public Texture background;
	public SpriteBatch batch;
	public MenuActor menu;
	public float tick;
	
	public final static int MARGIN_X = 25;
	public final static int MARGIN_Y = 25;
    protected ActorFrame frame;
    private float delta;
	private GeneralBackButtonHandler backHandler;
	private InputMultiplexer inputMultiplexer;

	public MenuScreen(GameState state) {
		super(state);
		initializeMenuScreen();

	}

    protected float getMenuTop() {
        return 0;
    }


	protected void setBackButtonCallback(HudCallback callback) {
		backHandler.setCallback(callback);
	}

	private void initializeMenuScreen() {

        stage = new RoboNinjaStage();


		backHandler = new GeneralBackButtonHandler(null);
		inputMultiplexer = new InputMultiplexer(stage, backHandler);
		Gdx.input.setInputProcessor(inputMultiplexer);
//		background = Art.getTexture("menu-background");
		background = Art.getTexture("titlebg");
		batch = getSimpleScreenBatch();
		menu = new MenuActor(){
            @Override
            protected float getStartingTop() {
                return getMenuTop();
            }
        };
		menu.setPosition(0, 150);
		menu.setWidth(RoboNinjaGame.WIDTH);
		menu.setChildFontScale(Fonts.FONT_SCALE_HEADER_BUTTONS);
		menu.setDefaultFont(Fonts.HEADER_FONT);
        menu.setButtonWidth(200);

		frame = new ActorFrame();
//		frame.setPosition(MARGIN_X, MARGIN_Y);
//		frame.setWidth(RoboNinjaGame.WIDTH - MARGIN_X * 2);
//		frame.setHeight(RoboNinjaGame.HEIGHT - MARGIN_Y * 2);
//		stage.addActor(frame);
		stage.addActor(menu);
//        group.setPosition(-RoboNinjaGame.WIDTH / 2, -RoboNinjaGame.HEIGHT / 2);


		addMenuItems();

		buildScreen();

	}

    protected void buildScreen() {

    }



	@Override
	public void show() {
		super.show();
		Gdx.input.setInputProcessor(inputMultiplexer);
	}

	@Override
	public void render(float delta) {
		tick += Math.min(0.10, delta);
        this.delta = delta;
		handleInput();
		AsyncLoader.update();
        AudioPlayer.update(delta);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        drawMovingBackground();
		renderMenu();

		stage.draw();
		stage.act();
        renderAboveFrame();
	}

    private float getBackgroundX() {
        float localTick = tick * 60;
        int pixels = 1280 - RoboNinjaGame.WIDTH;
        float positionRaw = localTick % pixels;
        if ((localTick % (pixels * 2)) > pixels) {
            positionRaw = pixels - positionRaw;
        }
        return -positionRaw;
    }

    protected void drawMovingBackground() {
        batch.begin();
//        batch.draw(background, getBackgroundX(), -100);
        batch.draw(background, 0, 0, RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT);
        batch.end();
    }


    protected void renderAboveFrame() {
        //do nothing in default implementation.
    }

    @Override
	public void hide() {
		super.hide();
		Gdx.input.setInputProcessor(null);
	}

	protected abstract void handleInput();

	protected abstract void renderMenu();

	protected abstract void addMenuItems();



}
