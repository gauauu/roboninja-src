package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Screen;
import net.tolberts.android.game.GameState;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.screens.actors.MenuItemActor;

public class AchievementsBoardsScreen extends MenuScreen {

    private final OptionsScreen backScreen;

    public AchievementsBoardsScreen(final OptionsScreen optionsScreen, GameState gameState) {
        super(gameState);
        this.backScreen = optionsScreen;
        setBackButtonCallback(new HudCallback() {
            @Override
            public void callback() {
                setScreen(optionsScreen);
            }
        });

    }

    @Override
    protected void handleInput() {

    }

    @Override
    protected void renderMenu() {

    }

    @Override
    protected float getMenuTop() {
        return 60.0f;
    }

    @Override
    protected void addMenuItems() {

        menu.setButtonWidth(420);
        menu.addMenuItem("View Achievements", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                viewAchievements();
            }
        });
        menu.addMenuItem("View Leaderboards", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                viewLeaderboards();
            }
        });

        menu.addMenuItem("Back", new MenuItemActor.SelectedCallback() {
            @Override
            public void selected() {
                setScreen(backScreen);
            }
        });

    }

    private void viewLeaderboards() {
        ((RoboNinjaGame)gameState.game).platformAdapter.getPlayServicesAdapter().showAllLeaderboards();
    }

    private void viewAchievements() {
        ((RoboNinjaGame)gameState.game).platformAdapter.getPlayServicesAdapter().showAchievements();
    }
}
