package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.AsyncLoader;
import net.tolberts.android.game.loaders.Audio;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.hud.HudCallback;
import net.tolberts.android.roboninja.hud.HudTapListener;
import net.tolberts.android.roboninja.minimap.MinimapTracker;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.MinimapPositionActor;
import net.tolberts.android.roboninja.screens.actors.RoboNinjaStage;
import net.tolberts.android.roboninja.screens.actors.SmallButton;

public class MinimapScreen extends BaseScreen {

    public Stage stage;
    public Texture background;
    public Image minimap;
    public SpriteBatch batch;
    public float tick;

//    protected TextActor closeLabel;
    protected HudCallback onClose;
    protected int closeLabelX;
    protected Screen previousScreen;

    private InputMultiplexer inputMultiplexer;
    private ActorFrame closeFrame;
    private MinimapPositionActor positionIndicator;
    private MinimapTracker minimapTracker;
    private Texture asTexture;

    //this is an ugly way to do this. But I'm not yet ready to refactor the onClose listener to properly
    //indicate cancellation. If/When I do, this needs to go.
    public boolean wasCanceled = false;
    private SmallButton backButton;


    public MinimapScreen(GameState state) {
        super(state);
        initializeMenuScreen();
    }

    protected void initializeMenuScreen() {
        wasCanceled = false;
        stage = new RoboNinjaStage();
        Gdx.input.setInputProcessor(stage);
        background = Art.getTexture("menu-background");

        minimapTracker = ((RoboNinjaGameState) gameState).getProgress().minimapTracker;
        asTexture = minimapTracker.getAsTexture();
        minimap = new Image(asTexture);
        batch = getSimpleScreenBatch();
        inputMultiplexer = new InputMultiplexer();

        stage.addActor(minimap);


        float y = (minimapTracker.currentPosition.y * MinimapTracker.SCALE) - minimap.getHeight();
        y += RoboNinjaGame.HEIGHT / 2;
        float x = -minimapTracker.currentPosition.x * MinimapTracker.SCALE;
        x += RoboNinjaGame.WIDTH / 2;
        minimap.setPosition(x, y);


//        closeFrame = new ActorFrame();
//        closeLabel = new TextActor("Close Minimap", Fonts.SMALL_FONT);
//        closeLabel.setFontScale(Fonts.FONT_SCALE_SMALL);
//        stage.addActor(closeFrame);
//        closeFrame.addActor(closeLabel);
//        closeFrame.setZIndex(100);
//        closeFrame.addListener(new HudTapListener() {
//
//            @Override
//            public void onTap(float x, float y) {
//                wasCanceled = true; //bleh. This is here because TeleportScreen reuses functionality from this screen.
//                                    //and reuses the close button, but there I need to detect if it was canceled.
//                                    //This should be refactored if/when I get around to it. :-/
//                AudioPlayer.playSound(Audio.UI_CLICK);
//                if (onClose != null) {
//                    onClose.callback();
//                }
//                gameState.game.setScreen(previousScreen);
//            }
//        });
//
//        closeFrame.setBounds(RoboNinjaGame.WIDTH - 140, -200, 200, 250);
//        closeLabel.setPosition(RoboNinjaGame.WIDTH - 120, -22);
//        closeLabelX = 30;
        backButton = SmallButton.NewBackButton(new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                wasCanceled = true; //bleh. This is here because TeleportScreen reuses functionality from this screen.
                                    //and reuses the close button, but there I need to detect if it was canceled.
                                    //This should be refactored if/when I get around to it. :-/
                AudioPlayer.playSound(Audio.UI_CLICK);
                if (onClose != null) {
                    onClose.callback();
                }
                gameState.game.setScreen(previousScreen);

            }
        });
        backButton.setPosition(RoboNinjaGame.WIDTH - backButton.getWidth(), 0);
        stage.addActor(backButton);



        positionIndicator = new MinimapPositionActor();
        setPositionIndicatorPosition();

        stage.addActor(positionIndicator);

    }

    private void setPositionIndicatorPosition() {
        float x = (minimapTracker.currentPosition.x * MinimapTracker.SCALE * minimap.getScaleX()) + minimap.getX();
        float y = minimapTracker.currentPosition.y * MinimapTracker.SCALE;
        y = (minimap.getHeight() - y) * minimap.getScaleY();
        y += minimap.getY();
        //float y = ((minimapTracker.currentPosition.y * MinimapTracker.SCALE - minimap.getHeight()) * minimap.getScaleY()) + minimap.getY();
        positionIndicator.centerOn(x, y);


    }

    public void setOnClose(HudCallback onClose) {
        this.onClose = onClose;
    }

    @Override
    public void show() {
        super.show();


        inputMultiplexer.addProcessor(new GestureDetector(new GestureListener() {

            float lastZoomDistance = 0;
            float originalZoomDistance = 0;

            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                float panX = deltaX * RoboNinjaGame.ACTUAL_RATIO_X;
                float panY = deltaY * RoboNinjaGame.ACTUAL_RATIO_Y;
                minimap.setPosition(minimap.getX() + panX, minimap.getY() - panY);
                positionIndicator.setPosition(positionIndicator.getX() + panX, positionIndicator.getY() - panY);
                return true;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                //we track the last distance, so we scale an amount based on that.
                //If the initialDistance changes, we know it's a new zoom gesture, so start over.

                if (initialDistance != originalZoomDistance) {
                    originalZoomDistance = initialDistance;
                    lastZoomDistance = distance;
                }
                float moved = lastZoomDistance - distance;
                changeScale(-moved / 1000);
                lastZoomDistance = distance;


                return true;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }
        }));
        inputMultiplexer.addProcessor(new InputProcessor() {

            @Override
            public boolean keyDown(int keycode) {
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                changeScale(-(((float) amount) / 10));
                return true;
            }
        });
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }


    private void changeScale(float changeBy) {
        //first record where the center is relative to the current scale
        float centerXPct = (-minimap.getX() + RoboNinjaGame.WIDTH / 2) / (minimap.getWidth() * minimap.getScaleX());
        float centerYPct = (-minimap.getY() + RoboNinjaGame.HEIGHT / 2) / (minimap.getHeight() * minimap.getScaleY());


        //then actually scale
        float oldScale = minimap.getScaleX();
        float newScale = minimap.getScaleX() + changeBy;
        if (newScale < .1) {
            newScale = oldScale;
        }
        if (newScale > 3) {
            newScale = oldScale;
        }
        minimap.setScale(newScale);

        //then reposition to recenter it based on current scale
        float newX = -(centerXPct * (minimap.getWidth() * minimap.getScaleX()) - (RoboNinjaGame.WIDTH / 2));
        float newY = -(centerYPct * (minimap.getHeight() * minimap.getScaleY()) - (RoboNinjaGame.HEIGHT / 2));
        minimap.setPosition(newX, newY);


    }

    @Override
    public void render(float delta) {
        tick += Math.min(0.10, delta);
        handleInput();
        AsyncLoader.update();
        AudioPlayer.update(delta);

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.draw(background, -100, -100);
        //batch.draw(minimap, 0, 0);
        batch.end();

//        closeFrame.setBounds(RoboNinjaGame.WIDTH - 170, -200, 200, 250);
//
//        closeLabel.setPosition(closeLabelX, 220);

        setPositionIndicatorPosition();


        stage.draw();
        stage.act();
    }

    private void handleInput() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        super.hide();
        Gdx.input.setInputProcessor(null);
        asTexture.dispose();

    }

    public void setPreviousScreen(Screen previousScreen) {
        this.previousScreen = previousScreen;

    }


}
