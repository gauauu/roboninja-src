package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.levels.Level;
import net.tolberts.android.game.levels.LevelFactory;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.loaders.Fonts;
import net.tolberts.android.game.parallax.ParallaxCamera;
import net.tolberts.android.game.parallax.RepeatingBackgroundRenderer;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.cutscene.Cutscene;
import net.tolberts.android.roboninja.cutscene.Puppet;
import net.tolberts.android.roboninja.cutscene.WordBubble;
import net.tolberts.android.roboninja.hud.HudTapListener;
import net.tolberts.android.roboninja.screens.actors.ActorFrame;
import net.tolberts.android.roboninja.screens.actors.RoboNinjaStage;
import net.tolberts.android.roboninja.screens.actors.SmallButton;
import net.tolberts.android.roboninja.screens.actors.TextActor;

//Hmmm, this is extremely similar to GameScreen, with a good bit of copy/pasted code. (BAD!)
// But enough different that I realized that extending GameScreen was a bad idea.
// I guess what that means is I really need to pull out a bunch of this functionality into some other
// class that these can be composed of. Ugh.
public class CutsceneScreen extends BaseScreen  {

    private final OrthogonalTiledMapRenderer renderer;
    private final RepeatingBackgroundRenderer bgRenderer;
    private final ParallaxCamera camera;
    private final OrthographicCamera bgCamera;
    private final RoboNinjaStage stage;
    private final WordBubble wordBubble;
    Cutscene cutscene;
    private Level currentLevel;
    private Texture bgImage;

    public CutsceneScreen(GameState gameState, final Cutscene cutscene) {
        super(gameState);

        this.cutscene = cutscene;

        GameSettings settings = GameSettings.getInstance();
        renderer = new OrthogonalTiledMapRenderer(null, settings.scale);
        bgRenderer = new RepeatingBackgroundRenderer(null, settings.scale);
        camera = new ParallaxCamera();
        camera.setToOrtho(false, settings.viewport.x, settings.viewport.y);
        camera.position.x = settings.viewport.x / 2;
        camera.position.y = settings.viewport.y / 2;
        camera.update();

        bgCamera = new OrthographicCamera();
        bgCamera.setToOrtho(false, settings.viewport.x, settings.viewport.y);
        bgCamera.position.x = settings.viewport.x / 2;
        bgCamera.position.y = settings.viewport.y / 2;
        bgCamera.update();

        stage = new RoboNinjaStage();

        wordBubble = new WordBubble();
        stage.addActor(wordBubble);
        wordBubble.addListener(new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                if (cutscene.showStory) {
                    cutscene.dismissStory = true;
                }
            }
        });
        wordBubble.setVisible(false);
        wordBubble.setBounds(0, 0, RoboNinjaGame.WIDTH, RoboNinjaGame.HEIGHT);

//        ActorFrame goFrame = new ActorFrame();
//        goFrame.setBounds(-60, -200, 150, 230);
//        stage.addActor(goFrame);
//
//        TextActor goLabel = new TextActor("Skip", Fonts.SMALL_FONT);
//        goLabel.setFontScale(Fonts.FONT_SCALE_SMALL);
//        goLabel.setPosition(85, 210);
//        goFrame.addListener(new HudTapListener() {
//            @Override
//            public void onTap(float x, float y) {
//                cutscene.skip();
//            }
//        });
//        goFrame.addActor(goLabel);

        SmallButton skipButton = SmallButton.NewBackButton(new HudTapListener() {
            @Override
            public void onTap(float x, float y) {
                cutscene.skip();
            }
        });
        skipButton.setPosition(7, 7);
        skipButton.setScale(0.8f);
        stage.addActor(skipButton);


        Gdx.input.setInputProcessor(stage);

        setLevel(LevelFactory.getLevel(cutscene.levelId));

    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(stage);
    }

    public void setLevel(Level level) {
        currentLevel = level;
        currentLevel.initializeRenderer(renderer, bgRenderer);
        bgImage = Art.getTexture(currentLevel.zone.backdrop);


        setCameraTo(cutscene.getCameraPosition());
    }

    public void setCameraTo(Vector2 position) {
        camera.position.x = position.x;
        camera.position.y = position.y;
    }


    @Override
    public void render(float delta) {
        cutscene.update(delta);
        setCameraTo(cutscene.getCameraPosition());
        AudioPlayer.update(delta);
        renderGame();
        stage.draw();
        stage.act();
        if (cutscene.showStory) {
            wordBubble.setText(cutscene.storyText);
            wordBubble.setVisible(true);
        }
        if (cutscene.dismissStory || (!cutscene.showStory)) {
            wordBubble.setVisible(false);
        }
    }


    public void renderGame() {

        Gdx.gl.glClearColor(currentLevel.backgroundColor.x, currentLevel.backgroundColor.y,
                currentLevel.backgroundColor.z, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        renderer.setView(bgCamera);
        Batch bgBatch = renderer.getSpriteBatch();
        bgBatch.begin();
        bgBatch.draw(bgImage, 0, 0, RoboNinjaGame.WIDTH_IN_TILES, RoboNinjaGame.HEIGHT_IN_TILES);
        bgBatch.end();


        camera.update();
        camera.jumpToParallax(0.7f, 0f, 0f, 11.5f);

        if (bgRenderer.getMap() != null) {
            bgRenderer.setView(camera);
            bgRenderer.render();

        }
        camera.resetParallax();



        camera.update();
        renderer.setView(camera);
        renderer.render();
        Batch spriteBatch = renderer.getSpriteBatch();
        try {
            spriteBatch.begin();
        } catch (Exception e) {
            Gdx.app.error(RoboNinjaGame.TAG, "can't begin spritebatch again");
            e.printStackTrace();
        }

        for (Puppet puppet : cutscene.getPuppets()) {
            puppet.render(renderer);
        }
        spriteBatch.end();


    }


}
