package net.tolberts.android.roboninja.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import net.tolberts.android.game.BaseScreen;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.GameState;
import net.tolberts.android.game.Input;
import net.tolberts.android.game.characters.GameCharacter;
import net.tolberts.android.game.levels.Level;
import net.tolberts.android.game.levels.LevelChangedListener;
import net.tolberts.android.game.loaders.Art;
import net.tolberts.android.game.parallax.ParallaxCamera;
import net.tolberts.android.game.parallax.RepeatingBackgroundRenderer;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.util.AudioPlayer;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.RoboNinjaGameState;
import net.tolberts.android.roboninja.RoboNinjaProgress;
import net.tolberts.android.roboninja.cutscene.Cutscene;
import net.tolberts.android.roboninja.cutscene.CutsceneFinishedCallback;
import net.tolberts.android.roboninja.hud.Hud;
import net.tolberts.android.roboninja.mc.MainCharacter;
import net.tolberts.android.roboninja.mc.abilities.consumable.PortableRespawn;
import net.tolberts.android.roboninja.mc.abilities.consumable.Slowmo;
import net.tolberts.android.roboninja.screens.camera.CameraUpdater;
import net.tolberts.android.roboninja.screens.camera.VerticalThresholdFollower;

public class GameScreen extends BaseScreen implements LevelChangedListener {

    public static final float PI2 = 3.1415926535897932384626433832795f * 2.0f;
    public Level currentLevel;

    //    CameraUpdater cameraUpdater = new MainCharFollow();
    CameraUpdater cameraUpdater = new VerticalThresholdFollower();

    protected OrthogonalTiledMapRenderer renderer;
    protected RepeatingBackgroundRenderer bgRenderer;
    protected ParallaxCamera camera;
    protected OrthographicCamera bgCamera;
    private Hud hud;
    private Rectangle cameraRectangle = new Rectangle();
    protected Texture bgImage;
    private float blindingAmount = 0.0f;
    private Texture whiteSquare;

    private final ShaderProgram waveyShader;
    private final ShaderProgram defaultShader;
    private float angleWave;


    public GameScreen(GameState gameState) {

        super(gameState);

        ShaderProgram.pedantic = false;

        SpriteBatch batch = new SpriteBatch();

        final String vertexShader = Gdx.files.internal("data/shaders/vertexShader.glsl").readString();
        final String fragmentShader = Gdx.files.internal("data/shaders/defaultPixelShader.glsl").readString();
        waveyShader = new ShaderProgram(vertexShader, fragmentShader);
        defaultShader = SpriteBatch.createDefaultShader();


//		gameState.gameScreen = this;

        GameSettings settings = GameSettings.getInstance();
        renderer = new OrthogonalTiledMapRenderer(null, settings.scale);
        bgRenderer = new RepeatingBackgroundRenderer(null, settings.scale);
        camera = new ParallaxCamera();
        camera.setToOrtho(false, settings.viewport.x, settings.viewport.y);
        camera.position.x = settings.viewport.x / 2;
        camera.position.y = settings.viewport.y / 2;
        camera.update();

        bgCamera = new OrthographicCamera();
        bgCamera.setToOrtho(false, settings.viewport.x, settings.viewport.y);
        bgCamera.position.x = settings.viewport.x / 2;
        bgCamera.position.y = settings.viewport.y / 2;
        bgCamera.update();

        hud = new Hud(this);

        whiteSquare = Art.getTexture("whiteSquare");

        if (gameState.level != null) {
            setLevel(gameState.level, gameState);
        }


        gameState.setGameScreenListener(this);

    }

    /**
     * @return Returns the world rectangle that is currently shown on camera
     */
    public Rectangle getCameraRectangle() {
        GameSettings settings = GameSettings.getInstance();
        cameraRectangle.x = camera.position.x - settings.viewport.x / 2;
        cameraRectangle.y = camera.position.y - settings.viewport.y / 2;
        cameraRectangle.width = settings.viewport.x;
        cameraRectangle.height = settings.viewport.y;
        return cameraRectangle;
    }

    public void setCameraTo(Vector2 position) {
        camera.position.x = position.x;
        camera.position.y = position.y;
    }

    public void levelChanged(Level level, GameState gameState) {
        setLevel(level, gameState);
    }

    public void setLevel(Level level, final GameState gameState) {
        currentLevel = level;
        currentLevel.initializeRenderer(renderer, bgRenderer);
        bgImage = Art.getTexture(currentLevel.zone.backdrop);
        gameState.levelTimer = 0.0f;

        AudioPlayer.playMusic(currentLevel.zone.music);
        setBlindingAmount(0.0f);

        RoboNinjaProgress progress = ((RoboNinjaGameState) gameState).getProgress();

        if ((level.activationMessage != null) && !progress.isFlagSet("activation-message-" + level.id)) {
            showActivationMessage(level.activationMessage);
            progress.setFlag("activation-message-" + level.id);
        }

        if ((level.arrivalAchievement != null) && !progress.isFlagSet("activation-achievement-" + level.id)) {
            if (gameState.speedMode != GameState.SPEED_MODE_EASY) {
                PlayServicesAdapter playServicesAdapter = ((RoboNinjaGame) gameState.game).platformAdapter.getPlayServicesAdapter();
                if (playServicesAdapter != null && playServicesAdapter.isLoggedIn()) {
                    playServicesAdapter.unlockAchievement(level.arrivalAchievement);
                    progress.setFlag("activation-achievement-" + level.id);
                }
            }
        }

        if ((level.arrivalCutscene != null) && !progress.isFlagSet("activation-cutscene-" + level.id)) {
            progress.setFlag("activation-cutscene-" + level.id);
            CutsceneScreen csScreen = new CutsceneScreen(gameState, new Cutscene(level.arrivalCutscene, new CutsceneFinishedCallback() {
                @Override
                public void cutsceneFinished() {
                    gameState.game.setScreen(GameScreen.this);

                }
            }));
            gameState.game.setScreen(csScreen);
        }

        setCameraTo(gameState.mc.getPosition());
    }

    private void showActivationMessage(String activationMessage) {
        gameState.currentMode = GameState.MODE_PAUSE;
        activationMessage = activationMessage.replace("|", "\n");
        hud.showGeneralPanel("", activationMessage);
    }

    @Override
    public void render(float delta) {
        currentLevel = gameState.level;
        hud.update(delta);
        updateGameState(delta);
        renderGame(delta);
    }


    public void updateGameState(float delta) {
        if (delta > 0.10f) {
            delta = 0.10f;
        }
        AudioPlayer.update(delta);
        gameState.update(delta);
        currentLevel.update(gameState, delta);

        if (Input.wasKeyPressed(Keys.A)) {
            gameState.mc.toggleAbility(MainCharacter.STATE_RUNNING);
        }

        if (Input.wasKeyPressed(Keys.S)) {
            gameState.mc.toggleAbility(MainCharacter.STATE_JUMPING);
        }
        if (Input.wasKeyPressed(Keys.D)) {
            gameState.mc.toggleAbility(MainCharacter.STATE_WALL);
        }
        if (Input.wasKeyPressed(Keys.F)) {
            ((RoboNinjaGameState)gameState).useConsumable(Slowmo.ID);
        }
        if (Input.wasKeyPressed(Keys.G)) {
            ((RoboNinjaGameState)gameState).useConsumable(PortableRespawn.ID);
        }

        if (Input.wasPPressed()) {
            ((RoboNinjaGameState) gameState).setRespawnAtCurrent();

        }
//        if (Input.wasLeftPressed()) {
//            camera.position.x = camera.position.x - 0.5f;
//        }
//        if (Input.wasRightPressed()) {
//            camera.position.x = camera.position.x + 0.5f;
//        }
//        if (Input.wasDownPressed()) {
//            camera.position.y = camera.position.y - 0.5f;
//        }
//        if (Input.wasUpPressed()) {
//            camera.position.y = camera.position.y + 0.5f;
//        }
        cameraUpdater.updateCamera(camera, this, delta);

        if (gameState.currentMode == GameState.MODE_TRIGGER_MAIN_MENU) {
            mainMenu();
        }
    }

    private void mainMenu() {
        AudioPlayer.playMusic(MainMenuScreen.MAIN_MENU_MUSIC);
        setScreen(new MainMenuScreen(gameState));
    }

    public void renderGame(float delta) {

        final float dt = Gdx.graphics.getRawDeltaTime();

        angleWave += dt * 2;
        while (angleWave > PI2)
            angleWave -= PI2;

        //feed the shader with the new data
        waveyShader.begin();
//        waveyShader.setUniformf("waveData", angleWave, 0.5f);
        waveyShader.setUniformf("waveData", angleWave, 0.02f * gameState.levelTimer);
        waveyShader.end();

        Gdx.gl.glClearColor(currentLevel.backgroundColor.x, currentLevel.backgroundColor.y,
                currentLevel.backgroundColor.z, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        renderer.setView(bgCamera);
        Batch bgBatch = renderer.getSpriteBatch();
        bgBatch.setShader(defaultShader);
        bgBatch.begin();
        bgBatch.draw(bgImage, 0, 0, RoboNinjaGame.WIDTH_IN_TILES, RoboNinjaGame.HEIGHT_IN_TILES);
        bgBatch.end();

        camera.update();
        camera.jumpToParallax(0.7f, 0f, 0f, 11.5f);


        if (bgRenderer.getMap() != null) {
            if (currentLevel.zone.waveyBackdrop) {
                bgRenderer.getSpriteBatch().setShader(waveyShader);
            } else {
                bgRenderer.getSpriteBatch().setShader(defaultShader);
            }
            bgRenderer.setView(camera);
            bgRenderer.render();

        }

        camera.resetParallax();


        camera.update();
        renderer.setView(camera);
        renderer.render();
        Batch spriteBatch = renderer.getSpriteBatch();
        try {
            spriteBatch.setShader(defaultShader);
            spriteBatch.begin();
        } catch (Exception e) {
            Gdx.app.error("RoboNinja", "can't begin spritebatch again");
            e.printStackTrace();
        }

        if (blindingAmount > 0) {
            renderer.setView(bgCamera);
            spriteBatch.setColor(1, 1, 1, blindingAmount);
            spriteBatch.draw(whiteSquare, 0, 0, RoboNinjaGame.WIDTH_IN_TILES, RoboNinjaGame.HEIGHT_IN_TILES);
            renderer.setView(camera);
            spriteBatch.setColor(1, 1, 1, 1);

        }


        gameState.mc.render(renderer);
        for (GameCharacter character : gameState.characters) {
            character.render(renderer);
        }
        spriteBatch.end();
        hud.render(delta);


    }

    @Override
    public void pause() {
        gameState.gameProgress.saveProgress();
        super.pause();
    }

    @Override
    public void resume() {
        if (gameState.gameSettings.reloadOnResume) {
            gameState.resumeGame();
        }
        super.resume();
    }

    public void setBlindingAmount(float litAmount) {
        blindingAmount = litAmount;
    }
}
