package net.tolberts.android.roboninja.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.tolberts.android.game.GameSettings;
import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.platformadapters.UnavailablePlayServicesAdapter;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.HashSet;
import java.util.Set;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Robo-Ninja";

        config.addIcon("data/ic_launcher-web-32.png", Files.FileType.Internal);
        config.addIcon("data/ic_launcher-web-16.png", Files.FileType.Internal);
        config.height = RoboNinjaGame.HEIGHT;
        config.width = RoboNinjaGame.WIDTH;

        GameSettings.getInstance().reloadOnResume = false;

        PlatformAdapter platformAdapter = new PlatformAdapter() {
            @Override
            public PlayServicesAdapter getPlayServicesAdapter() {
                return new UnavailablePlayServicesAdapter();
            }

            @Override
            public boolean usesKeyboard() {
                return true;
            }

            @Override
            public boolean usesOldGraphics() {
                return false;
            }

            @Override
            public String overrideStartingLevel() {
                return null;
            }

            @Override
            public Set<String> getOtherFlags() {
                return new HashSet<String>() {{
                    add(PlatformAdapter.FLAG_ALLOW_FAST_MODE);
                }};
//                return null;
            }
        };

        RoboNinjaGame game = new RoboNinjaGame(platformAdapter);
        new LwjglApplication(game, config);
    }
}
