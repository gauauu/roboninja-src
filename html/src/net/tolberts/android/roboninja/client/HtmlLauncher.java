package net.tolberts.android.roboninja.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.platformadapters.UnavailablePlayServicesAdapter;
import net.tolberts.android.roboninja.RoboNinjaGame;

public class HtmlLauncher extends GwtApplication {

    @Override
    public GwtApplicationConfiguration getConfig() {
        return new GwtApplicationConfiguration(480, 320);
    }

    @Override
    public ApplicationListener getApplicationListener() {
        PlatformAdapter adapter = new PlatformAdapter() {
            @Override
            public PlayServicesAdapter getPlayServicesAdapter() {
                return new UnavailablePlayServicesAdapter();
            }

            @Override
            public boolean usesKeyboard() {
                return true;
            }

            @Override
            public boolean usesOldGraphics() {
                return false;
            }

            @Override
            public String overrideStartingLevel() {
                return null;
            }
        };
        return new RoboNinjaGame(adapter);
    }
}