package net.tolberts.android.roboninja;

import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.game.platformadapters.UnavailablePlayServicesAdapter;
import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.uikit.UIApplication;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;
import net.tolberts.android.roboninja.RoboNinjaGame;

import java.util.Set;

public class IOSLauncher extends IOSApplication.Delegate {
    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        PlatformAdapter adapter = new PlatformAdapter() {
            @Override
            public PlayServicesAdapter getPlayServicesAdapter() {
                return new UnavailablePlayServicesAdapter();
            }

            @Override
            public boolean usesKeyboard() {
                return false;
            }

            @Override
            public boolean usesOldGraphics() {
                return false;
            }

            @Override
            public String overrideStartingLevel() {
                return null;
            }

            @Override
            public Set<String> getOtherFlags() {
                return null;
            }
        };
        return new IOSApplication(new RoboNinjaGame(adapter), config);
    }

    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
}