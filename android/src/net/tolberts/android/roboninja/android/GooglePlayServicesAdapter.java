package net.tolberts.android.roboninja.android;

import com.badlogic.gdx.Gdx;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.leaderboard.LeaderboardVariant;
import com.google.android.gms.games.leaderboard.Leaderboards;
import com.google.android.gms.games.leaderboard.Leaderboards.SubmitScoreResult;
import net.tolberts.android.game.GamePrefs;
import net.tolberts.android.game.platformadapters.Achievement;
import net.tolberts.android.game.platformadapters.Leaderboard;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.playhelpers.GameHelper;

import java.util.HashMap;
import java.util.Map;

public class GooglePlayServicesAdapter implements PlayServicesAdapter {


    private final AndroidPlatformAdapter platformAdapter;
    private final AndroidLauncher launcher;
    private final GameHelper gameHelper;

    private final Map<String, String> achievementMap = new HashMap<String, String>() {{
        put(Achievement.COMPLETE, "CgkI2bGAiokcEAIQAAA");
        put(Achievement.COMPLETE_500, "CgkI2bGAiokcEAIQAQ");
        put(Achievement.COMPLETE_250, "CgkI2bGAiokcEAIQAg");
        put(Achievement.COMPLETE_50, "CgkI2bGAiokcEAIQDA");
        put(Achievement.COMPLETE_0, "CgkI2bGAiokcEAIQAw");
        put(Achievement.ANGUNA, "CgkI2bGAiokcEAIQBA");
        put(Achievement.ONE_DOWN, "CgkI2bGAiokcEAIQBw");
        put(Achievement.COLLECTIBLES, "CgkI2bGAiokcEAIQDw");
    }};

    private final Map<String, String> leaderboardMap = new HashMap<String, String>() {{
        put(Leaderboard.DEATHS, "CgkI2bGAiokcEAIQBQ");
        put(Leaderboard.TIME, "CgkI2bGAiokcEAIQCA");
        put(Leaderboard.DEATHS_HARD, "CgkI2bGAiokcEAIQCg");
        put(Leaderboard.TIME_HARD, "CgkI2bGAiokcEAIQCw");
    }};

    public GooglePlayServicesAdapter(AndroidPlatformAdapter androidPlatformAdapter) {
        this.platformAdapter = androidPlatformAdapter;
        this.launcher = platformAdapter.launcher;
        this.gameHelper = platformAdapter.gameHelper;
    }


    @Override
    public String getPlayServiceLabel() {
        return "Google Play";
    }

    @Override
    public boolean isAvailable() {
        return platformAdapter.isNetworkAvailable();
    }

    public boolean isEnabled() {
        return isAvailable() && GamePrefs.getBool(ALLOW_PLAY_SERVICES, false);
    }

    @Override
    public boolean attemptLogin() {
        loginGPGS();
        return true;
    }

    public boolean isLoggedIn() {
        return gameHelper.isSignedIn();
    }

    @Override
    public void logOut() {
        gameHelper.signOut();
    }

    private void loginIfNecessary() {
        if (isAvailable() && isEnabled() && !gameHelper.isSignedIn() && !gameHelper.isConnecting()) {
            loginGPGS();
        }
    }

    @Override
    public void unlockAchievement(String achievement) {
        String key = achievementMap.get(achievement);
        if (!isAvailable()) {
            return;
        }
        if (!isEnabled()) {
            return;
        }
        if (key != null) {
            loginIfNecessary();
            if (gameHelper.isSignedIn()) {
                try {
                    Games.Achievements.unlock(gameHelper.getApiClient(), key);
                } catch (Exception e) {
                    Gdx.app.log(RoboNinjaGame.TAG, "failed to unlock achievement: " + key + ": " + e.getMessage(), e);

                }
            }
        }

    }

    @Override
    public void showAchievements() {
        launcher.startActivityForResult(Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 23423);
    }

    @Override
    public void submitLeaderboardScore(final String leaderboard, int score, final AdapterStringCallback callback) {
        final String id = leaderboardMap.get(leaderboard);
        Games.Leaderboards.submitScoreImmediate(gameHelper.getApiClient(), id, score).setResultCallback(new ResultCallback<SubmitScoreResult>(){
            @Override
            public void onResult(SubmitScoreResult res) {
                if (res.getStatus().getStatusCode() == 0) {
                    callback.setResult(leaderboard);
                }
            }
        });
    }

    public void loginGPGS() {
        try {
            launcher.runOnUiThread(new Runnable() {
                public void run() {
                    gameHelper.beginUserInitiatedSignIn();
                }
            });
        } catch (final Exception ex) {
            Gdx.app.error(RoboNinjaGame.TAG, ex.getMessage());
        }
    }

    public void showLeaderboard(final String leaderboard) {

        try {
            launcher.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final String id = leaderboardMap.get(leaderboard);
                    launcher.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(),
                            id), 1);
                }
            });
        } catch (final Exception e) {
            Gdx.app.error(RoboNinjaGame.TAG, e.getMessage());
        }
    }

    @Override
    public void showAllLeaderboards() {

        try {
            launcher.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    launcher.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(gameHelper.getApiClient()), 1);
                }
            });
        } catch (final Exception e) {
            Gdx.app.error(RoboNinjaGame.TAG, e.getMessage());
        }

    }


    @Override
    public void queryLeaderboardScore(String leaderboard, int timespan, final AdapterStringCallback callback) {
        final String id = leaderboardMap.get(leaderboard);
        PendingResult<Leaderboards.LoadPlayerScoreResult> pendingResult =
                Games.Leaderboards.loadCurrentPlayerLeaderboardScore(
                        gameHelper.getApiClient(), id, timespan, LeaderboardVariant.COLLECTION_PUBLIC);

        pendingResult.setResultCallback(new ResultCallback<Leaderboards.LoadPlayerScoreResult>() {
            @Override
            public void onResult(Leaderboards.LoadPlayerScoreResult loadPlayerScoreResult) {
                callback.setResult(loadPlayerScoreResult.getScore().getDisplayRank());
            }
        });
    }

}
