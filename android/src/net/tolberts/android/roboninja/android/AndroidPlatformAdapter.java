package net.tolberts.android.roboninja.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import net.tolberts.android.game.platformadapters.PlatformAdapter;
import net.tolberts.android.game.platformadapters.PlayServicesAdapter;
import net.tolberts.android.roboninja.playhelpers.GameHelper;

import java.util.Set;


public class AndroidPlatformAdapter implements PlatformAdapter {

    final GameHelper gameHelper;
    final AndroidLauncher launcher;
    private final PlayServicesAdapter playServicesAdapter;
    public boolean usesOldGraphics = false;
    public String overrideStartingLevel = null;


    public AndroidPlatformAdapter(AndroidLauncher androidLauncher, GameHelper gameHelper) {
        this.gameHelper = gameHelper;
        this.launcher = androidLauncher;
        if (gameHelper != null) {
            this.playServicesAdapter = new GooglePlayServicesAdapter(this);
        } else {
            this.playServicesAdapter = new PlayServicesAdapter(){
                @Override
                public String getPlayServiceLabel() {
                    return null;
                }

                @Override
                public boolean isAvailable() {
                    return false;
                }

                @Override
                public boolean isEnabled() {
                    return false;
                }

                @Override
                public boolean attemptLogin() {
                    return false;
                }

                @Override
                public void logOut() {

                }

                @Override
                public boolean isLoggedIn() {
                    return false;
                }

                @Override
                public void unlockAchievement(String achievement) {

                }

                @Override
                public void showAchievements() {

                }

                @Override
                public void submitLeaderboardScore(String leaderboard, int score, AdapterStringCallback callback) {

                }

                @Override
                public void showLeaderboard(String leaderboard) {

                }

                @Override
                public void showAllLeaderboards() {

                }

                @Override
                public void queryLeaderboardScore(String leaderboard, int timeSpan, AdapterStringCallback callback) {

                }
            };
        }
    }

    @Override
    public PlayServicesAdapter getPlayServicesAdapter() {
        return playServicesAdapter;
    }

    @Override
    public boolean usesKeyboard() {
        return false;
    }

    @Override
    public boolean usesOldGraphics() {
        return usesOldGraphics;
    }

    @Override
    public String overrideStartingLevel() {
        return overrideStartingLevel;
    }

    @Override
    public Set<String> getOtherFlags() {
        return null;
    }

    boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager
                = (ConnectivityManager) launcher.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }




}