package net.tolberts.android.roboninja.android.aaron;

import android.content.Intent;
import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.android.AndroidLauncher;
import net.tolberts.android.roboninja.android.AndroidPlatformAdapter;
import net.tolberts.android.roboninja.playhelpers.GameHelper;

public class AaronAndroidLauncher extends AndroidLauncher {

    GameHelper gameHelper;
    AndroidPlatformAdapter platformAdapter;

	@Override
	protected void onCreate (Bundle savedInstanceState)  {

		super.onCreate(savedInstanceState);



		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;
        platformAdapter = new AndroidPlatformAdapter(this, null);
        platformAdapter.usesOldGraphics = true;
        platformAdapter.overrideStartingLevel = "aaron";
        gameHelper.setup(this);



		initialize(new RoboNinjaGame(platformAdapter), config);

	}

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    @Override
    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
    }




    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onStop(){
        super.onStop();
    }
}
