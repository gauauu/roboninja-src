package net.tolberts.android.roboninja.android;

import android.content.Intent;
import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.playhelpers.GameHelper;

import java.util.concurrent.Semaphore;

public class AndroidLauncher extends AndroidApplication implements GameHelper.GameHelperListener {

    public GameHelper gameHelper;
    public AndroidPlatformAdapter platformAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;
        gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
//        gameHelper.setConnectOnStart(GamePrefs.getBool(GooglePlayServicesAdapter.ALLOW_PLAY_SERVICES, false));
        gameHelper.setConnectOnStart(false);
        platformAdapter = createPlatformAdapter();
        gameHelper.setup(this);


        initialize(new RoboNinjaGame(platformAdapter), config);

    }

    protected AndroidPlatformAdapter createPlatformAdapter() {
        return new AndroidPlatformAdapter(this, gameHelper);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }

    @Override
    public void onActivityResult(int request, int response, Intent data) {
        super.onActivityResult(request, response, data);
        gameHelper.onActivityResult(request, response, data);

    }


    @Override
    public void onStart() {
        super.onStart();
        gameHelper.onStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        gameHelper.onStop();
    }


}
