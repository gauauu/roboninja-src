Robo-Ninja is a challenging metroid-vania style game, primarily targeted to mobile devices (Android in particular). It is centered around the "tap to jump" mechanic, but with the freedom to explore a world looking for new power-ups.  You can find more information at the [Bite the Chili](http://www.bitethechili.com/roboninja/) website.

Robo-Ninja is built with [libGDX](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0CB8QFjAAahUKEwix-8eJt9LHAhXTi5IKHQ8xBfA&url=https%3A%2F%2Flibgdx.badlogicgames.com%2F&ei=09DjVfHFKdOXygSP4pSADw&usg=AFQjCNFafSiou65aW33ixPv5H2htnp7Nyw&sig2=pZL1okCZkxQV8B0PHXvcQg), and can be built for desktop, Android, and (theoretically) iOS.  Because of the use of reflection and compressed data, the gwt/javascript version will not currently work.

I did a lot of learning as I went with Robo-Ninja, and made quite a few design changes over time, so please forgive some of the messy and unpleasant code, and ugly hacks.  There's quite a bit of code here that I'm not completely proud of.

All the code here is licensed under the NCSA license (see license.txt), but the graphic and audio assets included have their own licenses, which you can find under the file android/assets/data/text-data/credits.txt
