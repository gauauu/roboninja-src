package net.tolberts.android.roboninja.android.donate;

import net.tolberts.android.roboninja.android.AndroidLauncher;
import net.tolberts.android.roboninja.android.AndroidPlatformAdapter;
import net.tolberts.android.roboninja.playhelpers.GameHelper;

import java.util.HashSet;
import java.util.Set;


public class DonateAndroidPlatformAdapter extends AndroidPlatformAdapter {


    public DonateAndroidPlatformAdapter(AndroidLauncher androidLauncher, GameHelper gameHelper) {
        super(androidLauncher, gameHelper);
    }

    public Set<String> getOtherFlags(){
        return new HashSet<String>(){{
            add(FLAG_ALLOW_FAST_MODE);
        }};
    }
}