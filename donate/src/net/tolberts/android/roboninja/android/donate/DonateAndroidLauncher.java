package net.tolberts.android.roboninja.android.donate;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import net.tolberts.android.roboninja.RoboNinjaGame;
import net.tolberts.android.roboninja.android.*;

public class DonateAndroidLauncher extends AndroidLauncher {
    @Override
    protected AndroidPlatformAdapter createPlatformAdapter() {
        return new DonateAndroidPlatformAdapter(this, gameHelper);
    }
}
