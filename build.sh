#!/bin/bash

if [ -z "$TMP" ]; then
    TMP="/tmp/roboninja"
fi

if [ -z "$WORKSPACE" ]; then
    WORKSPACE=`pwd`
fi

if [ -z "$TMXRASTERIZER" ]; then
    TMXRASTERIZER="$WORKSPACE/tools/bin/tmxrasterizer"
fi

#if [ -z "$ANT_HOME" ]; then
    #ANT_EXEC="ant"
#else
    #ANT_EXEC="$ANT_HOME/bin/ant"
#fi

if [ "$1" = "--skip-map" ] || [ "$2" = "--skip-map" ] || [ "$1" = "--aaron" ]; then
    echo "skipping map"
else

    export MAP_DIR="$WORKSPACE/android/assets/data/maps"

    rm -rf "$TMP"
    mkdir "$TMP"

    cd "$WORKSPACE/tools/mmapgen"
    chmod 777 gradlew
    ./gradlew clean
    ./gradlew installApp
    cd "$WORKSPACE/tools/bin"
    cp "$WORKSPACE/tools/mmapgen/yellowDotSmall.png" .
    cp "$WORKSPACE/tools/mmapgen/blueDotSmall.png" .
    chmod 777 *
    "$WORKSPACE/tools/mmapgen/build/install/mmapgen/bin/mmapgen" "$MAP_DIR" "$TMP/minimap.png" 2 "$TMXRASTERIZER -t %s -a --ignore-visibility" "$TMP"
    cp "$TMP/minimap.png" "$WORKSPACE/android/assets/data/textures"

fi


if [ "$1" = "--skip-build" ] || [ "$2" = "--skip-build" ] || [ "$1" = "--aaron" ] || [ "$1" = "--donate" ]; then
    echo "skipping normal build"
else
    cd "$WORKSPACE"
    ./gradlew clean
    ./gradlew android:build
fi

if [ "$1" = "--aaron" ]; then
    echo "building aaron version"
    cd "$WORKSPACE"
    ./gradlew clean
    cp -rf android/assets aaron
    ./gradlew aaron:build
    #jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/Dropbox/devStuff/androidBiteTheChiliKey aaron/build/outputs/apk/aaron-release-unsigned.apk bitethechiliproduction
    #rm -f aaron/build/outputs/apk/roboninja-aaron.apk
    #/mnt/data/lib-linux/android-sdk/build-tools/21.0.0/zipalign -v 4 aaron/build/outputs/apk/aaron-release-unsigned.apk aaron/build/outputs/apk/roboninja-aaron.apk
    echo "cleanup...."
    cd aaron/assets
    git clean -df
    cd ../..
    #cp aaron/build/outputs/apk/roboninja-aaron.apk /home/tolbert/Dropbox/Public
fi

if [ "$1" = "--donate" ]; then
    echo "building donate version"
    cd "$WORKSPACE"
    ./gradlew clean
    cp -rf android/assets donate
    cp -rf android/libs donate
    cp -rf android/src donate
    ./gradlew donate:build
    echo "cleanup"
    cd donate/assets
    git clean -df
    cd ../lib
    git clean -df
    cd ../..
    echo "signing and aligning for release: $RELEASE_KEY"
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $RELEASE_KEY -storepass $RELEASE_KEY_PASSWD donate/build/outputs/apk/donate-release-unsigned.apk bitethechiliproduction
    rm -f donate/build/outputs/apk/roboninja-donate.apk
    $SDK_DIR/build-tools/21.0.0/zipalign -v 4 donate/build/outputs/apk/donate-release-unsigned.apk donate/build/outputs/apk/roboninja-donate.apk

fi

if [ "$1" = "--release-script" ] || [ "$2" = "--release-script" ] || [ "$3" = "--release-script" ] ; then
    echo "signing and aligning for release: $RELEASE_KEY"
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $RELEASE_KEY -storepass $RELEASE_KEY_PASSWD android/build/outputs/apk/android-release-unsigned.apk bitethechiliproduction
    rm -f android/build/outputs/apk/roboninja.apk
    $SDK_DIR/build-tools/21.0.0/zipalign -v 4 android/build/outputs/apk/android-release-unsigned.apk android/build/outputs/apk/roboninja.apk
fi

if [ "$1" = "--release" ] || [ "$2" = "--release" ] || [ "$3" = "--release" ] ; then
    echo "signing and aligning for release"
    jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ~/Dropbox/devStuff/androidBiteTheChiliKey android/build/outputs/apk/android-release-unsigned.apk bitethechiliproduction
    rm -f android/build/outputs/apk/roboninja.apk
    /mnt/data/lib-linux/android-sdk/build-tools/21.0.0/zipalign -v 4 android/build/outputs/apk/android-release-unsigned.apk android/build/outputs/apk/roboninja.apk
fi

if [ "$1" = "--push-pub" ] || [ "$2" = "--push-pub" ] || [ "$3" = "--push-pub" ] ; then
    echo "pushing to dropbox..."
    cp "$TMP/minimap.png" "/home/tolbert/Dropbox/Public"
    cp "$WORKSPACE/android/build/outputs/apk/roboninja.apk" "/home/tolbert/Dropbox/Public/"
fi

